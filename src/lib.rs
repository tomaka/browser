#![warn(unsafe_code)]

extern crate cssparser;
extern crate fnv;
extern crate html5ever;
extern crate smallvec;
extern crate string_cache;      // https://github.com/servo/html5ever/issues/210

pub use document::Document;

mod block;
mod computed;
mod css_property;
mod css_rule;
mod css_rules_set;
mod css_selector;
mod css_stylesheet;
mod css_unit;
mod document;
mod flow;
mod inline;
mod root_flow;
mod specified;
mod style_tree;

pub mod draw;
