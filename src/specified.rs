use std::ascii::AsciiExt;
use std::fmt::Debug;
use std::vec::IntoIter as VecIntoIter;
use cssparser::Color;
use cssparser::Parser;
use cssparser::Token;
use css_property::CssProperty;
use css_rule::CssRule;
use css_unit::CssUnit;
use draw::Cursor;
use draw::Draw;

/// Contains the specified values of a node.
#[derive(Debug, Copy, Clone)]
pub struct SpecifiedValues<I, F> {
    pub background_attachment: ValueOrInherit<SpecifiedBackgroundAttachment>,
    pub background_color: ValueOrInherit<SpecifiedColor>,
    pub background_image: ValueOrInherit<Option<I>>,
    pub background_position: ValueOrInherit<(SpecifiedLengthPercentage,
                                             SpecifiedLengthPercentage)>,
    pub background_repeat: ValueOrInherit<SpecifiedBackgroundRepeat>,
    pub border_bottom_color: ValueOrInherit<SpecifiedColor>,
    pub border_bottom_style: ValueOrInherit<SpecifiedBorderStyle>,
    pub border_bottom_width: ValueOrInherit<SpecifiedLength>,
    pub border_left_color: ValueOrInherit<SpecifiedColor>,
    pub border_left_style: ValueOrInherit<SpecifiedBorderStyle>,
    pub border_left_width: ValueOrInherit<SpecifiedLength>,
    pub border_right_color: ValueOrInherit<SpecifiedColor>,
    pub border_right_style: ValueOrInherit<SpecifiedBorderStyle>,
    pub border_right_width: ValueOrInherit<SpecifiedLength>,
    pub border_top_color: ValueOrInherit<SpecifiedColor>,
    pub border_top_style: ValueOrInherit<SpecifiedBorderStyle>,
    pub border_top_width: ValueOrInherit<SpecifiedLength>,
    pub bottom: ValueOrInherit<SpecifiedLengthPercentageAuto>,
    pub clear: ValueOrInherit<SpecifiedClear>,
    pub color: ValueOrInherit<SpecifiedColor>,
    pub cursor: ValueOrInherit<SpecifiedCursor<I>>,
    pub display: ValueOrInherit<SpecifiedDisplay>,
    pub float: ValueOrInherit<SpecifiedFloat>,
    pub font_size: ValueOrInherit<SpecifiedLengthPercentage>,
    pub font_family: ValueOrInherit<F>,
    pub height: ValueOrInherit<SpecifiedLengthPercentageAuto>,
    pub left: ValueOrInherit<SpecifiedLengthPercentageAuto>,
    pub margin_bottom: ValueOrInherit<SpecifiedLengthPercentageAuto>,
    pub margin_left: ValueOrInherit<SpecifiedLengthPercentageAuto>,
    pub margin_right: ValueOrInherit<SpecifiedLengthPercentageAuto>,
    pub margin_top: ValueOrInherit<SpecifiedLengthPercentageAuto>,
    pub padding_bottom: ValueOrInherit<SpecifiedLengthPercentage>,
    pub padding_left: ValueOrInherit<SpecifiedLengthPercentage>,
    pub padding_right: ValueOrInherit<SpecifiedLengthPercentage>,
    pub padding_top: ValueOrInherit<SpecifiedLengthPercentage>,
    pub position: ValueOrInherit<SpecifiedPosition>,
    pub right: ValueOrInherit<SpecifiedLengthPercentageAuto>,
    pub text_align: ValueOrInherit<SpecifiedTextAlign>,
    pub top: ValueOrInherit<SpecifiedLengthPercentageAuto>,
    pub visibility: ValueOrInherit<SpecifiedVisibility>,
    pub white_space: ValueOrInherit<SpecifiedWhiteSpace>,
    pub width: ValueOrInherit<SpecifiedLengthPercentageAuto>,
}

impl<I, F> SpecifiedValues<I, F> {
    pub fn merge(&mut self, rule: &CssRule<I, F>) where I: Clone, F: Clone {
        match *rule {
            CssRule::BackgroundAttachment(ref val) => self.background_attachment = val.clone(),
            CssRule::BackgroundColor(ref val) => self.background_color = val.clone(),
            CssRule::BackgroundImage(ref val) => self.background_image = val.clone(),
            CssRule::BackgroundPosition(ref val) => self.background_position = val.clone(),
            CssRule::BackgroundRepeat(ref val) => self.background_repeat = val.clone(),
            CssRule::BorderBottomColor(ref val) => self.border_bottom_color = val.clone(),
            CssRule::BorderBottomStyle(ref val) => self.border_bottom_style = val.clone(),
            CssRule::BorderBottomWidth(ref val) => self.border_bottom_width = val.clone(),
            CssRule::BorderLeftColor(ref val) => self.border_left_color = val.clone(),
            CssRule::BorderLeftStyle(ref val) => self.border_left_style = val.clone(),
            CssRule::BorderLeftWidth(ref val) => self.border_left_width = val.clone(),
            CssRule::BorderRightColor(ref val) => self.border_right_color = val.clone(),
            CssRule::BorderRightStyle(ref val) => self.border_right_style = val.clone(),
            CssRule::BorderRightWidth(ref val) => self.border_right_width = val.clone(),
            CssRule::BorderTopColor(ref val) => self.border_top_color = val.clone(),
            CssRule::BorderTopStyle(ref val) => self.border_top_style = val.clone(),
            CssRule::BorderTopWidth(ref val) => self.border_top_width = val.clone(),
            CssRule::Bottom(ref val) => self.bottom = val.clone(),
            CssRule::Clear(ref val) => self.clear = val.clone(),
            CssRule::Color(ref val) => self.color = val.clone(),
            CssRule::Cursor(ref val) => self.cursor = val.clone(),
            CssRule::Display(ref val) => self.display = val.clone(),
            CssRule::Float(ref val) => self.float = val.clone(),
            CssRule::FontFamily(ref val) => self.font_family = val.clone(),
            CssRule::FontSize(ref val) => self.font_size = val.clone(),
            CssRule::Height(ref val) => self.height = val.clone(),
            CssRule::Left(ref val) => self.left = val.clone(),
            CssRule::MarginBottom(ref val) => self.margin_bottom = val.clone(),
            CssRule::MarginLeft(ref val) => self.margin_left = val.clone(),
            CssRule::MarginRight(ref val) => self.margin_right = val.clone(),
            CssRule::MarginTop(ref val) => self.margin_top = val.clone(),
            CssRule::PaddingBottom(ref val) => self.padding_bottom = val.clone(),
            CssRule::PaddingLeft(ref val) => self.padding_left = val.clone(),
            CssRule::PaddingRight(ref val) => self.padding_right = val.clone(),
            CssRule::PaddingTop(ref val) => self.padding_top = val.clone(),
            CssRule::Position(ref val) => self.position = val.clone(),
            CssRule::Right(ref val) => self.right = val.clone(),
            CssRule::TextAlign(ref val) => self.text_align = val.clone(),
            CssRule::Top(ref val) => self.top = val.clone(),
            CssRule::Visibility(ref val) => self.visibility = val.clone(),
            CssRule::WhiteSpace(ref val) => self.white_space = val.clone(),
            CssRule::Width(ref val) => self.width = val.clone(),
        }
    }
}

impl<I, F> Default for SpecifiedValues<I, F> {
    fn default() -> SpecifiedValues<I, F> {
        SpecifiedValues {
            background_attachment: ValueOrInherit::Value(SpecifiedBackgroundAttachment::Scroll),
            background_color: ValueOrInherit::Value(SpecifiedColor { red: 0.0, green: 0.0, blue: 0.0, alpha: 0.0 }),        // TODO: "transparent"
            background_image: ValueOrInherit::Value(None),
            background_position: ValueOrInherit::Value((SpecifiedLengthPercentage::Percentage { normalized_percents: 0.0 },
                                                        SpecifiedLengthPercentage::Percentage { normalized_percents: 0.0 })),
            background_repeat: ValueOrInherit::Value(SpecifiedBackgroundRepeat::Repeat),
            border_bottom_color: ValueOrInherit::Value(SpecifiedColor { red: 0.0, green: 0.0, blue: 0.0, alpha: 1.0 }),        // FIXME: something that means "same value as color"
            border_bottom_style: ValueOrInherit::Value(SpecifiedBorderStyle::None),
            border_bottom_width: ValueOrInherit::Value(SpecifiedLength { value: 1.0, unit: CssUnit::Px }),       // TODO: "medium"
            border_left_color: ValueOrInherit::Value(SpecifiedColor { red: 0.0, green: 0.0, blue: 0.0, alpha: 1.0 }),        // FIXME: something that means "same value as color"
            border_left_style: ValueOrInherit::Value(SpecifiedBorderStyle::None),
            border_left_width: ValueOrInherit::Value(SpecifiedLength { value: 1.0, unit: CssUnit::Px }),     // TODO: "medium"
            border_right_color: ValueOrInherit::Value(SpecifiedColor { red: 0.0, green: 0.0, blue: 0.0, alpha: 1.0 }),        // FIXME: something that means "same value as color"
            border_right_style: ValueOrInherit::Value(SpecifiedBorderStyle::None),
            border_right_width: ValueOrInherit::Value(SpecifiedLength { value: 1.0, unit: CssUnit::Px }),        // TODO: "medium"
            border_top_color: ValueOrInherit::Value(SpecifiedColor { red: 0.0, green: 0.0, blue: 0.0, alpha: 1.0 }),        // FIXME: something that means "same value as color"
            border_top_style: ValueOrInherit::Value(SpecifiedBorderStyle::None),
            border_top_width: ValueOrInherit::Value(SpecifiedLength { value: 1.0, unit: CssUnit::Px }),      // TODO: "medium"
            bottom: ValueOrInherit::Value(SpecifiedLengthPercentageAuto::Auto),
            clear: ValueOrInherit::Value(SpecifiedClear::None),
            color: ValueOrInherit::Inherit,
            cursor: ValueOrInherit::Inherit,
            display: ValueOrInherit::Value(SpecifiedDisplay::Inline),
            float: ValueOrInherit::Value(SpecifiedFloat::None),
            font_size: ValueOrInherit::Inherit,            // TODO: "medium"
            font_family: ValueOrInherit::Inherit,
            height: ValueOrInherit::Value(SpecifiedLengthPercentageAuto::Auto),
            left: ValueOrInherit::Value(SpecifiedLengthPercentageAuto::Auto),
            margin_bottom: ValueOrInherit::Value(SpecifiedLengthPercentageAuto::Length { value: 0.0, unit: CssUnit::Px }),
            margin_left: ValueOrInherit::Value(SpecifiedLengthPercentageAuto::Length { value: 0.0, unit: CssUnit::Px }),
            margin_right: ValueOrInherit::Value(SpecifiedLengthPercentageAuto::Length { value: 0.0, unit: CssUnit::Px }),
            margin_top: ValueOrInherit::Value(SpecifiedLengthPercentageAuto::Length { value: 0.0, unit: CssUnit::Px }),
            padding_bottom: ValueOrInherit::Value(SpecifiedLengthPercentage::Length { value: 0.0, unit: CssUnit::Px }),
            padding_left: ValueOrInherit::Value(SpecifiedLengthPercentage::Length { value: 0.0, unit: CssUnit::Px }),
            padding_right: ValueOrInherit::Value(SpecifiedLengthPercentage::Length { value: 0.0, unit: CssUnit::Px }),
            padding_top: ValueOrInherit::Value(SpecifiedLengthPercentage::Length { value: 0.0, unit: CssUnit::Px }),
            position: ValueOrInherit::Value(SpecifiedPosition::Static),
            right: ValueOrInherit::Value(SpecifiedLengthPercentageAuto::Auto),
            text_align: ValueOrInherit::Inherit,
            top: ValueOrInherit::Value(SpecifiedLengthPercentageAuto::Auto),
            visibility: ValueOrInherit::Inherit,
            white_space: ValueOrInherit::Inherit,
            width: ValueOrInherit::Value(SpecifiedLengthPercentageAuto::Auto),
        }
    }
}

impl<I, F> SpecifiedValues<I, F> {
    pub fn initial(initial_font: F) -> SpecifiedValues<I, F> {
        SpecifiedValues {
            color: ValueOrInherit::Value(SpecifiedColor { red: 0.0, green: 0.0, blue: 0.0, alpha: 1.0 }),       // TODO: implementation-defined
            cursor: ValueOrInherit::Value(SpecifiedCursor::Predefined(Cursor::Auto)),
            font_size: ValueOrInherit::Value(SpecifiedLengthPercentage::Length { value: 14.15, unit: CssUnit::Px }),     // TODO: "medium"
            font_family: ValueOrInherit::Value(initial_font),
            text_align: ValueOrInherit::Value(SpecifiedTextAlign::Left),
            visibility: ValueOrInherit::Value(SpecifiedVisibility::Visible),
            white_space: ValueOrInherit::Value(SpecifiedWhiteSpace::Normal),
            .. Default::default()
        }
    }
}

#[derive(Debug, Copy, Clone, PartialEq, Eq)]
pub enum ValueOrInherit<T> {
    Value(T),
    Inherit,
}

#[derive(Debug, Copy, Clone, PartialEq, Eq)]
pub enum SpecifiedDisplay {
    None,
    Block,
    InlineBlock,
    Inline,
    ListItem,
}

impl SpecifiedDisplay {
    pub fn parse(input: &mut Parser) -> Result<SpecifiedDisplay, ()> {
        match try!(input.next()) {
            Token::Ident(ref ident) if ident.eq_ignore_ascii_case("none") => {
                Ok(SpecifiedDisplay::None)
            },
            Token::Ident(ref ident) if ident.eq_ignore_ascii_case("block") => {
                Ok(SpecifiedDisplay::Block)
            },
            Token::Ident(ref ident) if ident.eq_ignore_ascii_case("inline-block") => {
                Ok(SpecifiedDisplay::InlineBlock)
            },
            Token::Ident(ref ident) if ident.eq_ignore_ascii_case("inline") => {
                Ok(SpecifiedDisplay::Inline)
            },
            Token::Ident(ref ident) if ident.eq_ignore_ascii_case("list-item") => {
                Ok(SpecifiedDisplay::ListItem)
            },
            _ => Err(())
        }
    }
}

#[derive(Debug, Copy, Clone, PartialEq)]
pub struct SpecifiedLength {
    pub value: f32,
    pub unit: CssUnit,
}

impl SpecifiedLength {
    pub fn parse(input: &mut Parser) -> Result<SpecifiedLength, ()> {
        match try!(input.next()) {
            Token::Dimension(val, unit) => {
                Ok(SpecifiedLength { value: val.value, unit: try!(unit.parse()) })
            },
            Token::Number(val) if val.value == 0.0 => {
                Ok(SpecifiedLength { value: 0.0, unit: CssUnit::Px })
            },
            _ => Err(())
        }
    }
}

#[derive(Debug, Copy, Clone, PartialEq)]
pub enum SpecifiedLengthPercentageAuto {
    Length { value: f32, unit: CssUnit },
    Percentage { normalized_percents: f32 },
    Auto,
}

impl SpecifiedLengthPercentageAuto {
    pub fn parse(input: &mut Parser) -> Result<SpecifiedLengthPercentageAuto, ()> {
        match try!(input.next()) {
            Token::Ident(ref ident) if ident.eq_ignore_ascii_case("auto") => {
                Ok(SpecifiedLengthPercentageAuto::Auto)
            },
            Token::Dimension(val, unit) => {
                Ok(SpecifiedLengthPercentageAuto::Length { value: val.value, unit: try!(unit.parse()) })
            },
            Token::Number(val) if val.value == 0.0 => {
                Ok(SpecifiedLengthPercentageAuto::Length { value: 0.0, unit: CssUnit::Px })
            },
            Token::Percentage(val) => {
                Ok(SpecifiedLengthPercentageAuto::Percentage { normalized_percents: val.unit_value })
            },
            _ => Err(())
        }
    }
}

#[derive(Debug, Copy, Clone, PartialEq)]
pub enum SpecifiedLengthPercentage {
    Length { value: f32, unit: CssUnit },
    Percentage { normalized_percents: f32 },
}

impl SpecifiedLengthPercentage {
    pub fn parse(input: &mut Parser) -> Result<SpecifiedLengthPercentage, ()> {
        match try!(input.next()) {
            Token::Dimension(val, unit) => {
                Ok(SpecifiedLengthPercentage::Length { value: val.value, unit: try!(unit.parse()) })
            },
            Token::Number(val) if val.value == 0.0 => {
                Ok(SpecifiedLengthPercentage::Length { value: 0.0, unit: CssUnit::Px })
            },
            Token::Percentage(val) => {
                Ok(SpecifiedLengthPercentage::Percentage { normalized_percents: val.unit_value })
            },
            _ => Err(())
        }
    }
}

#[derive(Debug, Copy, Clone, PartialEq, Eq)]
pub enum SpecifiedPosition {
    Static,
    Relative,
    Absolute,
    Fixed,
}

impl SpecifiedPosition {
    pub fn parse(input: &mut Parser) -> Result<SpecifiedPosition, ()> {
        match try!(input.next()) {
            Token::Ident(ref ident) if ident.eq_ignore_ascii_case("static") => {
                Ok(SpecifiedPosition::Static)
            },
            Token::Ident(ref ident) if ident.eq_ignore_ascii_case("relative") => {
                Ok(SpecifiedPosition::Relative)
            },
            Token::Ident(ref ident) if ident.eq_ignore_ascii_case("absolute") => {
                Ok(SpecifiedPosition::Absolute)
            },
            Token::Ident(ref ident) if ident.eq_ignore_ascii_case("fixed") => {
                Ok(SpecifiedPosition::Fixed)
            },
            _ => Err(())
        }
    }
}

#[derive(Debug, Copy, Clone, PartialEq, Eq)]
pub enum SpecifiedTextAlign {
    Left,
    Center,
    Right,
    Justify,
}

impl SpecifiedTextAlign {
    pub fn parse(input: &mut Parser) -> Result<SpecifiedTextAlign, ()> {
        match try!(input.next()) {
            Token::Ident(ref ident) if ident.eq_ignore_ascii_case("left") => {
                Ok(SpecifiedTextAlign::Left)
            },
            Token::Ident(ref ident) if ident.eq_ignore_ascii_case("center") => {
                Ok(SpecifiedTextAlign::Center)
            },
            Token::Ident(ref ident) if ident.eq_ignore_ascii_case("right") => {
                Ok(SpecifiedTextAlign::Right)
            },
            Token::Ident(ref ident) if ident.eq_ignore_ascii_case("justify") => {
                Ok(SpecifiedTextAlign::Justify)
            },
            _ => Err(())
        }
    }
}

#[derive(Debug, Copy, Clone, PartialEq, Eq)]
pub enum SpecifiedVisibility {
    Visible,
    Hidden,
    Collapse,
}

impl SpecifiedVisibility {
    pub fn parse(input: &mut Parser) -> Result<SpecifiedVisibility, ()> {
        match try!(input.next()) {
            Token::Ident(ref ident) if ident.eq_ignore_ascii_case("visible") => {
                Ok(SpecifiedVisibility::Visible)
            },
            Token::Ident(ref ident) if ident.eq_ignore_ascii_case("hidden") => {
                Ok(SpecifiedVisibility::Hidden)
            },
            Token::Ident(ref ident) if ident.eq_ignore_ascii_case("collapse") => {
                Ok(SpecifiedVisibility::Collapse)
            },
            _ => Err(())
        }
    }
}

#[derive(Debug, Copy, Clone, PartialEq)]
pub struct SpecifiedColor {
    pub red: f32,
    pub green: f32,
    pub blue: f32,
    pub alpha: f32,
}

impl SpecifiedColor {
    pub fn parse(input: &mut Parser) -> Result<SpecifiedColor, ()> {
        match try!(Color::parse(input)) {
            Color::RGBA(color) => {
                Ok(SpecifiedColor {
                    red: color.red,
                    green: color.green,
                    blue: color.blue,
                    alpha: color.alpha,
                })
            },
            // TODO: currentColor
            _ => Err(())
        }
    }
}

#[derive(Debug, Copy, Clone, PartialEq, Eq)]
pub enum SpecifiedBorderStyle {
    None,
    Hidden,
    Dotted,
    Dashed,
    Solid,
    Double,
    Groove,
    Ridge,
    Inset,
    Outset,
}

impl SpecifiedBorderStyle {
    #[inline]
    pub fn is_none_or_hidden(&self) -> bool {
        match *self {
            SpecifiedBorderStyle::None => true,
            SpecifiedBorderStyle::Hidden => true,
            _ => false
        }
    }
}

impl SpecifiedBorderStyle {
    pub fn parse(input: &mut Parser) -> Result<SpecifiedBorderStyle, ()> {
        match try!(input.next()) {
            Token::Ident(ref ident) if ident.eq_ignore_ascii_case("none") => {
                Ok(SpecifiedBorderStyle::None)
            },
            Token::Ident(ref ident) if ident.eq_ignore_ascii_case("hidden") => {
                Ok(SpecifiedBorderStyle::Hidden)
            },
            Token::Ident(ref ident) if ident.eq_ignore_ascii_case("dotted") => {
                Ok(SpecifiedBorderStyle::Dotted)
            },
            Token::Ident(ref ident) if ident.eq_ignore_ascii_case("dashed") => {
                Ok(SpecifiedBorderStyle::Dashed)
            },
            Token::Ident(ref ident) if ident.eq_ignore_ascii_case("solid") => {
                Ok(SpecifiedBorderStyle::Solid)
            },
            Token::Ident(ref ident) if ident.eq_ignore_ascii_case("double") => {
                Ok(SpecifiedBorderStyle::Double)
            },
            Token::Ident(ref ident) if ident.eq_ignore_ascii_case("groove") => {
                Ok(SpecifiedBorderStyle::Groove)
            },
            Token::Ident(ref ident) if ident.eq_ignore_ascii_case("ridge") => {
                Ok(SpecifiedBorderStyle::Ridge)
            },
            Token::Ident(ref ident) if ident.eq_ignore_ascii_case("inset") => {
                Ok(SpecifiedBorderStyle::Inset)
            },
            Token::Ident(ref ident) if ident.eq_ignore_ascii_case("outset") => {
                Ok(SpecifiedBorderStyle::Outset)
            },
            _ => Err(())
        }
    }
}

#[derive(Debug, Copy, Clone, PartialEq, Eq)]
pub enum SpecifiedBackgroundAttachment {
    Fixed,
    Scroll,
}

impl SpecifiedBackgroundAttachment {
    pub fn parse(input: &mut Parser) -> Result<SpecifiedBackgroundAttachment, ()> {
        match try!(input.next()) {
            Token::Ident(ref ident) if ident.eq_ignore_ascii_case("fixed") => {
                Ok(SpecifiedBackgroundAttachment::Fixed)
            },
            Token::Ident(ref ident) if ident.eq_ignore_ascii_case("scroll") => {
                Ok(SpecifiedBackgroundAttachment::Scroll)
            },
            _ => Err(())
        }
    }
}

#[derive(Debug, Copy, Clone, PartialEq, Eq)]
pub enum SpecifiedBackgroundRepeat {
    NoRepeat,
    RepeatX,
    RepeatY,
    Repeat,
}

impl SpecifiedBackgroundRepeat {
    pub fn parse(input: &mut Parser) -> Result<SpecifiedBackgroundRepeat, ()> {
        match try!(input.next()) {
            Token::Ident(ref ident) if ident.eq_ignore_ascii_case("no-repeat") => {
                Ok(SpecifiedBackgroundRepeat::NoRepeat)
            },
            Token::Ident(ref ident) if ident.eq_ignore_ascii_case("repeat-x") => {
                Ok(SpecifiedBackgroundRepeat::RepeatX)
            },
            Token::Ident(ref ident) if ident.eq_ignore_ascii_case("repeat-y") => {
                Ok(SpecifiedBackgroundRepeat::RepeatY)
            },
            Token::Ident(ref ident) if ident.eq_ignore_ascii_case("repeat") => {
                Ok(SpecifiedBackgroundRepeat::Repeat)
            },
            _ => Err(())
        }
    }
}

#[derive(Debug, Copy, Clone, PartialEq, Eq)]
pub enum SpecifiedFloat {
    None,
    Left,
    Right,
}

impl SpecifiedFloat {
    pub fn parse(input: &mut Parser) -> Result<SpecifiedFloat, ()> {
        match try!(input.next()) {
            Token::Ident(ref ident) if ident.eq_ignore_ascii_case("none") => {
                Ok(SpecifiedFloat::None)
            },
            Token::Ident(ref ident) if ident.eq_ignore_ascii_case("left") => {
                Ok(SpecifiedFloat::Left)
            },
            Token::Ident(ref ident) if ident.eq_ignore_ascii_case("right") => {
                Ok(SpecifiedFloat::Right)
            },
            _ => Err(())
        }
    }
}

#[derive(Debug, Copy, Clone, PartialEq, Eq)]
pub enum SpecifiedClear {
    None,
    Left,
    Right,
    Both,
}

impl SpecifiedClear {
    pub fn parse(input: &mut Parser) -> Result<SpecifiedClear, ()> {
        match try!(input.next()) {
            Token::Ident(ref ident) if ident.eq_ignore_ascii_case("none") => {
                Ok(SpecifiedClear::None)
            },
            Token::Ident(ref ident) if ident.eq_ignore_ascii_case("left") => {
                Ok(SpecifiedClear::Left)
            },
            Token::Ident(ref ident) if ident.eq_ignore_ascii_case("right") => {
                Ok(SpecifiedClear::Right)
            },
            Token::Ident(ref ident) if ident.eq_ignore_ascii_case("both") => {
                Ok(SpecifiedClear::Both)
            },
            _ => Err(())
        }
    }
}

#[derive(Debug, Copy, Clone, PartialEq, Eq)]
pub enum SpecifiedWhiteSpace {
    Normal,
    Pre,
    Nowrap,
    PreWrap,
    PreLine,
}

impl SpecifiedWhiteSpace {
    pub fn parse(input: &mut Parser) -> Result<SpecifiedWhiteSpace, ()> {
        match try!(input.next()) {
            Token::Ident(ref ident) if ident.eq_ignore_ascii_case("normal") => {
                Ok(SpecifiedWhiteSpace::Normal)
            },
            Token::Ident(ref ident) if ident.eq_ignore_ascii_case("pre") => {
                Ok(SpecifiedWhiteSpace::Pre)
            },
            Token::Ident(ref ident) if ident.eq_ignore_ascii_case("nowrap") => {
                Ok(SpecifiedWhiteSpace::Nowrap)
            },
            Token::Ident(ref ident) if ident.eq_ignore_ascii_case("pre-wrap") => {
                Ok(SpecifiedWhiteSpace::PreWrap)
            },
            Token::Ident(ref ident) if ident.eq_ignore_ascii_case("pre-line") => {
                Ok(SpecifiedWhiteSpace::PreLine)
            },
            _ => Err(())
        }
    }
}

#[derive(Debug, Copy, Clone, PartialEq, Eq)]
pub enum SpecifiedCursor<I> {
    Custom(I),
    Predefined(Cursor),
}

impl<I> SpecifiedCursor<I> {
    /// Parses a single element of the `cursor` property.
    ///
    /// In order to parse the entire `cursor` property, you have to call this function for each
    /// of the comma-separated values until one of the calls returns `Ok`.
    pub fn parse<D>(input: &mut Parser, draw: &D) -> Result<SpecifiedCursor<I>, ()>
        where D: Draw<ImageResource = I>,
              I: Debug + Clone
    {
        match try!(input.next()) {
            Token::UnquotedUrl(val) => {
                match draw.parse_image(&val) {
                    Ok(img) => Ok(SpecifiedCursor::Custom(img)),
                    Err(_) => return Err(())
                }
            },
            Token::Ident(ref ident) if ident.eq_ignore_ascii_case("auto") => {
                Ok(SpecifiedCursor::Predefined(Cursor::Auto))
            },
            Token::Ident(ref ident) if ident.eq_ignore_ascii_case("crosshair") => {
                Ok(SpecifiedCursor::Predefined(Cursor::Crosshair))
            },
            Token::Ident(ref ident) if ident.eq_ignore_ascii_case("default") => {
                Ok(SpecifiedCursor::Predefined(Cursor::Default))
            },
            Token::Ident(ref ident) if ident.eq_ignore_ascii_case("pointer") => {
                Ok(SpecifiedCursor::Predefined(Cursor::Pointer))
            },
            Token::Ident(ref ident) if ident.eq_ignore_ascii_case("move") => {
                Ok(SpecifiedCursor::Predefined(Cursor::Move))
            },
            Token::Ident(ref ident) if ident.eq_ignore_ascii_case("e-resize") => {
                Ok(SpecifiedCursor::Predefined(Cursor::EResize))
            },
            Token::Ident(ref ident) if ident.eq_ignore_ascii_case("ne") => {
                Ok(SpecifiedCursor::Predefined(Cursor::Ne))
            },
            Token::Ident(ref ident) if ident.eq_ignore_ascii_case("resize") => {
                Ok(SpecifiedCursor::Predefined(Cursor::Resize))
            },
            Token::Ident(ref ident) if ident.eq_ignore_ascii_case("nw-resize") => {
                Ok(SpecifiedCursor::Predefined(Cursor::NwResize))
            },
            Token::Ident(ref ident) if ident.eq_ignore_ascii_case("n-resize") => {
                Ok(SpecifiedCursor::Predefined(Cursor::NResize))
            },
            Token::Ident(ref ident) if ident.eq_ignore_ascii_case("se-resize") => {
                Ok(SpecifiedCursor::Predefined(Cursor::SeResize))
            },
            Token::Ident(ref ident) if ident.eq_ignore_ascii_case("s-resize") => {
                Ok(SpecifiedCursor::Predefined(Cursor::SResize))
            },
            Token::Ident(ref ident) if ident.eq_ignore_ascii_case("w-resize") => {
                Ok(SpecifiedCursor::Predefined(Cursor::WResize))
            },
            Token::Ident(ref ident) if ident.eq_ignore_ascii_case("text") => {
                Ok(SpecifiedCursor::Predefined(Cursor::Text))
            },
            Token::Ident(ref ident) if ident.eq_ignore_ascii_case("wait") => {
                Ok(SpecifiedCursor::Predefined(Cursor::Wait))
            },
            Token::Ident(ref ident) if ident.eq_ignore_ascii_case("help") => {
                Ok(SpecifiedCursor::Predefined(Cursor::Help))
            },
            Token::Ident(ref ident) if ident.eq_ignore_ascii_case("progress") => {
                Ok(SpecifiedCursor::Predefined(Cursor::Progress))
            },
            _ => Err(())
        }
    }
}
