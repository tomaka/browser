use std::ascii::AsciiExt;
use std::str::FromStr;
use cssparser::Color;
use cssparser::Parser;
use cssparser::Token;

/// Describes the unit of a CSS length.
#[derive(Debug, Copy, Clone, PartialEq, Eq)]
pub enum CssUnit {
    Px,
    Em,
    Ex,
    In,
    Cm,
    Mm,
    Pt,
    Pc,
    Vw,
    Vh,
}

impl FromStr for CssUnit {
    type Err = ();  

    fn from_str(unit: &str) -> Result<CssUnit, ()> {
        if unit.eq_ignore_ascii_case("px") {
            Ok(CssUnit::Px)
        } else if unit.eq_ignore_ascii_case("em") {
            Ok(CssUnit::Em)
        } else if unit.eq_ignore_ascii_case("vw") {
            Ok(CssUnit::Vw)
        } else if unit.eq_ignore_ascii_case("vh") {
            Ok(CssUnit::Vh)
        } else if unit.eq_ignore_ascii_case("ex") {
            Ok(CssUnit::Ex)
        } else if unit.eq_ignore_ascii_case("in") {
            Ok(CssUnit::In)
        } else if unit.eq_ignore_ascii_case("cm") {
            Ok(CssUnit::Cm)
        } else if unit.eq_ignore_ascii_case("mm") {
            Ok(CssUnit::Mm)
        } else if unit.eq_ignore_ascii_case("pt") {
            Ok(CssUnit::Pt)
        } else if unit.eq_ignore_ascii_case("pc") {
            Ok(CssUnit::Pc)
        } else {
            Err(())
        }
    }
}
