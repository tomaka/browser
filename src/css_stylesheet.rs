use std::fmt::Debug;
use std::vec::IntoIter as VecIntoIter;
use smallvec::SmallVec;
use cssparser::Parser;
use cssparser::Token;
use css_selector::AttributeRequirement;
use css_selector::CssSelector;
use css_selector::SelectorElement;
use css_rule::CssRule;
use css_rules_set::CssRulesSet;
use draw::Draw;

/// Defines a whole CSS stylesheet.
#[derive(Debug, Clone)]
pub struct CssStylesheet<I, F> {
    blocks: Vec<Block<I, F>>,
}

#[derive(Debug, Clone)]
struct Block<I, F> {
    selector: CssSelector,
    rules: CssRulesSet<I, F>,
}

impl<I, F> CssStylesheet<I, F> where I: Clone, F: Clone {
    /// Parses a stylesheet from the CSS parser.
    pub fn parse<D>(input: &mut Parser, draw: &D) -> Result<CssStylesheet<I, F>, ()>
        where I: Debug,
              F: Debug,
              D: Draw<ImageResource = I, FontResource = F>
    {
        let mut blocks = Vec::new();

        loop {
            if input.is_exhausted() {
                break;
            }

            let mut selectors: SmallVec<[_; 6]> = SmallVec::new();
            loop {
                let selector = match CssSelector::parse(input) {
                    Ok(s) => s,
                    Err(_) => {
                        loop {
                            if input.is_exhausted() { return Ok(CssStylesheet { blocks: blocks }); }
                            if input.expect_curly_bracket_block().is_ok() { break; }
                        };
                        break;
                    }
                };

                selectors.push(selector);

                match try!(input.next()) {
                    Token::CurlyBracketBlock => break,
                    Token::Comma => continue,
                    _ => return Err(())
                };
            }

            if let Ok(rules) = input.parse_nested_block(|input| CssRulesSet::parse(input, draw)) {
                for selector in selectors.into_iter() {
                    blocks.push(Block {
                        selector: selector,
                        rules: rules.clone(),
                    });
                }
            }
        }

        Ok(CssStylesheet {
            blocks: blocks,
        })
    }

    pub fn query<N>(&self, node: N) -> QueryResults<N, I, F>
        where N: NodeInterface
    {
        let mut output = Vec::new();
        let mut if_hover_output = Vec::new();

        'blocks_iter: for block in self.blocks.iter() {
            // If `Some`, then this block only applies if the given node is hovered.
            //
            // For example, in `div:hover p`, the content of `if_hover` would be the `div`
            // while the current node is the `p`.
            //
            // You may wonder: but what about `div:hover p:hover a`? If the `p` is hovered,
            // then the `div` is necessarily hovered as well. Therefore we only keep the `p` in
            // a situation like this.
            let mut if_hover = None;

            let mut iterated_node = node.clone();

            for element in block.selector.iter().rev() {
                match element {
                    &SelectorElement::Element { ref tag_name, ref required_attributes, hover } => {
                        if hover && if_hover.is_none() {
                            if_hover = Some(iterated_node.clone());
                        }

                        if let &Some(ref t) = tag_name {
                            if *t != iterated_node.tag_name() {
                                continue 'blocks_iter;
                            }
                        }

                        for &(ref attr, ref req) in required_attributes.iter() {
                            match (req, iterated_node.attribute(attr)) {
                                (&AttributeRequirement::Exists, Some(_)) => (),
                                (&AttributeRequirement::Equal(ref req), Some(ref v)) if v == req => (),
                                (&AttributeRequirement::ContainsSpaceSeparated(_), _) => unimplemented!(),
                                (&AttributeRequirement::ContainsHyphenSeparated(_), _) => unimplemented!(),
                                _ => continue 'blocks_iter,
                            }
                        }
                    },
                    &SelectorElement::ChildCombinator => {

                    },
                    &SelectorElement::DescendantCombinator => {
                        iterated_node = match iterated_node.parent() {
                            Some(p) => p,
                            None => continue 'blocks_iter
                        };
                    },
                    &SelectorElement::SiblingCombinator => {
                    },
                }
            }

            if let Some(if_hover) = if_hover {
                for rule in block.rules.rules() {
                    if_hover_output.push((if_hover.clone(), rule.clone()));
                }
            } else {
                for rule in block.rules.rules() {
                    output.push(rule.clone());
                }
            }
        }

        QueryResults {
            rules: output.into_iter(),
            has_before_pseudo_element: false,
            has_after_pseudo_element: false,
            if_hovered: if_hover_output.into_iter(),
        }
    }
}

pub struct QueryResults<N, I, F> {
    pub rules: VecIntoIter<CssRule<I, F>>,       // TODO: other iterator
    pub has_before_pseudo_element: bool,
    pub has_after_pseudo_element: bool,
    pub if_hovered: VecIntoIter<(N, CssRule<I, F>)>,      // TODO: other iterators
}

pub trait NodeInterface: Clone {
    fn tag_name(&self) -> String;

    fn parent(&self) -> Option<Self>;

    fn attribute(&self, attr: &str) -> Option<String>;
}

#[cfg(test)]
mod tests {
    use cssparser::Parser;
    use css_stylesheet::CssStylesheet;
    use draw::DummyDraw;

    #[test]
    fn basic() {
        let mut parser = Parser::new("div > a { width: 5%; }");
        let stylesheet = CssStylesheet::parse(&mut parser, &DummyDraw).unwrap();
        assert!(parser.is_exhausted());
    }
}
