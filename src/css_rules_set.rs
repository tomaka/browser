use std::fmt::Debug;
use std::slice::Iter;
use smallvec::SmallVec;
use cssparser::Parser;
use css_rule::CssRule;
use draw::Draw;

/// Defines a set of one or multiple CSS rules.
///
/// For example `margin-top: 12px; width: 3%; line-height: 1.2;` is a rules set.
#[derive(Debug, Clone)]
pub struct CssRulesSet<I, F> {
    rules: SmallVec<[CssRule<I, F>; 4]>,
}

impl<I, F> CssRulesSet<I, F> {
    #[inline]
    pub fn empty() -> CssRulesSet<I, F> {
        CssRulesSet {
            rules: SmallVec::new()
        }
    }

    /// Parses a set of rules from the CSS parser.
    pub fn parse<D>(input: &mut Parser, draw: &D) -> Result<CssRulesSet<I, F>, ()>
         where D: Draw<ImageResource = I, FontResource = F>,
               I: Clone + Debug,
               F: Clone + Debug
    {
        let mut rules = SmallVec::new();

        while !input.is_exhausted() {
            if let Ok(rule) = CssRule::parse(input, draw) {
                for rule in rule {
                    rules.push(rule);
                }
            }
        }

        Ok(CssRulesSet {
            rules: rules
        })
    }

    /// Returns an iterator to the rules.
    #[inline]
    pub fn rules(&self) -> Iter<CssRule<I, F>> {
        self.rules.iter()
    }
}
