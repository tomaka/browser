use std::borrow::Cow;
use std::cell::RefCell;
use std::fmt;
use std::rc::Rc;
use std::rc::Weak;
use cssparser::Parser as CssParser;
use html5ever;
use html5ever::Attribute;
use html5ever::tendril::Tendril;
use html5ever::tendril::TendrilSink;
use html5ever::tendril::StrTendril;
use html5ever::tree_builder::NodeOrText;
use html5ever::tree_builder::QuirksMode;
use html5ever::tree_builder::TreeSink;
use smallvec::SmallVec;
use string_cache::QualName;     // https://github.com/servo/html5ever/issues/210

use css_rules_set::CssRulesSet;
use css_stylesheet::CssStylesheet;
use draw::Draw;
use style_tree::NodeId as StyleTreeNodeId;
use style_tree::StylesheetId;
use style_tree::StyleTree;

pub use style_tree::DrawIter;

/// Main struct of this library.
// Implementation note. The inner API of this library only allows creating nodes at fixed positions,
// while the external API and html5ever allow creating parent-free nodes and changing the parent of
// existing nodes. The `Document` struct's code is here only to handle these differences.
//
// It also handles interpreting the `<style>` tag and the `style` attribute.
pub struct Document<D> where D: Draw {
    // List of nodes.
    root: Rc<RefCell<Node<D>>>,

    // The inner tree that actually handles all the computation.
    tree: StyleTree<D>,

    draw: Rc<D>,
}

// FIXME FIXME FIXME FIXME
// Once the library no longer uses Rc, this can be removed
unsafe impl<D> Send for Document<D> where D: Draw + Send {}

impl<D> Document<D> where D: Draw {
    /// Builds a new document by parsing HTML code.
    pub fn parse(draw: D, html: &str, viewport_size: [u32; 2]) -> Document<D> {
        let empty = Document::empty(draw, viewport_size);
        let mut parser = html5ever::parse_document(empty, Default::default()).from_utf8();
        parser.process(Tendril::from_slice(html.as_bytes()));
        parser.finish()
    }

    /// Builds an empty document.
    pub fn empty(draw: D, viewport_size: [u32; 2]) -> Document<D> {
        let draw = Rc::new(draw);
        let tree = StyleTree::new(draw.clone(), viewport_size);

        Document {
            root: Rc::new(RefCell::new(Node {
                parent: None,
                children: SmallVec::new(),
                tag_name: None,
                attributes: Vec::new(),
                text: None,
                flow_id: Some(tree.root_node()),
                styles: CssRulesSet::empty(),
                stylesheet: None,
                stylesheet_id: None,
            })),
            tree: tree,
            draw: draw,
        }
    }

    /// Sets the pixel at which the cursor is located.
    ///
    /// You can also pass `None` if you know that the cursor is not over the document. The
    /// default value is `None`.
    #[inline]
    pub fn set_cursor_position(&mut self, pos: Option<[u32; 2]>) {
        self.tree.set_cursor_position(pos);
    }

    /// Changes the size of the viewport.
    #[inline]
    pub fn set_viewport_size(&mut self, viewport_size: [u32; 2]) {
        self.tree.set_viewport_size(viewport_size);
    }

    /// Returns the list of elements to draw in order to render the document.
    ///
    /// Note that calling `draw`, just like the other functions of this struct, may access the
    /// `Draw` implementation that was passed in the constructor.
    /// This is important to keep in mind if you use some sort of locking system, as you could
    /// trigger a deadlock.
    #[inline]
    pub fn draw(&mut self) -> DrawIter<D> {
        self.tree.draw()
    }
}

impl<D> fmt::Debug for Document<D> where D: Draw + fmt::Debug {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        f.debug_struct("Document")
            .field("root", &self.root)
            //.field("tree", &self.tree)
            .finish()
    }
}

#[doc(hidden)]
pub struct Node<D> where D: Draw {
    // Parent of the node, if any.
    parent: Option<Weak<RefCell<Node<D>>>>,

    // Children of the node, if any.
    children: SmallVec<[Rc<RefCell<Node<D>>>; 8]>,

    // The tag name, only for element nodes.
    tag_name: Option<QualName>,

    attributes: Vec<(String, String)>,

    // The text content, only for text nodes.
    text: Option<String>,

    // Identifier of the node in the actual tree.
    flow_id: Option<StyleTreeNodeId<D>>,

    // Parsed content of the `style` attribute.
    styles: CssRulesSet<D::ImageResource, D::FontResource>,

    // Stylesheet of this element. For `<style>` tags.
    stylesheet: Option<CssStylesheet<D::ImageResource, D::FontResource>>,

    // Stylesheet registered in the style tree.
    stylesheet_id: Option<StylesheetId<D>>,
}

impl<D> fmt::Debug for Node<D> where D: Draw + fmt::Debug {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        f.debug_struct("Node")
            .field("parent", &self.parent)
            .field("children", &self.children)
            .field("tag_name", &self.tag_name)
            .field("text", &self.text)
            .field("flow_id", &self.flow_id)
            .field("styles", &self.styles)
            .field("stylesheet", &self.stylesheet)
            .finish()
    }
}

impl<D> TreeSink for Document<D> where D: Draw {
    type Output = Self;
    type Handle = Rc<RefCell<Node<D>>>;

    #[inline]
    fn finish(self) -> Self::Output {
        self
    }

    #[inline]
    fn parse_error(&mut self, _msg: Cow<'static, str>) {}

    #[inline]
    fn get_document(&mut self) -> Self::Handle {
        self.root.clone()
    }

    #[inline]
    fn get_template_contents(&self, _target: Self::Handle) -> Self::Handle {
        unimplemented!()
    }

    #[inline]
    fn same_node(&self, x: Self::Handle, y: Self::Handle) -> bool {
        &*x as *const RefCell<_> == &*y as *const RefCell<_>
    }

    #[inline]
    fn elem_name(&self, target: Self::Handle) -> QualName {
        target.borrow().tag_name.as_ref().unwrap().clone()
    }

    #[inline]
    fn set_quirks_mode(&mut self, _mode: QuirksMode) {}

    #[inline]
    fn create_element(&mut self, name: QualName, attrs: Vec<Attribute>) -> Self::Handle {
        let mut styles = CssRulesSet::empty();
        for attr in attrs.iter() {
            if &attr.name.local == "style" {
                let attr = attr.value.to_string();
                let mut parser = CssParser::new(&attr);
                if let Ok(s) = CssRulesSet::parse(&mut parser, &*self.draw) {
                    styles = s;
                }
            }
        }

        Rc::new(RefCell::new(Node {
            parent: None,
            children: SmallVec::new(),
            tag_name: Some(name),
            attributes: attrs.iter().map(|a| (a.name.local.to_string(),
                                              a.value.to_string())).collect(),
            text: None,
            flow_id: None,
            styles: styles,
            stylesheet: None,
            stylesheet_id: None,
        }))
    }

    fn create_comment(&mut self, _text: StrTendril) -> Self::Handle {
        unimplemented!()
    }

    fn append(&mut self, parent: Self::Handle, child: NodeOrText<Self::Handle>) {
        match child {
            NodeOrText::AppendNode(node) => {
                assert!(node.borrow().flow_id.is_none());
                assert!(node.borrow().children.is_empty());     // not implemented
                node.borrow_mut().parent = Some(Rc::downgrade(&parent));
                parent.borrow_mut().children.push(node.clone());

                let mut node = node.borrow_mut();
                let tag_name = (*node.tag_name.as_ref().unwrap().local).to_owned();

                if let Some(ref flow_id) = parent.borrow().flow_id {
                    let new_flow_id = if let Some(ref text) = node.text {
                        self.tree.append_text_node(flow_id, &text)
                    } else {
                        self.tree.append_element_node(flow_id, &tag_name,
                                                      node.styles.rules().cloned(),
                                                      node.attributes.iter().cloned())
                    };

                    node.flow_id = Some(new_flow_id);
                }
            },

            NodeOrText::AppendText(text) => {
                if &parent.borrow().tag_name.as_ref().unwrap().local == "style" {
                    let mut parser = CssParser::new(&text);
                    if let Ok(stylesheet) = CssStylesheet::parse(&mut parser, &*self.draw) {
                        parent.borrow_mut().stylesheet = Some(stylesheet);
                    }
                }

                let new_node = Rc::new(RefCell::new(Node {
                    parent: Some(Rc::downgrade(&parent)),
                    children: SmallVec::new(),
                    tag_name: None,
                    attributes: Vec::new(),
                    text: Some((*text).to_owned()),
                    flow_id: None,
                    styles: CssRulesSet::empty(),
                    stylesheet: None,
                    stylesheet_id: None,
                }));

                parent.borrow_mut().children.push(new_node.clone());

                let stylesheet_id = if let Some(ref flow_id) = parent.borrow().flow_id {
                    let flow_id = self.tree.append_text_node(flow_id, &*text);
                    new_node.borrow_mut().flow_id = Some(flow_id);

                    let parent = parent.borrow();
                    if let Some(stylesheet) = parent.stylesheet.as_ref() {
                        Some(self.tree.add_stylesheet(stylesheet.clone()))
                    } else {
                        None
                    }
                } else {
                    None
                };

                parent.borrow_mut().stylesheet_id = stylesheet_id;
            },
        }
    }

    fn append_before_sibling(&mut self, _sibling: Self::Handle, _new_node: NodeOrText<Self::Handle>) -> Result<(), NodeOrText<Self::Handle>> {
        unimplemented!()
    }

    #[inline]
    fn append_doctype_to_document(&mut self, _name: StrTendril, _public_id: StrTendril,
                                  _system_id: StrTendril)
    {
    }

    fn add_attrs_if_missing(&mut self, _target: Self::Handle, _attrs: Vec<Attribute>) {
        unimplemented!()
    }

    fn remove_from_parent(&mut self, _target: Self::Handle) {
        /*self.tree.remove_from_parent(&self.nodes[target].flow_id);

        if let Some(previous_parent) = self.nodes[target].parent {
            match self.nodes[previous_parent].ty {
                NodeTy::Root { ref mut children } => {
                    let index = children.iter().position(|&n| n == target)
                                               .expect("Parent-children mismatch in DOM");
                    children.remove(index);                    
                },
                NodeTy::Element { ref mut children, .. } => {
                    let index = children.iter().position(|&n| n == target)
                                               .expect("Parent-children mismatch in DOM");
                    children.remove(index);                    
                },
                _ => panic!()
            }

        }

        self.nodes[target].parent = None;*/
        unimplemented!()
    }

    fn reparent_children(&mut self, _node: Self::Handle, _new_parent: Self::Handle) {
        unimplemented!()
    }

    #[inline]
    fn mark_script_already_started(&mut self, _node: Self::Handle) {}
}

#[cfg(test)]
mod tests {
    use document::Document;
    use draw::Draw;

    #[test]
    fn doc_impl_send() {
        fn need_send<T: Send>(_: &T) {}
        fn interm<T: Send + Draw>(d: &Document<T>) { need_send(d) }
    }
}
