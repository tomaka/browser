use std::fmt;
use block::BlockFlow;
use computed::Display;
use computed::LengthPercentage;
use computed::LengthPercentageAuto;
use computed::Position;
use computed::TextAlign;
use draw::Draw;
use inline::InlineFlow;
use specified::SpecifiedWhiteSpace as WhiteSpace;

/// Trait implemented on objects that can build a layout from a hierarchy of nodes. For example,
/// a block flow, an inline flow, a table, a flex box, etc.
///
/// If you implement `Flow`, you probably also want to implement `FlowDraw`.
pub trait Flow {
    /// Identifier of a node.
    type NodeId;

    /// Custom data that the flow user can store inside a node.
    type CustomData: Clone;

    /// The `Draw` object from which the fonts and images have been obtained.
    type Draw: Draw;

    /// Properties of an element node.
    type Properties;

    /// Returns the identifier of the root element.
    ///
    /// Use this function to add nodes as children of the root.
    fn root(&self) -> Self::NodeId;

    /// Returns the width and height of the border box of the root element.
    fn root_border_box_dimensions(&self) -> [f32; 2];

    /// Returns the id of the node at the given position.
    ///
    /// The position is relative to the content box of the root of the flow.
    // TODO: find a way to ignore nodes whose visibility is hidden.
    fn node_at_position(&self, pos: [f32; 2]) -> Option<(Self::NodeId, Self::CustomData)>;

    /// Creates a new element node and adds it as the last child of the given parent.
    ///
    /// # Panic
    ///
    /// - Panicks if `parent` longer exists.
    ///
    fn append_element_node(&mut self, parent: &Self::NodeId,
                           properties: &Self::Properties, data: Self::CustomData)
                           -> Self::NodeId;

    /// Creates a new node whose content is a replaced element, and adds it as the last child
    /// of the given parent.
    ///
    /// # Panic
    ///
    /// - Panicks if `parent` longer exists.
    ///
    fn append_replaced_content_node(&mut self, parent: &Self::NodeId,
                                    content: <Self::Draw as Draw>::ImageResource,
                                    properties: &Self::Properties, data: Self::CustomData)
                                    -> Self::NodeId { unimplemented!() }

    /// Creates a new text node and adds it as the last child of the given parent.
    ///
    /// # Panic
    ///
    /// - Panicks if `parent` longer exists.
    ///
    fn append_text_node(&mut self, parent: &Self::NodeId, text: String, data: Self::CustomData)
                        -> Self::NodeId;

    /// Changes the properties of an element node.
    ///
    /// # Panic
    ///
    /// - Panicks if `node` is a text node.
    /// - Panicks if `node` longer exists.
    ///
    fn set_element_properties(&mut self, node: &Self::NodeId, properties: &Self::Properties);

    /// Destroys the given node and all its children.
    ///
    /// # Panic
    ///
    /// - Panicks if trying to delete the root node.
    ///
    fn delete_node(&mut self, node: &Self::NodeId);
}

/// Extension for `Flow`. This is a separate trait because `DrawIter` may want to hold a
/// reference to `Self`. Therefore you may want to implement `Flow` on `T` and `FlowDraw`
/// on `&mut T` for example.
/// Once HKTs are available in Rust, these two traits should become one.
pub trait FlowDraw {
    /// The `Draw` object from which the fonts and images have been obtained.
    type Draw: Draw;

    /// Custom data that the flow user can store inside a node.
    type CustomData: Clone;

    /// Iterator to the list of elements to draw alongside with the custom data of the node.
    type DrawIter: Iterator<Item = (LayoutElement<<Self::Draw as Draw>::ImageResource,
                                                  <Self::Draw as Draw>::FontVariant>,
                                    Self::CustomData)>;

    /// Produces an iterator of the elements to draw.
    fn draw(self) -> Self::DrawIter;
}

/// A single element to render.
// TODO: change the template parameter to take a `Draw` for more encapsulation
#[derive(Debug, Clone)]
pub enum LayoutElement<Img, Font> {
    /// A glyph.
    Text {
        /// The glyph.
        text: char,
        /// The font to draw with.
        font: Font,

        /// Number of pixels from the origin of the viewport to the left border of the glyph.
        left_coordinate: f32,
        /// Number of pixels from the origin of the viewport to the top border of the glyph.
        top_coordinate: f32,
        /// Width in pixels of the glyph.
        width: f32,
        /// Height in pixels of the glyph.
        height: f32,
    },

    /// A block.
    Block {
        /// Number of pixels from the origin of the viewport to the left border of the rectangle.
        left_coordinate: f32,
        /// Number of pixels from the origin of the viewport to the top border of the rectangle.
        top_coordinate: f32,
        /// Width in pixels of the rectangle.
        width: f32,
        /// Height in pixels of the rectangle.
        height: f32,
    },

    /// A replaced content.
    ReplacedContent {
        /// The image to draw.
        image: Img,

        /// Number of pixels from the origin of the viewport to the left border of the rectangle.
        left_coordinate: f32,
        /// Number of pixels from the origin of the viewport to the top border of the rectangle.
        top_coordinate: f32,
        /// Width in pixels of the rectangle.
        width: f32,
        /// Height in pixels of the rectangle.
        height: f32,
    },
}

impl<Img, Font> LayoutElement<Img, Font> {
    /// Adds a value to the coordinates of the element.
    pub fn apply_translation(&mut self, translation: [f32; 2]) {
        match self {
            &mut LayoutElement::Text { ref mut left_coordinate, ref mut top_coordinate, .. } => {
                *left_coordinate += translation[0];
                *top_coordinate += translation[1];
            },
            &mut LayoutElement::Block { ref mut left_coordinate, ref mut top_coordinate, .. } => {
                *left_coordinate += translation[0];
                *top_coordinate += translation[1];
            },
            &mut LayoutElement::ReplacedContent { ref mut left_coordinate,
                                                  ref mut top_coordinate, .. } =>
            {
                *left_coordinate += translation[0];
                *top_coordinate += translation[1];
            },
        }
    }
}

/// The computed values of a node of the flow.
// TODO: rename to InFlowNodeProperties
pub struct NodeProperties<D> where D: Draw {
    pub display: Display,

    pub width: LengthPercentageAuto,
    pub height: LengthPercentageAuto,

    pub padding_top: LengthPercentage,
    pub padding_right: LengthPercentage,
    pub padding_bottom: LengthPercentage,
    pub padding_left: LengthPercentage,

    pub margin_top: LengthPercentageAuto,
    pub margin_right: LengthPercentageAuto,
    pub margin_bottom: LengthPercentageAuto,
    pub margin_left: LengthPercentageAuto,

    pub border_top_width: f32,
    pub border_right_width: f32,
    pub border_bottom_width: f32,
    pub border_left_width: f32,

    pub font_size: f32,
    pub font_family: D::FontResource,
    pub text_align: TextAlign,
    pub white_space: WhiteSpace,

    /* TODO:
    BorderCollapse,
    BorderSpacing,
    CaptionSide,
    Clear,
    Clip,
    Direction,
    Display,
    EmptyCells,
    Float,
    FontStyle,
    FontVariant,
    FontWeight,
    LetterSpacing,
    LineHeight,
    MaxHeight,
    MaxWidth,
    MinHeight,
    MinWidth,
    Overflow,
    TableLayout,
    TextIndent,
    UnicodeBidi,
    VerticalAlign,
    WordSpacing,
    ZIndex,*/
}

impl<D> fmt::Debug for NodeProperties<D> where D: Draw {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        f.debug_struct("NodeProperties")
            .field("display", &self.display)
            .field("width", &self.width)
            .field("height", &self.height)
            .field("padding_top", &self.padding_top)
            .field("padding_right", &self.padding_right)
            .field("padding_bottom", &self.padding_bottom)
            .field("padding_left", &self.padding_left)
            .field("margin_top", &self.margin_top)
            .field("margin_right", &self.margin_right)
            .field("margin_bottom", &self.margin_bottom)
            .field("margin_left", &self.margin_left)
            .field("border_top_width", &self.border_top_width)
            .field("border_right_width", &self.border_right_width)
            .field("border_bottom_width", &self.border_bottom_width)
            .field("border_left_width", &self.border_left_width)
            .field("font_size", &self.font_size)
            .field("font_family", &self.font_family)
            .field("text_align", &self.text_align)
            .finish()
    }
}

// *grumble* A manual implementation is required, or Rust will only implement Clone if D
// implements Clone.
impl<D> Clone for NodeProperties<D> where D: Draw {
    fn clone(&self) -> NodeProperties<D> {
        NodeProperties {
            display: self.display.clone(),
            width: self.width.clone(),
            height: self.height.clone(),
            padding_top: self.padding_top.clone(),
            padding_right: self.padding_right.clone(),
            padding_bottom: self.padding_bottom.clone(),
            padding_left: self.padding_left.clone(),
            margin_top: self.margin_top.clone(),
            margin_right: self.margin_right.clone(),
            margin_bottom: self.margin_bottom.clone(),
            margin_left: self.margin_left.clone(),
            border_top_width: self.border_top_width.clone(),
            border_right_width: self.border_right_width.clone(),
            border_bottom_width: self.border_bottom_width.clone(),
            border_left_width: self.border_left_width.clone(),
            font_size: self.font_size.clone(),
            font_family: self.font_family.clone(),
            text_align: self.text_align.clone(),
            white_space: self.white_space.clone(),
        }
    }
}

/// The computed values of a node of the flow.
// TODO: rename to NodeProperties
pub struct NodePropertiesWithPosition<D> where D: Draw {
    pub properties: NodeProperties<D>,

    pub position: Position,
    pub top: LengthPercentageAuto,
    pub right: LengthPercentageAuto,
    pub bottom: LengthPercentageAuto,
    pub left: LengthPercentageAuto,
}

impl<D> fmt::Debug for NodePropertiesWithPosition<D> where D: Draw {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        f.debug_struct("NodePropertiesWithPosition")
            .field("properties", &self.properties)
            .field("position", &self.position)
            .field("top", &self.top)
            .field("right", &self.right)
            .field("bottom", &self.bottom)
            .field("left", &self.left)
            .finish()
    }
}

// *grumble* A manual implementation is required, or Rust will only implement Clone if D
// implements Clone.
impl<D> Clone for NodePropertiesWithPosition<D> where D: Draw {
    fn clone(&self) -> NodePropertiesWithPosition<D> {
        NodePropertiesWithPosition {
            properties: self.properties.clone(),
            position: self.position.clone(),
            top: self.top.clone(),
            right: self.right.clone(),
            bottom: self.bottom.clone(),
            left: self.left.clone(),
        }
    }
}

/// Abstraction over all possible flows.
pub enum AbstractFlow<C, D> where C: Clone, D: Draw {
    Block(BlockFlow<C, D>),
    Inline(InlineFlow<C, D>),
}

impl<C, D> From<BlockFlow<C, D>> for AbstractFlow<C, D> where C: Clone, D: Draw {
    #[inline]
    fn from(flow: BlockFlow<C, D>) -> AbstractFlow<C, D> {
        AbstractFlow::Block(flow)
    }
}

impl<C, D> From<InlineFlow<C, D>> for AbstractFlow<C, D> where C: Clone, D: Draw {
    #[inline]
    fn from(flow: InlineFlow<C, D>) -> AbstractFlow<C, D> {
        AbstractFlow::Inline(flow)
    }
}

impl<C, D> Flow for AbstractFlow<C, D> where C: Clone, D: Draw {
    type NodeId = AbstractFlowNodeId<C, D>;
    type CustomData = C;
    type Draw = D;
    type Properties = NodeProperties<D>;

    #[inline]
    fn root(&self) -> Self::NodeId {
        match self {
            &AbstractFlow::Block(ref flow) => {
                AbstractFlowNodeId(AbstractFlowNodeIdInner::Block(flow.root()))
            },
            &AbstractFlow::Inline(ref flow) => {
                AbstractFlowNodeId(AbstractFlowNodeIdInner::Inline(flow.root()))
            },
        }
    }

    #[inline]
    fn root_border_box_dimensions(&self) -> [f32; 2] {
        match self {
            &AbstractFlow::Block(ref flow) => flow.root_border_box_dimensions(),
            &AbstractFlow::Inline(ref flow) => flow.root_border_box_dimensions(),
        }
    }

    #[inline]
    fn node_at_position(&self, pos: [f32; 2]) -> Option<(Self::NodeId, Self::CustomData)> {
        match self {
            &AbstractFlow::Block(ref flow) => {
                flow.node_at_position(pos).map(|(n, c)| {
                    (AbstractFlowNodeId(AbstractFlowNodeIdInner::Block(n)), c)
                })
            },
            &AbstractFlow::Inline(ref flow) => {
                flow.node_at_position(pos).map(|(n, c)| {
                    (AbstractFlowNodeId(AbstractFlowNodeIdInner::Inline(n)), c)
                })
            },
        }
    }

    #[inline]
    fn append_element_node(&mut self, parent: &Self::NodeId,
                           properties: &NodeProperties<Self::Draw>, data: Self::CustomData)
                           -> Self::NodeId
    {
        match (self, parent) {
            (&mut AbstractFlow::Block(ref mut flow),
             &AbstractFlowNodeId(AbstractFlowNodeIdInner::Block(ref parent))) =>
            {
                let n = flow.append_element_node(parent, properties, data);
                AbstractFlowNodeId(AbstractFlowNodeIdInner::Block(n))
            },
            (&mut AbstractFlow::Inline(ref mut flow),
             &AbstractFlowNodeId(AbstractFlowNodeIdInner::Inline(ref parent))) =>
            {
                let n = flow.append_element_node(parent, properties, data);
                AbstractFlowNodeId(AbstractFlowNodeIdInner::Inline(n))
            },
            _ => panic!()
        }
    }

    #[inline]
    fn append_replaced_content_node(&mut self, parent: &Self::NodeId,
                                    content: <Self::Draw as Draw>::ImageResource,
                                    properties: &NodeProperties<Self::Draw>,
                                    data: Self::CustomData)
                                    -> Self::NodeId
    {
        match (self, parent) {
            (&mut AbstractFlow::Block(ref mut flow),
             &AbstractFlowNodeId(AbstractFlowNodeIdInner::Block(ref parent))) =>
            {
                let n = flow.append_replaced_content_node(parent, content, properties, data);
                AbstractFlowNodeId(AbstractFlowNodeIdInner::Block(n))
            },
            (&mut AbstractFlow::Inline(ref mut flow),
             &AbstractFlowNodeId(AbstractFlowNodeIdInner::Inline(ref parent))) =>
            {
                let n = flow.append_replaced_content_node(parent, content, properties, data);
                AbstractFlowNodeId(AbstractFlowNodeIdInner::Inline(n))
            },
            _ => panic!()
        }
    }

    #[inline]
    fn append_text_node(&mut self, parent: &Self::NodeId, text: String, data: Self::CustomData)
                        -> Self::NodeId
    {
        match (self, parent) {
            (&mut AbstractFlow::Block(ref mut flow),
             &AbstractFlowNodeId(AbstractFlowNodeIdInner::Block(ref parent))) =>
            {
                let n = flow.append_text_node(parent, text, data);
                AbstractFlowNodeId(AbstractFlowNodeIdInner::Block(n))
            },
            (&mut AbstractFlow::Inline(ref mut flow),
             &AbstractFlowNodeId(AbstractFlowNodeIdInner::Inline(ref parent))) =>
            {
                let n = flow.append_text_node(parent, text, data);
                AbstractFlowNodeId(AbstractFlowNodeIdInner::Inline(n))
            },
            _ => panic!()
        }
    }

    #[inline]
    fn set_element_properties(&mut self, node: &Self::NodeId,
                              properties: &NodeProperties<Self::Draw>)
    {
        match (self, node) {
            (&mut AbstractFlow::Block(ref mut flow),
             &AbstractFlowNodeId(AbstractFlowNodeIdInner::Block(ref node))) =>
            {
                flow.set_element_properties(node, properties)
            },
            (&mut AbstractFlow::Inline(ref mut flow),
             &AbstractFlowNodeId(AbstractFlowNodeIdInner::Inline(ref node))) =>
            {
                flow.set_element_properties(node, properties)
            },
            _ => panic!()
        }
    }

    #[inline]
    fn delete_node(&mut self, node: &Self::NodeId) {
        match (self, node) {
            (&mut AbstractFlow::Block(ref mut flow),
             &AbstractFlowNodeId(AbstractFlowNodeIdInner::Block(ref node))) =>
            {
                flow.delete_node(node)
            },
            (&mut AbstractFlow::Inline(ref mut flow),
             &AbstractFlowNodeId(AbstractFlowNodeIdInner::Inline(ref node))) =>
            {
                flow.delete_node(node)
            },
            _ => panic!()
        }
    }
}

impl<'a, C, D> FlowDraw for &'a mut AbstractFlow<C, D> where C: Clone + 'a, D: Draw + 'a {
    type Draw = D;
    type CustomData = C;
    type DrawIter = AbstractFlowDrawIter<'a, C, D>;

    #[inline]
    fn draw(self) -> Self::DrawIter {
        match self {
            &mut AbstractFlow::Block(ref mut flow) => {
                AbstractFlowDrawIter(AbstractFlowDrawIterInner::Block(flow.draw()))
            },
            &mut AbstractFlow::Inline(ref mut flow) => {
                AbstractFlowDrawIter(AbstractFlowDrawIterInner::Inline(flow.draw()))
            },
        }
    }
}

/// Draw iterator for `AbstractFlow`.
pub struct AbstractFlowDrawIter<'a, C, D>(AbstractFlowDrawIterInner<'a, C, D>)
    where C: Clone + 'a, D: Draw + 'a;

impl<'a, C, D> Iterator for AbstractFlowDrawIter<'a, C, D>
    where C: Clone + 'a, D: Draw + 'a
{
    type Item = (LayoutElement<D::ImageResource, D::FontVariant>, C);

    #[inline]
    fn next(&mut self) -> Option<Self::Item> {
        match self {
            &mut AbstractFlowDrawIter(AbstractFlowDrawIterInner::Block(ref mut iter)) => {
                iter.next()
            },
            &mut AbstractFlowDrawIter(AbstractFlowDrawIterInner::Inline(ref mut iter)) => {
                iter.next()
            },
        }
    }

    #[inline]
    fn size_hint(&self) -> (usize, Option<usize>) {
        match self {
            &AbstractFlowDrawIter(AbstractFlowDrawIterInner::Block(ref iter)) => {
                iter.size_hint()
            },
            &AbstractFlowDrawIter(AbstractFlowDrawIterInner::Inline(ref iter)) => {
                iter.size_hint()
            },
        }
    }
}

enum AbstractFlowDrawIterInner<'a, C, D> where C: Clone + 'a, D: Draw + 'a {
    Block(<&'a mut BlockFlow<C, D> as FlowDraw>::DrawIter),
    Inline(<&'a mut InlineFlow<C, D> as FlowDraw>::DrawIter),
}

/// Node id for `AbstractFlow`.
pub struct AbstractFlowNodeId<C, D>(AbstractFlowNodeIdInner<C, D>) where C: Clone, D: Draw;

enum AbstractFlowNodeIdInner<C, D> where C: Clone, D: Draw {
    Block(<BlockFlow<C, D> as Flow>::NodeId),
    Inline(<InlineFlow<C, D> as Flow>::NodeId),
}
