use std::fmt::Debug;

/// Interface that can be queried by the library for various info about the environment.
pub trait Draw {
    /// Implementation-specific type that represents an image that is guaranteed to be valid.
    type ImageResource: Debug + Clone;

    /// Implementation-specific type that represents a font that is guaranteed to be valid.
    type FontResource: Debug + Clone;

    /// Implementation-specific type that represents a specific variant of a font.
    ///
    /// The difference between `FontVariant` and `FontResource` is that `FontResource` typically
    /// contains the name of the font, while `FontVariant` contains the name, the boldness,
    /// the "italicness", etc. of the font.
    ///
    /// While building a `FontResource` can fail if the font isn't recognized, building a
    /// `FontVariant` never fails. It is the role of the implementation to handle the fact
    /// that a specific variant of the font may not exist.
    type FontVariant: Debug + Clone;

    /// Parses the name of an image. Returns `Err` if the image is not recognized.
    ///
    /// While this concept is named `image`, it does not have to actually be an *image*. The
    /// parameter of this function is directly equal to the content of the `src` property of
    /// `img` elements and to the content of `url()` for background images. You are
    /// therefore free to support whatever value and whatever protocol you want.
    ///
    /// For example you can put `<img src="special://secondary-camera" />` in your HTML code,
    /// and tweak your implementation of `Draw` so that it recognizes the value
    /// "special://secondary-camera" as the location where to put the rendering of the secondary
    /// camera of your game. The possibilities are endless.
    fn parse_image(&self, url: &str) -> Result<Self::ImageResource, ()>;

    /// Returns the intrinsic dimensions of the image. In other words the dimensions of the image.
    fn intrinsic_dimensions(&self, image: &Self::ImageResource) -> [f32; 2];

    /// Parses the name of a font. Returns `Err` if the font is not recognized.
    fn parse_font(&self, url: &str) -> Result<Self::FontResource, ()>;

    /// Returns the default font, to be used if the CSS doesn't specify any font or if `parse_font`
    /// returns `Err` for all the possible fonts.
    fn default_font(&self) -> Self::FontResource;

    /// Given a font, returns an object that represents a specific variant of that font.
    ///
    /// See the documentation of `FontVariant`.
    // TODO: font-style, etc.
    fn font_variant(&self, font: &Self::FontResource, font_weight: i32) -> Self::FontVariant;

    /// Returns the information about a single glyph of text.
    fn glyph_infos(&self, font: &Self::FontVariant, glyph: char) -> GlyphInfos;

    /// Returns the height of a line divided by the size of an EM.
    fn line_height(&self, font: &Self::FontVariant) -> f32;

    /// Returns the kerning value to apply to the second character, divided by the size of an EM.
    ///
    /// A positive value means that the second character moves away from the first one. A
    /// negative value means that the second character moves towards the first one.
    fn kerning(&self, font: &Self::FontVariant, first_char: char, second_char: char) -> f32;
}

/// A single element to render.
// TODO: change the template parameter to take a `Draw` for more encapsulation
#[derive(Debug, Clone, PartialEq)]
pub enum DrawProperties<Img, Font> {
    /// Draws a glyph.
    Text {
        /// The glyph.
        text: char,
        /// The font to draw with.
        font: Font,
        /// The RGBA color of the glpyh.
        color: [f32; 4],

        /// Number of pixels from the origin of the viewport to the left border of the glyph.
        left_coordinate: f32,
        /// Number of pixels from the origin of the viewport to the top border of the glyph.
        top_coordinate: f32,

        /// Width in pixels of the glyph.
        width: f32,
        /// Height in pixels of the glyph.
        height: f32,
    },

    /// Draws a rectangle that contains a specific color.
    Color {
        /// The color to draw.
        color: [f32; 4],

        /// Number of pixels from the origin of the viewport to the left border of the rectangle.
        left_coordinate: f32,
        /// Number of pixels from the origin of the viewport to the top border of the rectangle.
        top_coordinate: f32,
        /// Width in pixels of the rectangle.
        width: f32,
        /// Height in pixels of the rectangle.
        height: f32,
    },

    /// Draws a rectangle that contains an image.
    ///
    /// Note that the dimensions of the image can be different from the dimensions of the
    /// rectangle. If the image is smaller than the rectangle, it should repeat. If the image
    /// is larger than the rectangle, the overflow should not be visible.
    Image {
        /// The image to draw.
        image: Img,

        /// Width of the image in pixels.
        image_width: f32,
        /// Height of the image in pixels.
        image_height: f32,

        /// Number of pixels from the origin of the viewport to the left border of the rectangle.
        left_coordinate: f32,
        /// Number of pixels from the origin of the viewport to the top border of the rectangle.
        top_coordinate: f32,
        /// Width in pixels of the rectangle.
        width: f32,
        /// Height in pixels of the rectangle.
        height: f32,
    },

    /// Draws the mouse cursor at the given location.
    ///
    /// This element is only present if one of the predefined cursors is used. If the CSS rules
    /// specify a custom cursor, then the custom cursor will be parsed and will be drawn as if it
    /// was a regular image.
    Cursor {
        ///  The cursor to draw.
        cursor: Cursor,

        /// Position of the cursor in number of pixels from the top-left hand corner.
        position: [f32; 2],
    },
}

#[derive(Debug, Copy, Clone, PartialEq, Eq)]
pub enum Cursor {
    Auto,
    Crosshair,
    Default,
    Pointer,
    Move,
    EResize,
    Ne,
    Resize,
    NwResize,
    NResize,
    SeResize,
    SResize,
    WResize,
    Text,
    Wait,
    Help,
    Progress,
}

/// Information about a single glyph.
///
/// All the values of this struct must be relative to the size of an EM, so that the library can
/// adjust the values to any size.
pub struct GlyphInfos {
    /// Width of the glyph in pixels, divided by the number of pixels of an EM.
    pub width: f32,

    /// Height of the glyph in pixels, divided by the number of pixels of an EM.
    ///
    /// By definition, this value is supposed to be always 1.0 for the glyph 'M'. In practice this
    /// is not always exactly true.
    pub height: f32,

    /// Number of pixels from the end of the previous glyph to the start of this one, divided by
    /// the number of pixels of an EM.
    pub x_offset: f32,

    /// Number of pixels from the base of the line to the top of this one, divided by the number
    /// of pixels of an EM.
    ///
    /// For glyphs that don't go under the line (like 'm' or 'u' for example), this is equal to
    /// the height of the glyph. For glyphs that go under the line (like 'p' or 'g'), this is
    /// equal to the height of the glyph minus the portion that goes under the line.
    pub y_offset: f32,

    /// Number of pixels from the end of the previous glyph to the end of this one, divided by
    /// the number of pixels of an EM.
    ///
    /// Should always be superior to `width + x_offset`.
    pub x_advance: f32,
}

#[derive(Debug)]
pub struct DummyDraw;

impl Draw for DummyDraw {
    type ImageResource = ();
    type FontResource = ();
    type FontVariant = ();

    fn parse_image(&self, _: &str) -> Result<Self::ImageResource, ()> {
        Ok(())
    }

    fn parse_font(&self, _: &str) -> Result<Self::FontResource, ()> {
        Ok(())
    }

    #[inline]
    fn font_variant(&self, _: &Self::FontResource, _: i32) -> Self::FontVariant {
        ()
    }

    #[inline]
    fn intrinsic_dimensions(&self, _: &Self::ImageResource) -> [f32; 2] {
        [128.0, 128.0]
    }

    fn default_font(&self) -> Self::FontResource {
        ()
    }

    fn glyph_infos(&self, _: &Self::FontResource, _: char) -> GlyphInfos {
        GlyphInfos {
            width: 1.0,
            height: 1.0,
            x_offset: 0.0,
            x_advance: 1.0,
            y_offset: 1.0,
        }
    }

    #[inline]
    fn line_height(&self, _: &()) -> f32 {
        1.2
    }

    #[inline]
    fn kerning(&self, _: &(), _: char, _: char) -> f32 {
        0.0
    }
}
