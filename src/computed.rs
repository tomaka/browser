pub use specified::SpecifiedBorderStyle as BorderStyle;
pub use specified::SpecifiedColor as Color;
pub use specified::SpecifiedDisplay as Display;
pub use specified::SpecifiedPosition as Position;
pub use specified::SpecifiedTextAlign as TextAlign;
pub use specified::SpecifiedVisibility as Visibility;

#[derive(Debug, Clone)]
pub enum LengthPercentageAuto {
    Length { pixels: f32 },
    Percentage { percents: f32 },
    Auto,
}

#[derive(Debug, Clone)]
pub enum LengthPercentage {
    Length { pixels: f32 },
    Percentage { percents: f32 },
}
