use std::cell::RefCell;
use std::fmt;
use std::rc::Rc;
use std::rc::Weak;
use std::vec::IntoIter as VecIntoIter;

use computed::Display;
use computed::LengthPercentage;
use computed::LengthPercentageAuto;
use computed::Position;
use draw::Draw;
use flow::Flow;
use flow::FlowDraw;
use flow::LayoutElement;
use flow::NodeProperties;
use inline::InlineFlow;
use inline::RootComputedValues as InlineFlowRootComputedValues;

/// Represents the root of a block flow and all its children.
pub struct BlockFlow<C, D> where C: Clone, D: Draw {
    draw: Rc<D>,
    root: Rc<RefCell<Node<C, D>>>,
}

impl<C, D> BlockFlow<C, D> where C: Clone, D: Draw {
    #[inline]
    pub fn new(draw: Rc<D>, root_custom_data: C, root_properties: NodeProperties<D>)
               -> BlockFlow<C, D>
    {
        BlockFlow {
            draw: draw.clone(),
            root: Rc::new(RefCell::new(Node::Element {
                parent: None,
                children: Vec::new(),
                properties: root_properties,
                custom_data: root_custom_data,
                used_border_left_offset: 0.0,
                used_border_top_offset: 0.0,
                used_border_box_width: 0.0,
                used_border_box_height: 0.0,
                used_content_left_offset: 0.0,
                used_content_top_offset: 0.0,
                used_content_box_width: 0.0,
                used_content_box_height: 0.0,
            }))
        }
    }

    /// Checks whether `node` contains `pos`, and if not iterates over its children to check.
    fn node_at_position_recursive(&self, org_node: &Rc<RefCell<Node<C, D>>>, pos: [f32; 2])
                                  -> Option<(Rc<RefCell<Node<C, D>>>, C)>
    {
        let node = org_node.borrow();

        if let Node::Element { used_border_left_offset, used_border_top_offset,
                               used_border_box_width, used_border_box_height,
                               ref children, ref custom_data, .. } = *node
        {
            for child in children.iter() {
                match *child {
                    Child::Node(ref child) => {
                        if let Some(r) = self.node_at_position_recursive(child, pos) {
                            return Some(r);
                        }
                    },
                    Child::InlineFlow(ref flow) => {
                        // FIXME:
                    }
                }
            }
            
            if used_border_left_offset <= pos[0] &&
               used_border_left_offset + used_border_box_width >= pos[0] &&
               used_border_top_offset <= pos[1] &&
               used_border_top_offset + used_border_box_height >= pos[1]
            {
                return Some((org_node.clone(), custom_data.clone()));
            }

            None

        } else {
            unreachable!()
        }
    }

    fn draw_recursive(&self, node: &Rc<RefCell<Node<C, D>>>,
                      out: &mut Vec<(LayoutElement<D::ImageResource, D::FontVariant>, C)>)
    {
        match *node.borrow_mut() {
            Node::Element { ref children, ref properties,
                            ref used_border_left_offset, ref used_border_top_offset,
                            ref used_border_box_width, ref used_border_box_height,
                            ref used_content_left_offset, ref used_content_top_offset,
                            ref custom_data, .. } =>
            {
                out.push((LayoutElement::Block {
                    left_coordinate: *used_border_left_offset,
                    top_coordinate: *used_border_top_offset,
                    width: *used_border_box_width,
                    height: *used_border_box_height,
                }, custom_data.clone()));

                for child in children.iter() {
                    match child {
                        &Child::Node(ref child) => {
                            self.draw_recursive(child, out);
                        },
                        &Child::InlineFlow(ref flow) => {
                            let mut flow = flow.borrow_mut();
                            for (mut render, custom) in flow.draw() {
                                render.apply_translation([
                                    *used_content_left_offset,
                                    *used_content_top_offset
                                ]);
                                out.push((render, custom));
                            }
                        },
                    }
                }
            },

            Node::InlineChild { .. } => unreachable!(),
        }
    }

    // Refreshes the used values of all the nodes of this flow.
    #[inline]
    fn refresh_used_values(&self) {
        let (cb_w, cb_h) = {
            let root = self.root.borrow();

            if let Node::Element { ref properties, .. } = *root {
                let w = match properties.width {
                    LengthPercentageAuto::Length { pixels } => Some(pixels),
                    _ => None,
                };

                let h = match properties.height {
                    LengthPercentageAuto::Length { pixels } => Some(pixels),
                    _ => None,
                };

                (w, h)

            } else {
                unreachable!()
            }
        };

        // TODO: pass correct max_width/max_height values
        self.refresh_used_values_inner(&self.root, [0.0, 0.0], cb_w, cb_h, cb_w, cb_h);
    }

    // Takes a node and the dimensions of its containing box. Updates the node's used values and
    // returns its margin box's dimensions.
    fn refresh_used_values_inner(&self, node: &Rc<RefCell<Node<C, D>>>,
                                 parent_content_box_offset: [f32; 2],
                                 containing_box_width: Option<f32>,
                                 containing_box_height: Option<f32>,
                                 containing_box_max_width: Option<f32>,
                                 containing_box_max_height: Option<f32>) -> [f32; 2]
    {
        let mut node = node.borrow_mut();
        match *node {
            Node::Element { ref children, ref properties, 
                            ref mut used_border_left_offset, ref mut used_border_top_offset,
                            ref mut used_border_box_width, ref mut used_border_box_height,
                            ref mut used_content_left_offset, ref mut used_content_top_offset,
                            ref mut used_content_box_width, ref mut used_content_box_height,
                            .. } =>
            {
                // We start by resolving some properties that use percentages.

                // Width/height, top/right/bottom/left, and margins can be either `Some`
                // or `None`.
                let width = match properties.width {
                    LengthPercentageAuto::Length { pixels } => Some(pixels),
                    LengthPercentageAuto::Percentage { percents } => {
                        if let Some(cb_w) = containing_box_width {
                            Some(percents * cb_w)
                        } else {
                            None        // technically undefined, but we implement it as Auto
                        }
                    },
                    LengthPercentageAuto::Auto => None,
                };

                let margin_left = match properties.margin_left {
                    LengthPercentageAuto::Length { pixels } => Some(pixels),
                    LengthPercentageAuto::Percentage { percents } => {
                        if let Some(cb_w) = containing_box_width {
                            Some(percents * cb_w)
                        } else {
                            None
                        }
                    },
                    LengthPercentageAuto::Auto => None,
                };

                let margin_right = match properties.margin_right {
                    LengthPercentageAuto::Length { pixels } => Some(pixels),
                    LengthPercentageAuto::Percentage { percents } => {
                        if let Some(cb_w) = containing_box_width {
                            Some(percents * cb_w)
                        } else {
                            None
                        }
                    },
                    LengthPercentageAuto::Auto => None,
                };

                let height = match properties.height {
                    LengthPercentageAuto::Length { pixels } => Some(pixels),
                    LengthPercentageAuto::Percentage { percents } => {
                        if let Some(cb_h) = containing_box_height {
                            Some(percents * cb_h)
                        } else {
                            None
                        }
                    },
                    LengthPercentageAuto::Auto => None,
                };

                let margin_top = match properties.margin_top {
                    LengthPercentageAuto::Length { pixels } => Some(pixels),
                    LengthPercentageAuto::Percentage { percents } => {
                        if let Some(cb_w) = containing_box_width {      // not a mistake
                            Some(percents * cb_w)
                        } else {
                            None
                        }
                    },
                    LengthPercentageAuto::Auto => None,
                };

                let margin_bottom = match properties.margin_bottom {
                    LengthPercentageAuto::Length { pixels } => Some(pixels),
                    LengthPercentageAuto::Percentage { percents } => {
                        if let Some(cb_w) = containing_box_width {      // not a mistake
                            Some(percents * cb_w)
                        } else {
                            None
                        }
                    },
                    LengthPercentageAuto::Auto => None,
                };

                // Padding values are always calculated now.
                let padding_left = match properties.padding_left {
                    LengthPercentage::Length { pixels } => pixels,
                    LengthPercentage::Percentage { percents } => {
                        if let Some(cb_w) = containing_box_width {
                            percents * cb_w
                        } else {
                            0.0     // undefined by the specs
                        }
                    },
                };

                let padding_right = match properties.padding_right {
                    LengthPercentage::Length { pixels } => pixels,
                    LengthPercentage::Percentage { percents } => {
                        if let Some(cb_w) = containing_box_width {
                            percents * cb_w
                        } else {
                            0.0     // undefined by the specs
                        }
                    },
                };

                let padding_top = match properties.padding_top {
                    LengthPercentage::Length { pixels } => pixels,
                    LengthPercentage::Percentage { percents } => {
                        if let Some(cb_w) = containing_box_width {      // not a mistake
                            percents * cb_w
                        } else {
                            0.0     // undefined by the specs
                        }
                    },
                };

                let padding_bottom = match properties.padding_bottom {
                    LengthPercentage::Length { pixels } => pixels,
                    LengthPercentage::Percentage { percents } => {
                        if let Some(cb_w) = containing_box_width {      // not a mistake
                            percents * cb_w
                        } else {
                            0.0     // undefined by the specs
                        }
                    },
                };

                let border_top_width = properties.border_top_width;
                let border_left_width = properties.border_left_width;
                let border_right_width = properties.border_right_width;
                let border_bottom_width = properties.border_bottom_width;

                let (margin_left, width, margin_right) = match (margin_left, width, margin_right,
                                                                containing_box_width)
                {
                    (Some(ml), Some(w), Some(mr), Some(cb_w)) => {
                        let sum = w + ml + padding_right + padding_left + border_left_width +
                                  border_right_width;
                        let mr = if sum + mr < cb_w { mr }
                                 else if sum < cb_w { cb_w - sum }
                                 else { 0.0 };
                        (ml, w, mr)
                    },
                    (Some(ml), Some(w), Some(mr), _) => (ml, w, mr),
                    (Some(ml), Some(w), None, Some(cb_w)) => {
                        let mr = cb_w - w - ml - padding_right - padding_left - border_left_width -
                                 border_right_width;
                        let mr = if !(mr >= 0.0) { 0.0 } else { mr };
                        (ml, w, mr)
                    },
                    (None, Some(w), Some(mr), Some(cb_w)) => {
                        let ml = cb_w - w - mr - padding_right - padding_left - border_left_width -
                                 border_right_width;
                        let ml = if !(ml >= 0.0) { 0.0 } else { ml };
                        (ml, w, mr)
                    },
                    (Some(ml), None, Some(mr), Some(cb_w)) => {
                        let w = cb_w - ml - mr - padding_right - padding_left - border_left_width -
                                border_right_width;
                        let w = if !(w >= 0.0) { 0.0 } else { w };
                        (ml, w, mr)
                    },
                    (None, Some(w), None, Some(cb_w)) => {
                        let sum = w + padding_right + padding_left + border_left_width +
                                  border_right_width;
                        let m = (cb_w - sum) / 2.0;
                        (m, w, m)
                    },
                    (None, None, None, Some(cb_w)) => {
                        let sum = padding_right + padding_left + border_left_width +
                                  border_right_width;
                        (0.0, cb_w - sum, 0.0)
                    },
                    (_, _, _, None) => {
                        (0.0, 10.0, 0.0)     // FIXME: dummy
                    },
                    _ => unimplemented!()
                };

                let margin_top = margin_top.unwrap_or(0.0);
                let margin_bottom = margin_bottom.unwrap_or(0.0);

                *used_border_left_offset = parent_content_box_offset[0] + margin_left;
                *used_border_top_offset = parent_content_box_offset[1] + margin_top;

                *used_content_left_offset = *used_border_left_offset + border_left_width +
                                            padding_left;
                *used_content_top_offset = *used_border_top_offset + border_top_width +
                                           padding_top;

                let mut y_offset = 0.0;
                for child in children.iter() {
                    match child {
                        &Child::Node(ref child) => {
                            let dims = self.refresh_used_values_inner(child, [*used_content_left_offset,
                                                                      *used_content_top_offset + y_offset],
                                                                      Some(width), height.map(|h| h - y_offset),
                                                                      None, None);      // TODO: wrong
                            y_offset += dims[1];
                        },
                        &Child::InlineFlow(ref flow) => {
                            let mut flow = flow.borrow_mut();
                            flow.set_root_content_box_dimensions([width, height.unwrap_or(10000.0 /* FIXME: */)]);
                            y_offset += flow.height();
                        },
                    }
                }

                let height = match height {
                    Some(h) => h,
                    None => y_offset,
                };

                *used_content_box_width = width;
                *used_content_box_height = height;

                *used_border_box_width = width + padding_left + padding_right + border_left_width +
                                         border_right_width;
                *used_border_box_height = *used_content_box_height + padding_top + padding_bottom +
                                          border_top_width + border_bottom_width;

                [*used_border_box_width + margin_left + margin_right,
                 *used_border_box_height + margin_top + margin_bottom]
            },
            _ => unreachable!()
        }
    }
}

impl<C, D> fmt::Debug for BlockFlow<C, D> where C: Clone + fmt::Debug, D: Draw + fmt::Debug {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "BlockFlow {{ root: {:?} }}", self.root)
    }
}

impl<C, D> Flow for BlockFlow<C, D> where C: Clone, D: Draw {
    type NodeId = Rc<RefCell<Node<C, D>>>;
    type CustomData = C;
    type Draw = D;
    type Properties = NodeProperties<D>;

    #[inline]
    fn root(&self) -> Self::NodeId {
        self.root.clone()
    }

    #[inline]
    fn root_border_box_dimensions(&self) -> [f32; 2] {
        let root = self.root.borrow();
        if let &Node::Element { used_border_box_width, used_border_box_height, .. } = &*root {
            [used_border_box_width, used_border_box_height]
        } else {
            panic!()
        }
    }

    #[inline]
    fn node_at_position(&self, pos: [f32; 2]) -> Option<(Self::NodeId, C)> {
        self.node_at_position_recursive(&self.root, pos)
    }

    fn append_element_node(&mut self, parent: &Self::NodeId, new_properties: &NodeProperties<D>,
                           custom_data: C) -> Self::NodeId
    {
        assert!(new_properties.display != Display::None);

        let is_inline = new_properties.display == Display::Inline ||
                        new_properties.display == Display::InlineBlock;

        let result = match *parent.borrow_mut() {
            Node::Element { ref mut children, ref properties,
                            custom_data: ref parent_custom_data, .. } =>
            {
                if is_inline {
                    // Get the last child. If it is not an inline flow, create a new one and add it
                    // as last child.
                    let inline_flow = {
                        let flow = match children.last() {
                            Some(&Child::InlineFlow(ref flow)) => Some(flow.clone()),
                            _ => None
                        };

                        if let Some(flow) = flow {
                            flow
                        } else {
                            let new_flow = InlineFlow::new(self.draw.clone(),
                                                           parent_custom_data.clone(),
                                                           InlineFlowRootComputedValues {
                                                               content_box_width: 100.0,       // TODO:
                                                               content_box_height: 100.0,       // TODO:
                                                               font_family: new_properties.font_family.clone(),
                                                               font_size: new_properties.font_size.clone(),
                                                               white_space: new_properties.white_space.clone(),
                                                           });
                            let new_flow = Rc::new(RefCell::new(new_flow));

                            children.push(Child::InlineFlow(new_flow.clone()));
                            new_flow
                        }
                    };

                    let root = inline_flow.borrow().root();
                    let inner_node = inline_flow.borrow_mut()
                                                .append_element_node(&root, new_properties,
                                                                     custom_data);

                    Rc::new(RefCell::new(Node::InlineChild {
                        parent: Some(Rc::downgrade(parent)),
                        flow: inline_flow.clone(),
                        inner: inner_node,
                    }))

                } else {
                    // Regular child.
                    let node = Rc::new(RefCell::new(Node::Element {
                        parent: Some(Rc::downgrade(&parent)),
                        children: Vec::new(),
                        properties: new_properties.clone(),
                        custom_data: custom_data,
                        used_border_left_offset: 0.0,
                        used_border_top_offset: 0.0,
                        used_border_box_width: 0.0,
                        used_border_box_height: 0.0,
                        used_content_left_offset: 0.0,
                        used_content_top_offset: 0.0,
                        used_content_box_width: 0.0,
                        used_content_box_height: 0.0,
                    }));

                    children.push(Child::Node(node.clone()));
                    node
                }
            },

            // The new parent is already an InlineChild, so we create a new node in the inline
            // flow.
            Node::InlineChild { ref parent, ref flow, ref inner } => {
                assert!(is_inline);     // TODO: implement that

                let inner_node = flow.borrow_mut()
                                     .append_element_node(inner, new_properties, custom_data);

                Rc::new(RefCell::new(Node::InlineChild {
                    parent: parent.clone(),
                    flow: flow.clone(),
                    inner: inner_node,
                }))
            },
        };

        self.refresh_used_values();

        result
    }

    fn append_text_node(&mut self, parent: &Self::NodeId, text: String, custom_data: C)
                        -> Self::NodeId
    {
        let result = match *parent.borrow_mut() {
            Node::Element { ref mut children, ref properties,
                            custom_data: ref parent_custom_data, .. } =>
            {
                // Get the last child. If it is not an inline flow, create a new one and add it
                // as last child.
                let inline_flow = {
                    let flow = match children.last() {
                        Some(&Child::InlineFlow(ref flow)) => Some(flow.clone()),
                        _ => None
                    };

                    if let Some(flow) = flow {
                        flow
                    } else {
                        let new_flow = InlineFlow::new(self.draw.clone(),
                                                       parent_custom_data.clone(),
                                                       InlineFlowRootComputedValues {
                                                           content_box_width: 100.0,       // TODO:
                                                           content_box_height: 100.0,       // TODO:
                                                           font_family: properties.font_family.clone(),
                                                           font_size: properties.font_size.clone(),
                                                           white_space: properties.white_space.clone(),
                                                       });
                        let new_flow = Rc::new(RefCell::new(new_flow));

                        children.push(Child::InlineFlow(new_flow.clone()));
                        new_flow
                    }
                };

                // Add the new node to this flow.
                let root = inline_flow.borrow().root();
                let inner_node = inline_flow.borrow_mut().append_text_node(&root, text, custom_data);

                Rc::new(RefCell::new(Node::InlineChild {
                    parent: Some(Rc::downgrade(&parent)),
                    flow: inline_flow,
                    inner: inner_node,
                }))
            },

            // The new parent is already an InlineChild, so we create a new node in the inline
            // flow.
            Node::InlineChild { ref parent, ref flow, ref inner } => {
                let inner_node = flow.borrow_mut().append_text_node(inner, text, custom_data);

                Rc::new(RefCell::new(Node::InlineChild {
                    parent: parent.clone(),
                    flow: flow.clone(),
                    inner: inner_node,
                }))
            },
        };

        self.refresh_used_values();

        result
    }

    fn set_element_properties(&mut self, node: &Self::NodeId, new_properties: &NodeProperties<D>) {
        match *node.borrow_mut() {
            Node::Element { ref mut properties, .. } => {
                assert_eq!(properties.display, new_properties.display); // display change not supported
                *properties = new_properties.clone()
            },
            Node::InlineChild { ref flow, ref inner, .. } => {
                flow.borrow_mut().set_element_properties(inner, new_properties);
            },
        };

        self.refresh_used_values();
    }

    #[inline]
    fn delete_node(&mut self, node: &Self::NodeId) {
        unimplemented!()
    }
}

impl<'a, C, D> FlowDraw for &'a mut BlockFlow<C, D> where C: Clone, D: Draw {
    type CustomData = C;
    type Draw = D;
    type DrawIter = VecIntoIter<(LayoutElement<D::ImageResource, D::FontVariant>, C)>;

    #[inline]
    fn draw(self) -> Self::DrawIter {
        let mut result = Vec::new();
        self.draw_recursive(&self.root, &mut result);
        result.into_iter()
    }
}

enum Node<C, D> where C: Clone, D: Draw {
    // The node is a proper element.
    Element {
        parent: Option<Weak<RefCell<Node<C, D>>>>,
        children: Vec<Child<C, D>>,
        properties: NodeProperties<D>,
        custom_data: C,

        used_border_left_offset: f32,
        used_border_top_offset: f32,
        used_border_box_width: f32,
        used_border_box_height: f32,
        used_content_left_offset: f32,
        used_content_top_offset: f32,
        used_content_box_width: f32,
        used_content_box_height: f32,
    },

    // The node is a child contained within an InlineFlow.
    InlineChild {
        parent: Option<Weak<RefCell<Node<C, D>>>>,
        // The flow in question.
        flow: Rc<RefCell<InlineFlow<C, D>>>,
        // Node ID.
        inner: <InlineFlow<C, D> as Flow>::NodeId,
    }
}

impl<C, D> fmt::Debug for Node<C, D> where C: Clone + fmt::Debug, D: Draw + fmt::Debug {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match *self {
            Node::Element { ref children, ref properties, .. } => {
                f.debug_struct("Node::Element")
                    .field("children", children)
                    .field("properties", properties)
                    .finish()
            },
            Node::InlineChild { ref inner, .. } => {
                f.debug_struct("Node::InlineChild")
                    .field("inner", inner)
                    .finish()
            },
        }
    }
}

enum Child<C, D> where C: Clone, D: Draw {
    Node(Rc<RefCell<Node<C, D>>>),
    InlineFlow(Rc<RefCell<InlineFlow<C, D>>>),
}

impl<C, D> Clone for Child<C, D> where C: Clone, D: Draw {
    fn clone(&self) -> Child<C, D> {
        match *self {
            Child::Node(ref node) => Child::Node(node.clone()),
            Child::InlineFlow(ref flow) => Child::InlineFlow(flow.clone()),
        }
    }
}

impl<C, D> fmt::Debug for Child<C, D> where C: Clone + fmt::Debug, D: Draw + fmt::Debug {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match *self {
            Child::Node(ref node) => {
                f.debug_tuple("Child::Node")
                    .field(node)
                    .finish()
            },
            Child::InlineFlow(ref flow) => {
                f.debug_tuple("Child::InlineFlow")
                    .field(flow)
                    .finish()
            },
        }
    }
}
