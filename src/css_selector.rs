use std::slice::Iter;
use smallvec::SmallVec;
use cssparser::Parser;
use cssparser::Token;

/// Defines a CSS selector.
///
/// For example `div > a` is a selector.
#[derive(Debug, Clone)]
pub struct CssSelector {
    elements: SmallVec<[SelectorElement; 6]>,
}

impl CssSelector {
    /// Parses a selector from the CSS parser.
    ///
    /// Stops at either the first `{` or the first `,`.
    pub fn parse(input: &mut Parser) -> Result<CssSelector, ()> {
        let mut elements: SmallVec<[SelectorElement; 6]> = SmallVec::new();

        // This variable holds whether or not the next non-combinator token (eg. an ident,
        // a `#foo`, a `.bar`, etc.) should create a new element. If false, it should apply to the
        // previous one.
        let mut ready_for_next_element = true;

        loop {
            let next_elem = input.try(|input| {
                match try!(input.next_including_whitespace()) {
                    Token::CurlyBracketBlock => Err(()),
                    Token::Comma => Err(()),
                    token => Ok(token),
                }
            });

            match next_elem {
                Ok(Token::Ident(s)) => {
                    assert!(ready_for_next_element);
                    ready_for_next_element = false;

                    elements.push(SelectorElement::Element {
                        tag_name: Some(s.into_owned()),
                        required_attributes: SmallVec::new(),
                        hover: false,
                    });
                },
                Ok(Token::Hash(s)) | Ok(Token::IDHash(s)) => {
                    match elements.last_mut() {
                        Some(&mut SelectorElement::Element { ref mut required_attributes, .. }) => {
                            // TODO: check duplicates
                            required_attributes.push(("id".to_owned(), AttributeRequirement::Equal(s.into_owned())));
                            continue;
                        },
                        _ => ()
                    };

                    let mut req = SmallVec::new();
                    req.push(("id".to_owned(), AttributeRequirement::Equal(s.into_owned())));
                    elements.push(SelectorElement::Element {
                        tag_name: None,
                        required_attributes: req,
                        hover: false,
                    });
                },
                Ok(Token::Delim('>')) => {
                    if elements.last().map(|e| e.is_element()).unwrap_or(false) {
                        elements.push(SelectorElement::ChildCombinator);
                        ready_for_next_element = true;
                    } else {
                        return Err(());
                    }
                },
                Ok(Token::Delim('+')) => {
                    if elements.last().map(|e| e.is_element()).unwrap_or(false) {
                        elements.push(SelectorElement::SiblingCombinator);
                        ready_for_next_element = true;
                    } else {
                        return Err(());
                    }
                },
                Ok(Token::WhiteSpace(_)) => {
                    ready_for_next_element = true;
                },
                Ok(Token::Colon) => {
                    match input.next_including_whitespace() {
                        Ok(Token::Ident(ref ident)) if ident == "hover" => {
                            match elements.last_mut() {
                                Some(&mut SelectorElement::Element { ref mut hover, .. }) => {
                                    *hover = true;
                                    continue;
                                },
                                _ => ()
                            };
                        },
                        _ => return Err(())
                    }
                },
                Ok(token) => { println!("failed to parse selector token {:?}", token); return Err(()) },
                Err(_) => break,
            };
        }

        if elements.is_empty() {
            return Err(());
        }

        Ok(CssSelector {
            elements: elements,
        })
    }

    #[inline]
    pub fn iter(&self) -> Iter<SelectorElement> {
        self.elements.iter()
    }
}

#[derive(Debug, Clone)]
pub enum SelectorElement {
    Element {
        tag_name: Option<String>,
        required_attributes: SmallVec<[(String, AttributeRequirement); 2]>,
        hover: bool,
    },

    ChildCombinator,
    DescendantCombinator,
    SiblingCombinator,
}

impl SelectorElement {
    #[inline]
    pub fn is_element(&self) -> bool {
        match *self {
            SelectorElement::Element { .. } => true,
            _ => false
        }
    }
}

#[derive(Debug, Clone)]
pub enum AttributeRequirement {
    Exists,
    Equal(String),
    ContainsSpaceSeparated(String),
    ContainsHyphenSeparated(String),
}

#[cfg(test)]
mod tests {
    use cssparser::Parser;
    use cssparser::Token;
    use css_selector::CssSelector;
    use css_selector::SelectorElement;

    /*#[test]
    fn basic() {
        let mut parser = Parser::new("div > a");
        let selector = CssSelector::parse(&mut parser).unwrap();

        assert!(parser.is_exhausted());
        assert_eq!(selector.elements[0], SelectorElement::TagName("div".to_owned()));
        assert_eq!(selector.elements[1], SelectorElement::ChildCombinator);
        assert_eq!(selector.elements[2], SelectorElement::TagName("a".to_owned()));
    }

    #[test]
    fn parser_position() {
        // We test that the parser is at `{` after parsing the selector.
        let mut parser = Parser::new("div { test }");
        let selector = CssSelector::parse(&mut parser).unwrap();

        assert_eq!(parser.next(), Ok(Token::CurlyBracketBlock));
        assert_eq!(selector.elements[0], SelectorElement::TagName("div".to_owned()));
    }*/
}
