use std::collections::HashMap;
use std::hash::BuildHasherDefault;
use std::fmt;
use std::mem;
use std::rc::Rc;
use std::slice::Iter as SliceIter;
use std::vec::IntoIter as VecIntoIter;
use fnv::FnvHasher;
use smallvec::SmallVec;

use computed::Display;
use draw::Draw;
use flow::Flow;
use flow::FlowDraw;
use flow::LayoutElement;
use flow::NodeProperties;
use specified::SpecifiedWhiteSpace;

/// Represents the root of an inline flow and all its content.
///
/// Things like margin, padding, etc. or the root are not handled. The code assumes that we are
/// within the content box of the inline node.
///
/// Only inline (or inline-something) nodes must be added, otherwise it will panic.
pub struct InlineFlow<C, D> where C: Clone, D: Draw {
    // Storage of the `Draw` object. Used primarily to determine the information about glyphs.
    draw: Rc<D>,

    // Storage for the nodes. The key `0` is always the root node.
    nodes: HashMap<u32, DomNode<C, D>, BuildHasherDefault<FnvHasher>>,

    // The characteristics of the root node.
    root_computed_values: RootComputedValues<D::FontResource>,

    // Identifier of the next assignment to `nodes`. Should be checked for overflows.
    next_id: u32,

    // Cache for the line boxes. Flushed and rebuilt entirely when a modification is made to
    // the content.
    line_boxes: Option<Vec<LineBox<C, D>>>,
}

impl<C, D> InlineFlow<C, D> where C: Clone, D: Draw {
    #[inline]
    pub fn new(draw: Rc<D>, root_custom_data: C, properties: RootComputedValues<D::FontResource>)
               -> InlineFlow<C, D>
    {
        let mut nodes = HashMap::with_hasher(BuildHasherDefault::<FnvHasher>::default());
        nodes.insert(0, DomNode {
            parent: None,
            children: Vec::new(),
            inner: DomNodeInner::Root,
            custom_data: root_custom_data,
        });

        InlineFlow {
            draw: draw,
            nodes: nodes,
            root_computed_values: properties,
            next_id: 1,
            line_boxes: None,
        }
    }

    /// Returns the width of the content of the flow. Always inferior or equal to
    /// `content_box_width`.
    pub fn width(&mut self) -> f32 {
        if self.line_boxes.is_none() {
            self.refresh();
        }

        unimplemented!()
    }

    /// Returns the height of the content of the flow.
    pub fn height(&mut self) -> f32 {
        if self.line_boxes.is_none() {
            self.refresh();
        }

        self.line_boxes.as_ref().unwrap().iter().map(|l| l.height).fold(0.0, |a,b| a+b)
    }

    /// Changes the dimensions of the content box of the root node.
    pub fn set_root_content_box_dimensions(&mut self, dimensions: [f32; 2]) {
        // Don't do anything if the values don't change. Avoids an unnecessary refresh.
        if self.root_computed_values.content_box_width == dimensions[0] &&
           self.root_computed_values.content_box_height == dimensions[1]
        {
            return;
        }

        self.root_computed_values.content_box_width = dimensions[0];
        self.root_computed_values.content_box_height = dimensions[1];
        self.line_boxes = None;
    }

    /// Rebuilds all the line boxes.
    fn refresh(&mut self) {
        let lines = {
            let mut current_line_width = 0.0;       // TODO: text-indent
            let mut lines = Vec::new();
            let mut current_line = LineBox { content: Vec::new(), height: 0.0 };
            let mut current_word: SmallVec<[Character<_, _>; 16]> = SmallVec::new();
            let mut current_word_width = 0.0;
            let mut previous_character = None;
            let mut previous_character_is_whitespace = false;

            // We build a linear list of all text nodes and their properties thanks to
            // `get_nodes_list()` and iterate over it.
            for (text_node, properties) in self.get_nodes_list() {
                // Determine various information about the node.

                let text = match text_node.inner {
                    DomNodeInner::Text(ref text) => text,
                    _ => panic!()
                };

                let font_family = match properties {
                    Some(ref props) => props.font_family.clone(),
                    None => self.root_computed_values.font_family.clone()
                };

                let font = self.draw.font_variant(&font_family, 500 /* TODO */);

                let font_size = match properties {
                    Some(ref props) => props.font_size.clone(),
                    None => self.root_computed_values.font_size.clone()
                };

                let (collapse_ws, line_breaks, preserve_crlf) = {
                    let white_space = match properties {
                        Some(ref props) => props.white_space,
                        None => self.root_computed_values.white_space,
                    };

                    match white_space {
                        SpecifiedWhiteSpace::Normal => (true, true, false),
                        SpecifiedWhiteSpace::Pre => (false, false, true),
                        SpecifiedWhiteSpace::Nowrap => (true, false, false),
                        SpecifiedWhiteSpace::PreWrap => (false, true, true),
                        SpecifiedWhiteSpace::PreLine => (true, true, true),
                    }
                };

                // TODO: line-height property
                let line_height = self.draw.line_height(&font) * font_size;

                for character in text.chars() {     // TODO: should handle unicode properly eventually
                    if current_line.height < line_height {
                        current_line.height = line_height;
                    }

                    // CRLF is always collapsed.
                    if character == '\n' && previous_character == Some('\r') { continue; }

                    let is_newline = character == '\r' || character == '\n';     // TODO: there are other characters
                    let is_whitespace = is_newline || character == ' ' || character == '\t';    // TODO: there are other characters

                    // Jumping to next line if we have a newline character.
                    if is_newline && preserve_crlf {
                        // Adding word to current line first.
                        for mut chr in current_word.into_iter() {
                            chr.offset[0] += current_line_width;
                            current_line.content.push(chr);
                        }
                        current_word_width = 0.0;

                        let finished_line = mem::replace(&mut current_line,
                                                         LineBox { content: Vec::new(), height: 0.0 });
                        lines.push(finished_line);
                        current_line_width = 0.0;
                    }

                    // Skipping consecutive white spaces if we collapse.
                    if collapse_ws && is_whitespace && previous_character_is_whitespace {
                        continue;
                    }
                    previous_character_is_whitespace = is_whitespace;

                    // Computing some info about the glyph.
                    let glyph_infos = self.draw.glyph_infos(&font, if is_whitespace { ' ' }
                                                                   else { character });
                    let kerning = if let Some(prev) = previous_character {
                        self.draw.kerning(&font, prev, character) * font_size
                    } else {
                        0.0
                    };
                    previous_character = Some(character);
                    let x_advance = glyph_infos.x_advance * font_size *
                                    if character == '\t' && !collapse_ws { 8.0 } else { 1.0 } + kerning;


                    if is_whitespace {
                        // Jumping to next line if necessary.
                        if line_breaks && current_line_width + current_word_width >=
                                          self.root_computed_values.content_box_width
                        {
                            let finished_line = mem::replace(&mut current_line,
                                                             LineBox { content: Vec::new(), height: 0.0 });
                            lines.push(finished_line);
                            current_line_width = 0.0;
                        }

                        // Adding word to current line.
                        for mut chr in current_word.into_iter() {
                            chr.offset[0] += current_line_width;
                            current_line.content.push(chr);
                        }
                        current_line_width += current_word_width;
                        current_word_width = 0.0;

                        // And add the whitespace as well. We only add it if we're not the first
                        // character or this line or if we don't collapse whitespaces.
                        if current_line_width != 0.0 || !collapse_ws {
                            current_line_width += x_advance;
                        }

                    } else {
                        current_word.push(Character {
                            dom_node_id: 0,     // FIXME:
                            custom_data: self.nodes[&0].custom_data.clone(),     // FIXME:
                            text: character,
                            font: font.clone(),
                            offset: [current_word_width + glyph_infos.x_offset * font_size + kerning,
                                     line_height - glyph_infos.y_offset * font_size /* FIXME: vertical-align */],
                            size: [glyph_infos.width * font_size, glyph_infos.height * font_size],
                        });

                        current_word_width += x_advance;
                    }
                }
            }

            for mut chr in current_word.into_iter() {
                chr.offset[0] += current_line_width;
                current_line.content.push(chr);
            }

            if !current_line.content.is_empty() {
                lines.push(current_line);
            }

            lines
        };

        self.line_boxes = Some(lines);
    }

    /// Produces an iterator to the list of nodes to be rendered, in order.
    ///
    /// The iterator produces a list of text nodes and their associated properties. All the nodes
    /// must have their DomNodeInner set to Text.
    // TODO: doesn't handle padding/margin ; should return special values indicating when we enter
    //       and when we leave a margin/padding
    // TODO: better iterator type?
    #[inline]
    fn get_nodes_list(&self) -> VecIntoIter<(&DomNode<C, D>, Option<&NodeProperties<D>>)> {
        fn add<'a, C, D>(nodes: &'a HashMap<u32, DomNode<C, D>, BuildHasherDefault<FnvHasher>>,
                         node: u32, out: &mut Vec<(&'a DomNode<C, D>, Option<&'a NodeProperties<D>>)>)
            where C: Clone, D: Draw
        {
            let node = nodes.get(&node).unwrap();

            let properties = match node.inner {
                DomNodeInner::Root => None,
                DomNodeInner::Element(ref props) => Some(props),
                DomNodeInner::Text(_) => panic!()
            };

            for &child in node.children.iter() {
                let child_node = nodes.get(&child).unwrap();
                if let DomNodeInner::Text(_) = child_node.inner {
                    out.push((child_node, properties));
                } else {
                    add(nodes, child, out);
                }
            }
        }

        let mut out = Vec::new();
        add(&self.nodes, 0, &mut out);
        out.into_iter()
    }
}

impl<C, D> fmt::Debug for InlineFlow<C, D> where C: Clone + fmt::Debug, D: Draw + fmt::Debug {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        f.debug_struct("InlineFlow")
            .field("draw", &self.draw)
            .field("nodes", &self.nodes)
            .field("line_boxes", &self.line_boxes)
            .field("next_id", &self.next_id)
            .finish()
    }
}

impl<C, D> Flow for InlineFlow<C, D> where C: Clone, D: Draw {
    type NodeId = u32;
    type CustomData = C;
    type Draw = D;
    type Properties = NodeProperties<D>;

    #[inline]
    fn root(&self) -> Self::NodeId {
        0
    }

    #[inline]
    fn root_border_box_dimensions(&self) -> [f32; 2] {
        unimplemented!()
    }

    fn node_at_position(&self, pos: [f32; 2]) -> Option<(u32, C)> {
        None    // FIXME:
    }

    fn append_element_node(&mut self, parent: &Self::NodeId, properties: &NodeProperties<D>,
                           custom_data: C) -> Self::NodeId
    {
        assert!(properties.display != Display::None);
        assert!(properties.display == Display::Inline ||
                properties.display == Display::InlineBlock);

        let new_id = self.next_id;
        self.next_id = self.next_id.checked_add(1).unwrap();

        self.nodes.insert(new_id, DomNode {
            parent: None,
            children: Vec::new(),
            inner: DomNodeInner::Element(properties.clone()),
            custom_data: custom_data,
        });

        self.nodes.get_mut(&parent).expect("Invalid parent ID")
                  .children.push(new_id);

        // Flush cached line boxes.
        self.line_boxes = None;

        new_id
    }

    fn append_text_node(&mut self, parent: &Self::NodeId, text: String, custom_data: C)
                        -> Self::NodeId
    {
        let new_id = self.next_id;
        self.next_id = self.next_id.checked_add(1).unwrap();

        self.nodes.insert(new_id, DomNode {
            parent: None,
            children: Vec::new(),
            inner: DomNodeInner::Text(text),
            custom_data: custom_data,
        });

        self.nodes.get_mut(&parent).expect("Invalid parent ID")
                  .children.push(new_id);

        // Flush cached line boxes.
        self.line_boxes = None;

        new_id
    }

    fn set_element_properties(&mut self, &node: &Self::NodeId, properties: &NodeProperties<D>) {
        assert!(properties.display == Display::Inline ||
                properties.display == Display::InlineBlock);

        let node = self.nodes.get_mut(&node).expect("Invalid node ID in inline flow");

        match node.inner {
            DomNodeInner::Element(ref mut props) => *props = properties.clone(),
            _ => panic!()
        };

        // Flush cached line boxes.
        self.line_boxes = None;
    }

    fn delete_node(&mut self, &node: &Self::NodeId) {
        assert!(node != 0);

        let children = self.nodes.get(&node).expect("Invalid node ID in inline flow")
                                 .children.clone();
        for child in children {
            self.delete_node(&child);
        }

        {
            if let Some(parent) = self.nodes.get(&node).expect("Invalid node ID in inline flow")
                                      .parent.clone() {
                self.nodes.get_mut(&parent).expect("Non-existing parent")
                          .children.retain(|&c| c != node);
            }

            self.nodes.get_mut(&node).unwrap().parent = None;
        };

        self.nodes.remove(&node);

        // Flush cached line boxes.
        self.line_boxes = None;
    }
}

impl<'a, C, D> FlowDraw for &'a mut InlineFlow<C, D> where C: 'a + Clone, D: 'a + Draw {
    type CustomData = C;
    type Draw = D;
    type DrawIter = DrawIter<'a, C, D>;

    fn draw(self) -> Self::DrawIter {
        if self.line_boxes.is_none() {
            self.refresh();
        }

        let total_characters = self.line_boxes.as_ref().unwrap().iter()
                                   .map(|l| l.content.len()).fold(0, |a,b|a+b);

        DrawIter {
            flow: self,
            next_line: self.line_boxes.as_ref().unwrap().iter(),
            next_line_character: None,
            current_line_height: 0.0,   // dummy value
            y_offset: 0.0,
        }
    }
}

pub struct DrawIter<'a, C, D> where C: 'a + Clone, D: 'a + Draw {
    flow: &'a InlineFlow<C, D>,
    next_line: SliceIter<'a, LineBox<C, D>>,
    next_line_character: Option<SliceIter<'a, Character<C, D>>>,
    current_line_height: f32,
    y_offset: f32,
}

impl<'a, C, D> Iterator for DrawIter<'a, C, D> where C: Clone, D: Draw {
    type Item = (LayoutElement<D::ImageResource, D::FontVariant>, C);

    fn next(&mut self) -> Option<Self::Item> {
        loop {
            if self.next_line_character.is_none() {
                match self.next_line.next() {
                    Some(l) => {
                        self.next_line_character = Some(l.content.iter());
                        self.current_line_height = l.height;
                    },
                    None => return None,
                };
            }

            let character = match self.next_line_character.as_mut().unwrap().next() {
                Some(c) => c,
                None => {
                    self.y_offset += self.current_line_height;
                    self.next_line_character = None;
                    continue;
                }
            };

            return Some((LayoutElement::Text {
                text: character.text,
                font: character.font.clone(),
                left_coordinate: character.offset[0],
                top_coordinate: character.offset[1] + self.y_offset,
                width: character.size[0],
                height: character.size[1],
            }, character.custom_data.clone()));
        }
    }

    fn size_hint(&self) -> (usize, Option<usize>) {
        let remaining_lines = self.next_line.clone().map(|l| l.content.len()).fold(0, |a,b| a+b);
        let curr_line = self.next_line_character.as_ref().map(|i| i.len()).unwrap_or(0);

        let total = remaining_lines + curr_line;
        (total, Some(total))
    }
}

impl<'a, C, D> ExactSizeIterator for DrawIter<'a, C, D> where C: Clone, D: Draw {}

/// Computed values of the root of the inline flow.
///
/// Some values are used for the calculations, others (like `font_family`) are the properties to
/// apply to text nodes that are direct children of the root.
#[derive(Debug, Clone)]
pub struct RootComputedValues<F> {
    /// Width in pixels of the containing box.
    pub content_box_width: f32,
    /// Height in pixels of the containing box.
    pub content_box_height: f32,
    /// The font family of the root.
    pub font_family: F,
    /// Font size in pixels.
    pub font_size: f32,
    /// White-space property.
    pub white_space: SpecifiedWhiteSpace,
}

// Information about an individual node in this hierarchy.
struct DomNode<C, D> where C: Clone, D: Draw {
    // Identifier of the parent, or None if no parent.
    parent: Option<u32>,
    // List of children.
    children: Vec<u32>,
    // Additional info.
    inner: DomNodeInner<D>,
    // The custom data passed by the exterior of this struct.
    custom_data: C,
}

impl<C, D> fmt::Debug for DomNode<C, D> where C: Clone + fmt::Debug, D: Draw {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        f.debug_struct("DomNode")
            .field("parent", &self.parent)
            .field("children", &self.children)
            .field("inner", &self.inner)
            .finish()
    }
}

// Additional info about a node.
enum DomNodeInner<D> where D: Draw {
    // This is the root node of the hierarchy.
    Root,
    // This is an element node.
    Element(NodeProperties<D>),
    // This is a text node.
    Text(String),
}

impl<D> fmt::Debug for DomNodeInner<D> where D: Draw {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match *self {
            DomNodeInner::Root => {
                f.debug_struct("DomNodeInner::Root")
                    .finish()
            },
            DomNodeInner::Element(ref props) => {
                f.debug_struct("DomNodeInner::Element")
                    .field("props", props)
                    .finish()
            },
            DomNodeInner::Text(ref txt) => {
                f.debug_struct("DomNodeInner::Text")
                    .field("txt", txt)
                    .finish()
            },
        }
    }
}

// An individual line of text.
struct LineBox<C, D> where D: Draw {
    // List of glyphs or sub-flows of this line.
    content: Vec<Character<C, D>>,
    // Height of the line box in pixels.
    height: f32,
}

impl<C, D> fmt::Debug for LineBox<C, D> where C: fmt::Debug, D: Draw {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        f.debug_struct("LineBox")
            .field("content", &self.content)
            .finish()
    }
}

// An individual element on a line of text.
struct Character<C, D> where D: Draw {
    // Identifier of the node this character belongs to. Multiple instances can point to the same
    // node.
    dom_node_id: u32,
    // Cached custom data of the node. We store this here to avoid a hashmap lookup every time
    // we draw.
    custom_data: C,
    // The content. Usually a single character, but we need a string in order to correctly handle
    // unicode.     TODO: is that true?
    text: char,
    // The font to use.
    font: D::FontVariant,
    // Offset in pixels from the top-left of the line box to this element. Used when drawing.
    offset: [f32; 2],
    // Size in pixels of the character. Used when drawing.
    size: [f32; 2],
}

impl<C, D> fmt::Debug for Character<C, D> where C: fmt::Debug, D: Draw {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        f.debug_struct("Character")
            .field("dom_node_id", &self.dom_node_id)
            .field("custom_data", &self.custom_data)
            .field("text", &self.text)
            .field("font", &self.font)
            .field("offset", &self.offset)
            .field("size", &self.size)
            .finish()
    }
}

#[cfg(test)]
mod tests {
    use flow::Flow;
    use flow::NodeProperties;
    use inline::InlineFlow;

    /*#[test]
    fn basic() {
        let mut flow = InlineFlow::new();

        let elem = flow.append_element_node(&flow.root(), &NodeProperties {
            display: Display::Inline,
        }, ());

        let root = flow.root();
        flow.append_child(elem, root);
    }

    #[test]
    #[should_panic]
    fn panic_not_inline() {
        let mut flow = InlineFlow::new();
        let _ = flow.append_element_node(&flow.root(), &NodeProperties {
            display: Display::Block,
        }, ());
    }*/
}
