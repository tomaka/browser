use std::ascii::AsciiExt;
use std::fmt::Debug;
use std::vec::IntoIter as VecIntoIter;
use cssparser::Color;
use cssparser::Parser;
use cssparser::Token;

use css_property::CssProperty;
use css_unit::CssUnit;
use draw::Draw;
use specified::SpecifiedBackgroundAttachment;
use specified::SpecifiedBackgroundRepeat;
use specified::SpecifiedBorderStyle;
use specified::SpecifiedClear;
use specified::SpecifiedColor;
use specified::SpecifiedCursor;
use specified::SpecifiedDisplay;
use specified::SpecifiedFloat;
use specified::SpecifiedLength;
use specified::SpecifiedLengthPercentage;
use specified::SpecifiedLengthPercentageAuto;
use specified::SpecifiedPosition;
use specified::SpecifiedTextAlign;
use specified::SpecifiedVisibility;
use specified::SpecifiedWhiteSpace;
use specified::ValueOrInherit;

#[derive(Debug, Clone, PartialEq)]
pub enum CssRule<I, F> {
    BackgroundAttachment(ValueOrInherit<SpecifiedBackgroundAttachment>),
    BackgroundColor(ValueOrInherit<SpecifiedColor>),        // TODO: `transparent` keyword?
    BackgroundImage(ValueOrInherit<Option<I>>),
    BackgroundPosition(ValueOrInherit<(SpecifiedLengthPercentage,
                                       SpecifiedLengthPercentage)>),
    BackgroundRepeat(ValueOrInherit<SpecifiedBackgroundRepeat>),
    BorderBottomColor(ValueOrInherit<SpecifiedColor>),
    BorderBottomStyle(ValueOrInherit<SpecifiedBorderStyle>),
    BorderBottomWidth(ValueOrInherit<SpecifiedLength>),     // TODO: handle "small", "medium", "thick" keywords
    BorderLeftColor(ValueOrInherit<SpecifiedColor>),
    BorderLeftStyle(ValueOrInherit<SpecifiedBorderStyle>),
    BorderLeftWidth(ValueOrInherit<SpecifiedLength>),       // TODO: handle "small", "medium", "thick" keywords
    BorderRightColor(ValueOrInherit<SpecifiedColor>),
    BorderRightStyle(ValueOrInherit<SpecifiedBorderStyle>),
    BorderRightWidth(ValueOrInherit<SpecifiedLength>),      // TODO: handle "small", "medium", "thick" keywords
    BorderTopColor(ValueOrInherit<SpecifiedColor>),
    BorderTopStyle(ValueOrInherit<SpecifiedBorderStyle>),
    BorderTopWidth(ValueOrInherit<SpecifiedLength>),        // TODO: handle "small", "medium", "thick" keywords
    Bottom(ValueOrInherit<SpecifiedLengthPercentageAuto>),
    Clear(ValueOrInherit<SpecifiedClear>),
    Color(ValueOrInherit<SpecifiedColor>),
    Cursor(ValueOrInherit<SpecifiedCursor<I>>),
    Display(ValueOrInherit<SpecifiedDisplay>),
    Float(ValueOrInherit<SpecifiedFloat>),
    FontFamily(ValueOrInherit<F>),
    FontSize(ValueOrInherit<SpecifiedLengthPercentage>),
    Height(ValueOrInherit<SpecifiedLengthPercentageAuto>),
    Left(ValueOrInherit<SpecifiedLengthPercentageAuto>),
    MarginBottom(ValueOrInherit<SpecifiedLengthPercentageAuto>),
    MarginLeft(ValueOrInherit<SpecifiedLengthPercentageAuto>),
    MarginRight(ValueOrInherit<SpecifiedLengthPercentageAuto>),
    MarginTop(ValueOrInherit<SpecifiedLengthPercentageAuto>),
    PaddingBottom(ValueOrInherit<SpecifiedLengthPercentage>),
    PaddingLeft(ValueOrInherit<SpecifiedLengthPercentage>),
    PaddingRight(ValueOrInherit<SpecifiedLengthPercentage>),
    PaddingTop(ValueOrInherit<SpecifiedLengthPercentage>),
    Position(ValueOrInherit<SpecifiedPosition>),
    Right(ValueOrInherit<SpecifiedLengthPercentageAuto>),
    TextAlign(ValueOrInherit<SpecifiedTextAlign>),
    Top(ValueOrInherit<SpecifiedLengthPercentageAuto>),
    Visibility(ValueOrInherit<SpecifiedVisibility>),
    WhiteSpace(ValueOrInherit<SpecifiedWhiteSpace>),
    Width(ValueOrInherit<SpecifiedLengthPercentageAuto>),

    /*pub font_family: F,
    pub font_family_was_inherited: bool,*/
}

impl<I, F> CssRule<I, F> {
    // TODO: faster iterator than Vec
    pub fn parse<D>(input: &mut Parser, draw: &D) -> Result<VecIntoIter<CssRule<I, F>>, ()>
        where D: Draw<ImageResource = I, FontResource = F>,
              I: Debug + Clone,
              F: Debug + Clone
    {
        let name = try!(input.expect_ident());
        try!(input.expect_colon());

        match name {
            ref name if name.eq_ignore_ascii_case("margin") => {
                // FIXME: handle "inherit"
                let first = try!(SpecifiedLengthPercentageAuto::parse(input));
                let second = SpecifiedLengthPercentageAuto::parse(input);
                let third = SpecifiedLengthPercentageAuto::parse(input);
                let fourth = SpecifiedLengthPercentageAuto::parse(input);

                let second = second.clone().unwrap_or(first.clone());
                let third = third.clone().unwrap_or(first.clone());
                let fourth = fourth.clone().unwrap_or(second.clone());

                Ok(vec![
                    CssRule::MarginTop(ValueOrInherit::Value(first)),
                    CssRule::MarginRight(ValueOrInherit::Value(second)),
                    CssRule::MarginBottom(ValueOrInherit::Value(third)),
                    CssRule::MarginLeft(ValueOrInherit::Value(fourth))
                ].into_iter())
            },
            ref name if name.eq_ignore_ascii_case("padding") => {
                // FIXME: handle "inherit"
                let first = try!(SpecifiedLengthPercentage::parse(input));
                let second = SpecifiedLengthPercentage::parse(input);
                let third = SpecifiedLengthPercentage::parse(input);
                let fourth = SpecifiedLengthPercentage::parse(input);

                let second = second.clone().unwrap_or(first.clone());
                let third = third.clone().unwrap_or(first.clone());
                let fourth = fourth.clone().unwrap_or(second.clone());

                Ok(vec![
                    CssRule::PaddingTop(ValueOrInherit::Value(first)),
                    CssRule::PaddingRight(ValueOrInherit::Value(second)),
                    CssRule::PaddingBottom(ValueOrInherit::Value(third)),
                    CssRule::PaddingLeft(ValueOrInherit::Value(fourth))
                ].into_iter())
            },
            name => {
                let name = try!(name.parse());
                let rule = try!(CssRule::parse_value_from_property(input, name, draw));
                Ok(vec![rule].into_iter())
            },
        }
    }

    /// Checks whether the value is correct for the given property and returns a `CssRule`.
    pub fn parse_value_from_property<D>(input: &mut Parser, property: CssProperty, draw: &D)
                                        -> Result<CssRule<I, F>, ()>
        where D: Draw<ImageResource = I, FontResource = F>,
              I: Debug + Clone,
              F: Debug + Clone
    {
        // FIXME: handle "inherit"
        Ok(match property {
            CssProperty::BackgroundAttachment => CssRule::BackgroundAttachment(ValueOrInherit::Value(try!(SpecifiedBackgroundAttachment::parse(input)))),
            CssProperty::BackgroundColor => CssRule::BackgroundColor(ValueOrInherit::Value(try!(SpecifiedColor::parse(input)))),
            CssProperty::BackgroundImage => {
                match try!(input.next()) {
                    Token::Ident(ref val) if val.eq_ignore_ascii_case("inherit") => {
                        CssRule::BackgroundImage(ValueOrInherit::Inherit)
                    },
                    Token::Ident(ref val) if val.eq_ignore_ascii_case("none") => {
                        CssRule::BackgroundImage(ValueOrInherit::Value(None))
                    },
                    Token::UnquotedUrl(val) => {
                        match draw.parse_image(&val) {
                            Ok(img) => CssRule::BackgroundImage(ValueOrInherit::Value(Some(img))),
                            Err(_) => return Err(())
                        }
                    },
                    _ => return Err(())
                }
            },
            CssProperty::BackgroundPosition => unimplemented!(),
            CssProperty::BackgroundRepeat => CssRule::BackgroundRepeat(ValueOrInherit::Value(try!(SpecifiedBackgroundRepeat::parse(input)))),
            CssProperty::BorderBottomColor => CssRule::BorderBottomColor(ValueOrInherit::Value(try!(SpecifiedColor::parse(input)))),
            CssProperty::BorderBottomStyle => CssRule::BorderBottomStyle(ValueOrInherit::Value(try!(SpecifiedBorderStyle::parse(input)))),
            CssProperty::BorderBottomWidth => CssRule::BorderBottomWidth(ValueOrInherit::Value(try!(SpecifiedLength::parse(input)))),
            CssProperty::BorderLeftColor => CssRule::BorderLeftColor(ValueOrInherit::Value(try!(SpecifiedColor::parse(input)))),
            CssProperty::BorderLeftStyle => CssRule::BorderLeftStyle(ValueOrInherit::Value(try!(SpecifiedBorderStyle::parse(input)))),
            CssProperty::BorderLeftWidth => CssRule::BorderLeftWidth(ValueOrInherit::Value(try!(SpecifiedLength::parse(input)))),
            CssProperty::BorderRightColor => CssRule::BorderRightColor(ValueOrInherit::Value(try!(SpecifiedColor::parse(input)))),
            CssProperty::BorderRightStyle => CssRule::BorderRightStyle(ValueOrInherit::Value(try!(SpecifiedBorderStyle::parse(input)))),
            CssProperty::BorderRightWidth => CssRule::BorderRightWidth(ValueOrInherit::Value(try!(SpecifiedLength::parse(input)))),
            CssProperty::BorderTopColor => CssRule::BorderTopColor(ValueOrInherit::Value(try!(SpecifiedColor::parse(input)))),
            CssProperty::BorderTopStyle => CssRule::BorderTopStyle(ValueOrInherit::Value(try!(SpecifiedBorderStyle::parse(input)))),
            CssProperty::BorderTopWidth => CssRule::BorderTopWidth(ValueOrInherit::Value(try!(SpecifiedLength::parse(input)))),
            CssProperty::Bottom => CssRule::Bottom(ValueOrInherit::Value(try!(SpecifiedLengthPercentageAuto::parse(input)))),
            CssProperty::Clear => CssRule::Clear(ValueOrInherit::Value(try!(SpecifiedClear::parse(input)))),
            CssProperty::Color => CssRule::Color(ValueOrInherit::Value(try!(SpecifiedColor::parse(input)))),
            CssProperty::Cursor => CssRule::Cursor(ValueOrInherit::Value(try!(SpecifiedCursor::parse(input, draw)))),       // TODO: parse coma-spearated list
            CssProperty::Display => CssRule::Display(ValueOrInherit::Value(try!(SpecifiedDisplay::parse(input)))),
            CssProperty::Float => CssRule::Float(ValueOrInherit::Value(try!(SpecifiedFloat::parse(input)))),
            CssProperty::FontFamily => {
                match try!(input.next()) {
                    Token::Ident(val) | Token::UnquotedUrl(val) => {
                        match draw.parse_font(&val) {
                            Ok(font) => CssRule::FontFamily(ValueOrInherit::Value(font)),
                            Err(_) => return Err(())
                        }
                    },
                    _ => return Err(())
                }
            },
            CssProperty::FontSize => CssRule::FontSize(ValueOrInherit::Value(try!(SpecifiedLengthPercentage::parse(input)))),
            CssProperty::Height => CssRule::Height(ValueOrInherit::Value(try!(SpecifiedLengthPercentageAuto::parse(input)))),
            CssProperty::Left => CssRule::Left(ValueOrInherit::Value(try!(SpecifiedLengthPercentageAuto::parse(input)))),
            CssProperty::MarginBottom => CssRule::MarginBottom(ValueOrInherit::Value(try!(SpecifiedLengthPercentageAuto::parse(input)))),
            CssProperty::MarginLeft => CssRule::MarginLeft(ValueOrInherit::Value(try!(SpecifiedLengthPercentageAuto::parse(input)))),
            CssProperty::MarginRight => CssRule::MarginRight(ValueOrInherit::Value(try!(SpecifiedLengthPercentageAuto::parse(input)))),
            CssProperty::MarginTop => CssRule::MarginTop(ValueOrInherit::Value(try!(SpecifiedLengthPercentageAuto::parse(input)))),
            CssProperty::PaddingBottom => CssRule::PaddingBottom(ValueOrInherit::Value(try!(SpecifiedLengthPercentage::parse(input)))),
            CssProperty::PaddingLeft => CssRule::PaddingLeft(ValueOrInherit::Value(try!(SpecifiedLengthPercentage::parse(input)))),
            CssProperty::PaddingRight => CssRule::PaddingRight(ValueOrInherit::Value(try!(SpecifiedLengthPercentage::parse(input)))),
            CssProperty::PaddingTop => CssRule::PaddingTop(ValueOrInherit::Value(try!(SpecifiedLengthPercentage::parse(input)))),
            CssProperty::Position => CssRule::Position(ValueOrInherit::Value(try!(SpecifiedPosition::parse(input)))),
            CssProperty::Right => CssRule::Right(ValueOrInherit::Value(try!(SpecifiedLengthPercentageAuto::parse(input)))),
            CssProperty::TextAlign => CssRule::TextAlign(ValueOrInherit::Value(try!(SpecifiedTextAlign::parse(input)))),
            CssProperty::Top => CssRule::Top(ValueOrInherit::Value(try!(SpecifiedLengthPercentageAuto::parse(input)))),
            CssProperty::Visibility => CssRule::Visibility(ValueOrInherit::Value(try!(SpecifiedVisibility::parse(input)))),
            CssProperty::WhiteSpace => CssRule::WhiteSpace(ValueOrInherit::Value(try!(SpecifiedWhiteSpace::parse(input)))),
            CssProperty::Width => CssRule::Width(ValueOrInherit::Value(try!(SpecifiedLengthPercentageAuto::parse(input)))),
            _ => return Err(())        // TODO:
        })
    }
}
