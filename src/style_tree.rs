use std::cell::RefCell;
use std::fmt;
use std::marker::PhantomData;
use std::rc::Rc;
use std::rc::Weak;

use computed::BorderStyle;
use computed::Color;
use computed::Display;
use computed::LengthPercentage;
use computed::LengthPercentageAuto;
use computed::Position;
use computed::TextAlign;
use computed::Visibility;
use cssparser::Parser;
use css_rule::CssRule;
use css_stylesheet::CssStylesheet;
use css_stylesheet::NodeInterface;
use css_unit::CssUnit;
use draw::Cursor;
use draw::Draw;
use draw::DrawProperties;
use flow::Flow;
use flow::FlowDraw;
use flow::LayoutElement;
use flow::NodeProperties;
use flow::NodePropertiesWithPosition;
use root_flow::RootFlow;
use specified::SpecifiedColor;
use specified::SpecifiedCursor;
use specified::SpecifiedLengthPercentageAuto;
use specified::SpecifiedLengthPercentage;
use specified::SpecifiedValues;
use specified::SpecifiedWhiteSpace;
use specified::ValueOrInherit;

/// Allows creation of a tree of elements and text nodes, where you indicate the list of
/// stylesheets and content of "style" attributes and the tree handles the rest.
pub struct StyleTree<D> where D: Draw {
    // List of all nodes. `None` designates a hole to fill. First element is always `Some` and
    // corresponds to the root node.
    nodes: Vec<Option<Node<D>>>,

    // The stylesheets that apply to this document.
    stylesheets: Vec<Rc<CssStylesheet<D::ImageResource, D::FontResource>>>,

    // Object that handles the actual CSS boxes layout.
    root_flow: RootFlow<usize, D>,

    // The implementation of `Draw` used to determine various things.
    draw: Rc<D>,

    // Dimensions of the viewport provided by the user.
    viewport_size: [u32; 2],

    // Position of the cursor provided by the user.
    cursor_position: Option<[f32; 2]>,
}

impl<D> StyleTree<D> where D: Draw {
    /// Builds a new empty tree.
    pub fn new(draw: Rc<D>, viewport_size: [u32; 2]) -> StyleTree<D> {
        let mut parser = Parser::new(DEFAULT_STYLESHEET);
        let default_stylesheet = CssStylesheet::parse(&mut parser, &*draw).unwrap();

        StyleTree::with_default_stylesheet(draw, default_stylesheet, viewport_size)
    }

    /// Builds a new empty tree.
    pub fn with_default_stylesheet(draw: Rc<D>, stylesheet: CssStylesheet<D::ImageResource,
                                                                          D::FontResource>,
                                   viewport_size: [u32; 2]) -> StyleTree<D>
    {
        // Specified values of the root node. They never change.
        let root_specified = SpecifiedValues::initial(draw.default_font());

        // Computed values of the root node.
        let root_computed = {
            let mut v = ComputedValues::from_specified_values(&root_specified, None,
                                                              viewport_size);
            // TODO: hardcode in the specified values instead?
            v.display = Display::Block;
            v.width = LengthPercentageAuto::Length { pixels: viewport_size[0] as f32 };
            v.height = LengthPercentageAuto::Length { pixels: viewport_size[1] as f32 };
            v
        };

        // The block flow that starts at the root. Handles everything related to the layout.
        let root_flow = RootFlow::new(draw.clone(), 0, NodePropertiesWithPosition::from(root_computed.clone()).properties);


        StyleTree {
            nodes: vec![
                Some(Node {
                    parent: None,
                    children: Vec::new(),
                    tag_name: None,
                    attributes: Vec::new(),
                    text: None,
                    specified_styles: root_specified,
                    additional_rules_if_hovered: Vec::new(),
                    computed_values: Some(root_computed),
                    flow_id: Some(root_flow.root()),
                })
            ],
            stylesheets: vec![Rc::new(stylesheet)],
            root_flow: root_flow,
            draw: draw,
            viewport_size: viewport_size,
            cursor_position: None,
        }
    }

    /// Sets the pixel at which the cursor is located.
    ///
    /// You can also pass `None` if you know that the cursor is not over the document. The
    /// default value is `None`.
    pub fn set_cursor_position(&mut self, pos: Option<[u32; 2]>) {
        self.cursor_position = pos.map(|p| [p[0] as f32, p[1] as f32]);

        // Finding which node is hovered.
        let hovered_node = if let Some(pos) = self.cursor_position {
            if let Some((_, node_id)) = self.root_flow.node_at_position(pos) {
                Some(node_id)
            } else {
                None
            }
        } else {
            None
        };

        // Refresh the whole hierarchy.
        for node_id in 1 .. self.nodes.len() {  // We start at 1 since the root is never modified.
            let (specified, parent, flow_id) = {
                let node = match self.nodes[node_id] {
                    Some(ref node) => node.clone(),
                    _ => continue
                };

                // Ignore text nodes.
                if node.computed_values.is_none() {
                    continue;
                }

                let flow_id = match node.flow_id {
                    None => continue,   // TODO: what if there's a `display: foo` when hovered?
                    Some(ref id) => id.clone(),
                };

                let mut specified = node.specified_styles.clone();

                if let Some(hovered_node) = hovered_node {
                    for &(if_hovered, ref additional) in node.additional_rules_if_hovered.iter() {
                        let mut hovered_node = hovered_node;
                        loop {
                            if hovered_node == if_hovered {
                                specified.merge(additional);
                                break;
                            }

                            hovered_node = match self.nodes[hovered_node].as_ref().unwrap().parent {
                                Some(p) => p,
                                None => break
                            };
                        }
                    }
                }

                (specified, node.parent, flow_id)
            };

            let computed_values = {
                let cv = if let Some(parent) = parent {
                    Some(self.nodes[parent].as_ref().unwrap().computed_values.as_ref().unwrap())
                } else {
                    None
                };

                ComputedValues::from_specified_values(&specified, cv, self.viewport_size)
            };

            self.nodes[node_id].as_mut().unwrap().computed_values = Some(computed_values.clone());

            self.root_flow.set_element_properties(&flow_id, &computed_values.into());
        }
    }

    /// Changes the size of the viewport.
    #[inline]
    pub fn set_viewport_size(&mut self, viewport_size: [u32; 2]) {
        // Ignore call if nothing changes.
        if self.viewport_size == viewport_size {
            return;
        }

        // FIXME: should flush computed values that use `vw`/`vh`.
        self.viewport_size = viewport_size;

        let mut root = self.nodes[0].as_mut().unwrap();
        root.computed_values.as_mut().unwrap().width = LengthPercentageAuto::Length {
                                                       pixels: viewport_size[0] as f32 };
        root.computed_values.as_mut().unwrap().height = LengthPercentageAuto::Length {
                                                        pixels: viewport_size[1] as f32 };

        self.root_flow.set_element_properties(root.flow_id.as_ref().unwrap(),
                                              &From::from(root.computed_values.as_ref().unwrap().clone()));
    }

    /// Adds a stylesheet to apply to all the nodes.
    pub fn add_stylesheet(&mut self, stylesheet: CssStylesheet<D::ImageResource, D::FontResource>)
                          -> StylesheetId<D>
    {
        // FIXME: rebuild the list of specified and computed values

        let storage = Rc::new(stylesheet);
        let id = Rc::downgrade(&storage);
        self.stylesheets.push(storage);
        StylesheetId(id)
    }

    /// Removes a previously-added stylesheet.
    pub fn remove_stylesheet(&mut self, id: StylesheetId<D>) {
        // FIXME: rebuild the list of specified and computed values

        if let Some(id) = id.0.upgrade() {
            self.stylesheets.retain(|s| {
                &**s as *const CssStylesheet<_, _> != &*id as *const CssStylesheet<_, _>
            });
        }
    }

    /// Returns the ID of the root node.
    #[inline]
    pub fn root_node(&self) -> NodeId<D> {
        NodeId(0, PhantomData)
    }

    /// Creates an element and adds it as a child of an already-existing node.
    pub fn append_element_node<I, J>(&mut self, parent: &NodeId<D>, tag_name: &str, style_attr: I,
                                     attributes: J) -> NodeId<D>
        where I: IntoIterator<Item = CssRule<D::ImageResource, D::FontResource>>,
              J: IntoIterator<Item = (String, String)>
    {
        enum NodeInt<'a, D> where D: Draw + 'a {
            Draft(&'a StyleTree<D>, &'a str, &'a [(String, String)], usize),
            Existing(&'a StyleTree<D>, usize),
        }

        impl<'a, D> Clone for NodeInt<'a, D> where D: Draw {
            fn clone(&self) -> NodeInt<'a, D> {
                match self {
                    &NodeInt::Draft(tree, tag, attrs, parent) => {
                        NodeInt::Draft(tree, tag, attrs, parent)
                    },
                    &NodeInt::Existing(tree, node) => NodeInt::Existing(tree, node),
                }
            }
        }

        impl<'a, D> NodeInterface for NodeInt<'a, D> where D: Draw {
            fn tag_name(&self) -> String {
                match *self {
                    NodeInt::Draft(_, tag, _, _) => tag.to_owned(),
                    NodeInt::Existing(tree, node) => tree.nodes[node].as_ref().unwrap().tag_name.as_ref().unwrap().clone(),
                }
            }

            fn parent(&self) -> Option<Self> {
                match *self {
                    NodeInt::Draft(tree, _, _, parent) => Some(NodeInt::Existing(tree, parent)),
                    NodeInt::Existing(tree, node) => tree.nodes[node].as_ref().unwrap().parent.map(|p| NodeInt::Existing(tree, p))
                }
            }

            fn attribute(&self, attr: &str) -> Option<String> {
                match *self {
                    NodeInt::Draft(_, _, attrs, _) => {
                        attrs.iter().filter_map(|&(ref n, ref v)| if n == attr { Some(v.clone()) } else { None }).next()
                    },
                    NodeInt::Existing(_, _) => unimplemented!()
                }
            }
        }

        let new_node_id = self.nodes.iter().position(|n| n.is_none()).unwrap_or(self.nodes.len());

        let attributes = attributes.into_iter().collect::<Vec<_>>();

        let mut specified_values: SpecifiedValues<D::ImageResource, D::FontResource> = Default::default();
        let mut additional_rules_if_hovered = Vec::new();

        for stylesheet in self.stylesheets.iter() {
            let result = stylesheet.query(NodeInt::Draft(self, tag_name, &attributes, parent.0));
            for rule in result.rules {
                specified_values.merge(&rule);
            }
            for (node, rule) in result.if_hovered {
                let node = match node {
                    NodeInt::Draft(_, _, _, _) => new_node_id,
                    NodeInt::Existing(_, id) => id,
                };

                additional_rules_if_hovered.push((node, rule));
            }
        }

        for rule in style_attr {
            specified_values.merge(&rule);
        }

        let computed_values = {
            let cv = self.nodes[parent.0].as_ref().unwrap().computed_values.as_ref().unwrap();
            ComputedValues::from_specified_values(&specified_values, Some(cv), self.viewport_size)
        };

        let flow_id = {
            let parent = self.nodes[parent.0].as_ref().expect("Wrong node ID");
            if computed_values.display != Display::None {
                if let Some(parent_flow_id) = parent.flow_id.as_ref() {
                    let props = NodePropertiesWithPosition::from(computed_values.clone());
                    Some(self.root_flow.append_element_node(parent_flow_id, &props, new_node_id))
                } else {
                    None
                }
            } else {
                None
            }
        };

        let node = Node {
            parent: Some(parent.0),
            children: Vec::new(),
            tag_name: Some(tag_name.to_owned()),
            attributes: attributes,
            text: None,
            specified_styles: specified_values,
            additional_rules_if_hovered: additional_rules_if_hovered,
            computed_values: Some(computed_values),
            flow_id: flow_id,
        };

        if self.nodes.len() == new_node_id {
            self.nodes.push(Some(node));
        } else {
            self.nodes[new_node_id] = Some(node);
        }

        self.nodes[parent.0].as_mut().unwrap().children.push(new_node_id);

        NodeId(new_node_id, PhantomData)
    }

    /// Creates a text node and adds it as a child of an already-existing node.
    pub fn append_text_node(&mut self, parent: &NodeId<D>, text: &str) -> NodeId<D> {
        let new_node_id = self.nodes.iter().position(|n| n.is_none()).unwrap_or(self.nodes.len());

        let flow_id = {
            let parent = self.nodes[parent.0].as_ref().expect("Wrong node ID");
            if let Some(parent_flow_id) = parent.flow_id.as_ref() {
                Some(self.root_flow.append_text_node(parent_flow_id, text.to_owned(), new_node_id))
            } else {
                None
            }
        };

        let node = Node {
            parent: Some(parent.0),
            children: Vec::new(),
            tag_name: None,
            attributes: Vec::new(),
            text: Some(text.to_owned()),
            specified_styles: Default::default(),
            additional_rules_if_hovered: Vec::new(),
            computed_values: None,
            flow_id: flow_id,
        };

        if self.nodes.len() == new_node_id {
            self.nodes.push(Some(node));
        } else {
            self.nodes[new_node_id] = Some(node);
        }

        self.nodes[parent.0].as_mut().unwrap().children.push(new_node_id);

        NodeId(new_node_id, PhantomData)
    }

    /// Sets the content of the "styles" attribute of a node.
    pub fn set_node_style_attr<I>(&mut self, node: &NodeId<D>, rules: I)
        where I: IntoIterator<Item = CssRule<D::ImageResource, D::FontResource>>
    {
        unimplemented!()
    }

    /// Deletes a node and all its children.
    pub fn delete_node(&mut self, node: &NodeId<D>) {
        unimplemented!()
        /*let node = node.0.upgrade().unwrap();

        if let Some(flow_id) = node.borrow().flow_id.as_ref() {
            self.root_flow.delete_node(flow_id);
        }

        let parent = node.borrow().parent.as_ref().unwrap().upgrade().unwrap();
        parent.borrow_mut().children
              .retain(|c| &**c as *const RefCell<_> != &*node as *const RefCell<_>);*/
    }

    /// Returns an iterator to the things that must be drawn.
    #[inline]
    pub fn draw(&mut self) -> DrawIter<D> {
        // Find the "cursor" property of the hovered element.
        let cursor = if let Some(pos) = self.cursor_position {
            if let Some((_, node_id)) = self.root_flow.node_at_position(pos) {
                let computed_values = match self.nodes[node_id].as_ref().unwrap().computed_values {
                    Some(ref cmp) => cmp,
                    None => self.nodes[self.nodes[node_id].as_ref().unwrap().parent.unwrap()].as_ref().unwrap().computed_values.as_ref().unwrap()
                };

                match computed_values.cursor {
                    SpecifiedCursor::Predefined(c) => {
                        Some(DrawProperties::Cursor {
                            cursor: c,
                            position: pos,
                        })
                    },
                    SpecifiedCursor::Custom(_) => unimplemented!()
                }
                
            } else {
                Some(DrawProperties::Cursor {
                    cursor: Cursor::Auto,
                    position: pos,
                })
            }
        } else {
            None
        };

        DrawIter {
            nodes: &self.nodes,
            draw: self.draw.clone(),
            inner: self.root_flow.draw(),
            cursor: cursor,
        }
    }
}

/// Iterator that produces the list of elements to draw.
pub struct DrawIter<'a, D> where D: Draw + 'a {
    nodes: &'a [Option<Node<D>>],

    draw: Rc<D>,
    inner: <&'a mut RootFlow<usize, D> as FlowDraw>::DrawIter,

    // The draw properties for the mouse cursor. Always added as the last element.
    cursor: Option<DrawProperties<D::ImageResource, D::FontVariant>>,
}

impl<'a, D> Iterator for DrawIter<'a, D> where D: Draw {
    type Item = DrawProperties<D::ImageResource, D::FontVariant>;

    fn next(&mut self) -> Option<Self::Item> {
        loop {
            let (draw_props, node_id) = match self.inner.next() {
                None => return self.cursor.take(),
                Some(v) => v
            };

            let computed_values = match self.nodes[node_id].as_ref().unwrap().computed_values {
                Some(ref cmp) => cmp,
                None => self.nodes[self.nodes[node_id].as_ref().unwrap().parent.unwrap()].as_ref().unwrap().computed_values.as_ref().unwrap()
            };

            if computed_values.visibility != Visibility::Visible {
                continue;
            }

            match draw_props {
                LayoutElement::Text { text, font, left_coordinate, top_coordinate, width,
                                      height } =>
                {
                    if computed_values.color.alpha <= 0.0 {
                        continue;
                    }

                    return Some(DrawProperties::Text {
                        text: text,
                        font: font,
                        color: [computed_values.color.red, computed_values.color.green,
                                computed_values.color.blue, computed_values.color.alpha],
                        left_coordinate: left_coordinate,
                        top_coordinate: top_coordinate,
                        width: width,
                        height: height
                    });
                },

                LayoutElement::Block { left_coordinate, top_coordinate, width, height } => {
                    if let Some(ref img) = computed_values.background_image {
                        let img_dims = self.draw.intrinsic_dimensions(img);
                        // FIXME: return both Color and Image
                        return Some(DrawProperties::Image {
                            image: img.clone(),
                            image_width: img_dims[0],
                            image_height: img_dims[1],
                            left_coordinate: left_coordinate,
                            top_coordinate: top_coordinate,
                            width: width,
                            height: height
                        });
                    }

                    if computed_values.background_color.alpha > 0.0 {
                        return Some(DrawProperties::Color {
                            color: [
                                computed_values.background_color.red,
                                computed_values.background_color.green,
                                computed_values.background_color.blue,
                                computed_values.background_color.alpha
                            ],
                            left_coordinate: left_coordinate,
                            top_coordinate: top_coordinate,
                            width: width,
                            height: height
                        });
                    }
                },

                LayoutElement::ReplacedContent { image, left_coordinate, top_coordinate, width,
                                                 height } =>
                {
                    return Some(DrawProperties::Image {
                        image: image,
                        image_width: width,
                        image_height: height,
                        left_coordinate: left_coordinate,
                        top_coordinate: top_coordinate,
                        width: width,
                        height: height
                    });
                }
            }
        }
    }
}

/// Identifier for a stylesheet.
#[derive(Debug, Clone)]
pub struct StylesheetId<D>(Weak<CssStylesheet<D::ImageResource, D::FontResource>>) where D: Draw;

/// Identifier for a node of the tree.
#[derive(Clone)]
pub struct NodeId<D>(usize, PhantomData<D>) where D: Draw;

impl<D> fmt::Debug for NodeId<D> where D: Draw {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "NodeId")
    }
}

struct Node<D> where D: Draw {
    parent: Option<usize>,
    children: Vec<usize>,
    tag_name: Option<String>,

    // Attriutes of the element.
    attributes: Vec<(String, String)>,
    text: Option<String>,
    specified_styles: SpecifiedValues<D::ImageResource, D::FontResource>,
    additional_rules_if_hovered: Vec<(usize, CssRule<D::ImageResource, D::FontResource>)>,

    // The computed values of the node. `None` for text nodes.
    computed_values: Option<ComputedValues<D::ImageResource, D::FontResource>>,

    // Id of this node in the layout tree. `None` if "display" is "none".
    flow_id: Option<<RootFlow<usize, D> as Flow>::NodeId>,
}

/// Contains the computed values of a node.
#[derive(Debug, Clone)]
pub struct ComputedValues<I, F> {
    pub background_color: Color,
    pub background_image: Option<I>,
    pub border_bottom_color: Color,
    pub border_bottom_style: BorderStyle,
    pub border_bottom_width: f32,
    pub border_left_color: Color,
    pub border_left_style: BorderStyle,
    pub border_left_width: f32,
    pub border_right_color: Color,
    pub border_right_style: BorderStyle,
    pub border_right_width: f32,
    pub border_top_color: Color,
    pub border_top_style: BorderStyle,
    pub border_top_width: f32,
    pub bottom: LengthPercentageAuto,
    pub color: SpecifiedColor,
    pub cursor: SpecifiedCursor<I>,
    pub display: Display,
    pub font_size: f32,
    pub font_family: F,
    pub height: LengthPercentageAuto,
    pub left: LengthPercentageAuto,
    pub margin_bottom: LengthPercentageAuto,
    pub margin_left: LengthPercentageAuto,
    pub margin_right: LengthPercentageAuto,
    pub margin_top: LengthPercentageAuto,
    pub padding_bottom: LengthPercentage,
    pub padding_left: LengthPercentage,
    pub padding_right: LengthPercentage,
    pub padding_top: LengthPercentage,
    pub position: Position,
    pub right: LengthPercentageAuto,
    pub text_align: TextAlign,
    pub top: LengthPercentageAuto,
    pub visibility: Visibility,
    pub white_space: SpecifiedWhiteSpace,
    pub width: LengthPercentageAuto,
}

impl<I, F> ComputedValues<I, F> where I: Clone, F: Clone {
    /// Builds the computed values of a node from its specified values.
    ///
    /// Values that need to be inherited will use the content of `parent`.
    /// This function panics if `parent` is `None` and some values are `inherit`.
    pub fn from_specified_values(values: &SpecifiedValues<I, F>, parent: Option<&ComputedValues<I, F>>,
                                 viewport_size: [u32; 2]) -> ComputedValues<I, F>
    {
        fn len_to_px(len: f32, unit: CssUnit, viewport_size: [u32; 2], ref_em: f32) -> f32 {
            match unit.clone() {
                CssUnit::Px => len,
                CssUnit::In => { len * 96.0 },
                CssUnit::Cm => { len * 243.84 },
                CssUnit::Mm => { len * 0.24384 },
                CssUnit::Pt => { len * 1.333333 },
                CssUnit::Pc => { len * 16.0 },
                CssUnit::Vw => { len * viewport_size[0] as f32 / 100.0 },
                CssUnit::Vh => { len * viewport_size[1] as f32 / 100.0 },
                CssUnit::Em => len * ref_em,
                CssUnit::Ex => unimplemented!()
            }
        }

        fn convert_lp(input: SpecifiedLengthPercentage, viewport_size: [u32; 2], ref_em: f32) -> LengthPercentage {
            match input {
                SpecifiedLengthPercentage::Percentage { normalized_percents } => LengthPercentage::Percentage { percents: normalized_percents },
                SpecifiedLengthPercentage::Length { value, unit } => LengthPercentage::Length { pixels: len_to_px(value, unit, viewport_size, ref_em) },
            }
        }

        fn convert_lpa(input: SpecifiedLengthPercentageAuto, viewport_size: [u32; 2], ref_em: f32) -> LengthPercentageAuto {
            match input {
                SpecifiedLengthPercentageAuto::Auto => LengthPercentageAuto::Auto,
                SpecifiedLengthPercentageAuto::Percentage { normalized_percents } => LengthPercentageAuto::Percentage { percents: normalized_percents },
                SpecifiedLengthPercentageAuto::Length { value, unit } => LengthPercentageAuto::Length { pixels: len_to_px(value, unit, viewport_size, ref_em) },
            }
        }

        let font_size = {
            let ref_em = parent.map(|p| p.font_size.clone()).unwrap_or(12.0 /* TODO: ?*/);

            match values.font_size {
                ValueOrInherit::Value(value) => match convert_lp(value.clone(), viewport_size, ref_em) {
                    LengthPercentage::Length { pixels } => pixels,
                    LengthPercentage::Percentage { percents } => percents * parent.unwrap().font_size,
                },
                ValueOrInherit::Inherit => parent.unwrap().font_size.clone(),
            }
        };

        ComputedValues {
            background_color: match values.background_color {
                ValueOrInherit::Value(value) => value.clone(),
                ValueOrInherit::Inherit => parent.unwrap().background_color.clone(),
            },
            background_image: match values.background_image {
                ValueOrInherit::Value(ref value) => value.clone(),
                ValueOrInherit::Inherit => parent.unwrap().background_image.clone(),
            },
            border_bottom_color: match values.border_bottom_color {
                ValueOrInherit::Value(v) => v.clone(),
                ValueOrInherit::Inherit => parent.unwrap().border_bottom_color.clone(),
            },
            border_bottom_style: match values.border_bottom_style {
                ValueOrInherit::Value(v) => v.clone(),
                ValueOrInherit::Inherit => parent.unwrap().border_bottom_style.clone(),
            },
            border_bottom_width: match values.border_bottom_width {
                ValueOrInherit::Value(v) => len_to_px(v.value, v.unit, viewport_size, font_size),
                ValueOrInherit::Inherit => parent.unwrap().border_bottom_width.clone(),
            },
            border_left_color: match values.border_left_color {
                ValueOrInherit::Value(v) => v.clone(),
                ValueOrInherit::Inherit => parent.unwrap().border_left_color.clone(),
            },
            border_left_style: match values.border_left_style {
                ValueOrInherit::Value(v) => v.clone(),
                ValueOrInherit::Inherit => parent.unwrap().border_left_style.clone(),
            },
            border_left_width: match values.border_left_width {
                ValueOrInherit::Value(v) => len_to_px(v.value, v.unit, viewport_size, font_size),
                ValueOrInherit::Inherit => parent.unwrap().border_left_width.clone(),
            },
            border_right_color: match values.border_right_color {
                ValueOrInherit::Value(v) => v.clone(),
                ValueOrInherit::Inherit => parent.unwrap().border_right_color.clone(),
            },
            border_right_style: match values.border_right_style {
                ValueOrInherit::Value(v) => v.clone(),
                ValueOrInherit::Inherit => parent.unwrap().border_right_style.clone(),
            },
            border_right_width: match values.border_right_width {
                ValueOrInherit::Value(v) => len_to_px(v.value, v.unit, viewport_size, font_size),
                ValueOrInherit::Inherit => parent.unwrap().border_right_width.clone(),
            },
            border_top_color: match values.border_top_color {
                ValueOrInherit::Value(v) => v.clone(),
                ValueOrInherit::Inherit => parent.unwrap().border_top_color.clone(),
            },
            border_top_style: match values.border_top_style {
                ValueOrInherit::Value(v) => v.clone(),
                ValueOrInherit::Inherit => parent.unwrap().border_top_style.clone(),
            },
            border_top_width: match values.border_top_width {
                ValueOrInherit::Value(v) => len_to_px(v.value, v.unit, viewport_size, font_size),
                ValueOrInherit::Inherit => parent.unwrap().border_top_width.clone(),
            },
            bottom: match values.bottom {
                ValueOrInherit::Value(value) => convert_lpa(value.clone(), viewport_size, font_size),
                ValueOrInherit::Inherit => parent.unwrap().bottom.clone(),
            },
            color: match values.color {
                ValueOrInherit::Value(ref value) => value.clone(),
                ValueOrInherit::Inherit => parent.unwrap().color.clone(),
            },
            cursor: match values.cursor {
                ValueOrInherit::Value(ref value) => value.clone(),
                ValueOrInherit::Inherit => parent.unwrap().cursor.clone(),
            },
            display: match values.display {
                ValueOrInherit::Value(display) => display.clone(),
                ValueOrInherit::Inherit => parent.unwrap().display.clone(),
            },
            font_size: font_size,
            font_family: match values.font_family {
                ValueOrInherit::Value(ref value) => value.clone(),
                ValueOrInherit::Inherit => parent.unwrap().font_family.clone(),
            },
            height: match values.height {
                ValueOrInherit::Value(value) => convert_lpa(value.clone(), viewport_size, font_size),
                ValueOrInherit::Inherit => parent.unwrap().height.clone(),
            },
            left: match values.left {
                ValueOrInherit::Value(value) => convert_lpa(value.clone(), viewport_size, font_size),
                ValueOrInherit::Inherit => parent.unwrap().left.clone(),
            },
            margin_bottom: match values.margin_bottom {
                ValueOrInherit::Value(value) => convert_lpa(value.clone(), viewport_size, font_size),
                ValueOrInherit::Inherit => parent.unwrap().margin_bottom.clone(),
            },
            margin_left: match values.margin_left {
                ValueOrInherit::Value(value) => convert_lpa(value.clone(), viewport_size, font_size),
                ValueOrInherit::Inherit => parent.unwrap().margin_left.clone(),
            },
            margin_right: match values.margin_right {
                ValueOrInherit::Value(value) => convert_lpa(value.clone(), viewport_size, font_size),
                ValueOrInherit::Inherit => parent.unwrap().margin_right.clone(),
            },
            margin_top: match values.margin_top {
                ValueOrInherit::Value(value) => convert_lpa(value.clone(), viewport_size, font_size),
                ValueOrInherit::Inherit => parent.unwrap().margin_top.clone(),
            },
            padding_bottom: match values.padding_bottom {
                ValueOrInherit::Value(value) => convert_lp(value.clone(), viewport_size, font_size),
                ValueOrInherit::Inherit => parent.unwrap().padding_bottom.clone(),
            },
            padding_left: match values.padding_left {
                ValueOrInherit::Value(value) => convert_lp(value.clone(), viewport_size, font_size),
                ValueOrInherit::Inherit => parent.unwrap().padding_left.clone(),
            },
            padding_right: match values.padding_right {
                ValueOrInherit::Value(value) => convert_lp(value.clone(), viewport_size, font_size),
                ValueOrInherit::Inherit => parent.unwrap().padding_right.clone(),
            },
            padding_top: match values.padding_top {
                ValueOrInherit::Value(value) => convert_lp(value.clone(), viewport_size, font_size),
                ValueOrInherit::Inherit => parent.unwrap().padding_top.clone(),
            },
            position: match values.position {
                ValueOrInherit::Value(value) => value.clone(),
                ValueOrInherit::Inherit => parent.unwrap().position.clone(),
            },
            right: match values.right {
                ValueOrInherit::Value(value) => convert_lpa(value.clone(), viewport_size, font_size),
                ValueOrInherit::Inherit => parent.unwrap().right.clone(),
            },
            text_align: match values.text_align {
                ValueOrInherit::Value(value) => value.clone(),
                ValueOrInherit::Inherit => parent.unwrap().text_align.clone(),
            },
            top: match values.top {
                ValueOrInherit::Value(value) => convert_lpa(value.clone(), viewport_size, font_size),
                ValueOrInherit::Inherit => parent.unwrap().top.clone(),
            },
            visibility: match values.visibility {
                ValueOrInherit::Value(value) => value.clone(),
                ValueOrInherit::Inherit => parent.unwrap().visibility.clone(),
            },
            white_space: match values.white_space {
                ValueOrInherit::Value(value) => value.clone(),
                ValueOrInherit::Inherit => parent.unwrap().white_space.clone(),
            },
            width: match values.width {
                ValueOrInherit::Value(value) => convert_lpa(value.clone(), viewport_size, font_size),
                ValueOrInherit::Inherit => parent.unwrap().width.clone(),
            },
        }
    }
}

impl<D> From<ComputedValues<D::ImageResource, D::FontResource>> for NodePropertiesWithPosition<D> where D: Draw {
    fn from(other: ComputedValues<D::ImageResource, D::FontResource>) -> NodePropertiesWithPosition<D> {
        NodePropertiesWithPosition {
            properties: NodeProperties {
                display: other.display,
                width: other.width,
                height: other.height,
                padding_top: other.padding_top,
                padding_right: other.padding_right,
                padding_bottom: other.padding_bottom,
                padding_left: other.padding_left,
                margin_top: other.margin_top,
                margin_right: other.margin_right,
                margin_bottom: other.margin_bottom,
                margin_left: other.margin_left,
                border_top_width: if other.border_top_style.is_none_or_hidden() { 0.0 }
                                  else { other.border_top_width },
                border_right_width: if other.border_right_style.is_none_or_hidden() { 0.0 }
                                    else { other.border_right_width },
                border_bottom_width: if other.border_bottom_style.is_none_or_hidden() { 0.0 }
                                     else { other.border_bottom_width },
                border_left_width: if other.border_left_style.is_none_or_hidden() { 0.0 }
                                   else { other.border_left_width },
                font_size: other.font_size,
                font_family: other.font_family,
                text_align: other.text_align,
                white_space: other.white_space,
            },
            position: other.position,
            top: other.top,
            right: other.right,
            bottom: other.bottom,
            left: other.left,
        }
    }
}

static DEFAULT_STYLESHEET: &'static str = r#"
html, address,
blockquote,
body, dd, div,
dl, dt, fieldset, form,
frame, frameset,
h1, h2, h3, h4,
h5, h6, noframes,
ol, p, ul, center,
dir, hr, menu, pre   { display: block; unicode-bidi: embed }
li              { display: list-item }
head            { display: none }
table           { display: table }
tr              { display: table-row }
thead           { display: table-header-group }
tbody           { display: table-row-group }
tfoot           { display: table-footer-group }
col             { display: table-column }
colgroup        { display: table-column-group }
td, th          { display: table-cell }
caption         { display: table-caption }
th              { font-weight: bolder; text-align: center }
caption         { text-align: center }
body            { margin: 8px }
h1              { font-size: 2em; margin: .67em 0 }
h2              { font-size: 1.5em; margin: .75em 0 }
h3              { font-size: 1.17em; margin: .83em 0 }
h4, p,
blockquote, ul,
fieldset, form,
ol, dl, dir,
menu            { margin: 1.12em 0 }
h5              { font-size: .83em; margin: 1.5em 0 }
h6              { font-size: .75em; margin: 1.67em 0 }
h1, h2, h3, h4,
h5, h6, b,
strong          { font-weight: bolder }
blockquote      { margin-left: 40px; margin-right: 40px }
i, cite, em,
var, address    { font-style: italic }
pre, tt, code,
kbd, samp       { font-family: monospace }
pre             { white-space: pre }
button, textarea,
input, select   { display: inline-block }
big             { font-size: 1.17em }
small, sub, sup { font-size: .83em }
sub             { vertical-align: sub }
sup             { vertical-align: super }
table           { border-spacing: 2px; }
thead, tbody,
tfoot           { vertical-align: middle }
td, th, tr      { vertical-align: inherit }
s, strike, del  { text-decoration: line-through }
hr              { border: 1px inset }
ol, ul, dir,
menu, dd        { margin-left: 40px }
ol              { list-style-type: decimal }
ol ul, ul ol,
ul ul, ol ol    { margin-top: 0; margin-bottom: 0 }
u, ins          { text-decoration: underline }
/*br:before       { content: "\A"; white-space: pre-line }      // TODO: not supported by CssSelector
center          { text-align: center }
:link, :visited { text-decoration: underline }
:focus          { outline: thin dotted invert }

BDO[DIR="ltr"]  { direction: ltr; unicode-bidi: bidi-override }
BDO[DIR="rtl"]  { direction: rtl; unicode-bidi: bidi-override }

*[DIR="ltr"]    { direction: ltr; unicode-bidi: embed }
*[DIR="rtl"]    { direction: rtl; unicode-bidi: embed }
*/
"#;
