use std::cell::RefCell;
use std::fmt;
use std::rc::Rc;
use std::rc::Weak;
use std::slice::IterMut as SliceIterMut;
use std::vec::IntoIter as VecIntoIter;

use block::BlockFlow;
use computed::Display;
use computed::LengthPercentage;
use computed::LengthPercentageAuto;
use computed::Position;
use draw::Draw;
use flow::Flow;
use flow::FlowDraw;
use flow::LayoutElement;
use flow::NodeProperties;
use flow::NodePropertiesWithPosition;
use inline::InlineFlow;
use inline::RootComputedValues as InlineFlowRootComputedValues;

/// Represents the flow of a document, including all absolute positionned children.
pub struct RootFlow<C, D> where C: Clone, D: Draw {
    draw: Rc<D>,
    root: BlockFlow<C, D>,

    viewport_dimensions: [f32; 2],

    /// List of nodes that are absolute positionned. Also includes `position: fixed`.
    absolute_nodes: Vec<AbsoluteNode<C, D>>,
}

impl<C, D> RootFlow<C, D> where C: Clone, D: Draw {
    #[inline]
    pub fn new(draw: Rc<D>, root_custom_data: C, root_properties: NodeProperties<D>)
               -> RootFlow<C, D>
    {
        let viewport_dimensions = {
            let width = match root_properties.width {
                LengthPercentageAuto::Length { pixels } => pixels,
                _ => panic!()
            };

            let height = match root_properties.height {
                LengthPercentageAuto::Length { pixels } => pixels,
                _ => panic!()
            };

            [width, height]
        };

        RootFlow {
            draw: draw.clone(),
            root: BlockFlow::new(draw, root_custom_data, root_properties),
            viewport_dimensions: viewport_dimensions,
            absolute_nodes: Vec::with_capacity(24),
        }
    }

    // Refreshes the positions of the absolute positionned nodes.
    fn refresh(&mut self) {
        for node in self.absolute_nodes.iter_mut() {
            debug_assert!(node.position == Position::Absolute ||
                          node.position == Position::Fixed);

            let left = match node.left {
                LengthPercentageAuto::Length { pixels } => Some(pixels),
                LengthPercentageAuto::Percentage { .. } => unimplemented!(),
                LengthPercentageAuto::Auto => None,
            };

            let right = match node.right {
                LengthPercentageAuto::Length { pixels } => Some(pixels),
                LengthPercentageAuto::Percentage { .. } => unimplemented!(),
                LengthPercentageAuto::Auto => None,
            };

            let top = match node.top {
                LengthPercentageAuto::Length { pixels } => Some(pixels),
                LengthPercentageAuto::Percentage { .. } => unimplemented!(),
                LengthPercentageAuto::Auto => None,
            };

            let bottom = match node.bottom {
                LengthPercentageAuto::Length { pixels } => Some(pixels),
                LengthPercentageAuto::Percentage { .. } => unimplemented!(),
                LengthPercentageAuto::Auto => None,
            };

            // TODO: wrong

            if let Some(right) = right {
                node.cached_offset[0] = self.viewport_dimensions[0] - right - node.flow.root_border_box_dimensions()[0];
            } else if let Some(left) = left {
                node.cached_offset[0] = left;
            }

            if let Some(bottom) = bottom {
                node.cached_offset[1] = self.viewport_dimensions[1] - bottom - node.flow.root_border_box_dimensions()[1];
            } else if let Some(top) = top {
                node.cached_offset[1] = top;
            }
        }
    }
}

impl<C, D> Flow for RootFlow<C, D> where C: Clone, D: Draw {
    type NodeId = NodeId<C, D>;
    type CustomData = C;
    type Draw = D;
    type Properties = NodePropertiesWithPosition<D>;

    #[inline]
    fn root(&self) -> Self::NodeId {
        NodeId {
            id: self.root.root(),
            source: NodeIdSource::Root,
        }
    }

    #[inline]
    fn root_border_box_dimensions(&self) -> [f32; 2] {
        unimplemented!()
    }

    fn node_at_position(&self, pos: [f32; 2]) -> Option<(Self::NodeId, Self::CustomData)> {
        // TODO: refresh if necessary?

        for (abs_node_id, node) in self.absolute_nodes.iter().enumerate() {
            // TODO: remove margin, border and padding as well
            let pos = [
                pos[0] - node.cached_offset[0],
                pos[1] - node.cached_offset[1]
            ];

            if let Some((n, c)) = node.flow.node_at_position(pos) {
                let node_id = NodeId {
                    id: n,
                    source: NodeIdSource::AbsoluteNode(abs_node_id),
                };

                return Some((node_id, c));
            }
        }

        // FIXME: check absoluted positionned nodes as well
        self.root.node_at_position(pos).map(|(n, c)| {
            (NodeId {
                id: n,
                source: NodeIdSource::Root,
            }, c)
        })
    }

    fn append_element_node(&mut self, parent: &Self::NodeId,
                           properties: &Self::Properties, data: Self::CustomData)
                           -> Self::NodeId
    {
        let is_absolute = match properties.position {
            Position::Absolute => true,
            Position::Fixed => true,
            Position::Relative => false,
            Position::Static => false,
        };

        if is_absolute {
            let new_node = AbsoluteNode {
                flow: BlockFlow::new(self.draw.clone(), data, properties.properties.clone()),
                parent: parent.clone(),
                cached_offset: [0.0, 0.0],
                position: properties.position.clone(),
                top: properties.top.clone(),
                right: properties.right.clone(),
                bottom: properties.bottom.clone(),
                left: properties.left.clone(),
            };

            let output = NodeId {
                id: new_node.flow.root(),
                source: NodeIdSource::AbsoluteNode(self.absolute_nodes.len()),
            };

            self.absolute_nodes.push(new_node);
            output

        } else {
            match parent.source {
                NodeIdSource::Root => {
                    let new_id = self.root.append_element_node(&parent.id, &properties.properties, data);
                    NodeId { id: new_id, source: NodeIdSource::Root }
                },
                NodeIdSource::AbsoluteNode(id) => {
                    let new_id = self.absolute_nodes[id].flow.append_element_node(&parent.id, &properties.properties, data);
                    NodeId { id: new_id, source: NodeIdSource::AbsoluteNode(id) }
                },
            }
        }
    }

    fn append_text_node(&mut self, parent: &Self::NodeId, text: String, data: Self::CustomData)
                        -> Self::NodeId
    {
        match parent.source {
            NodeIdSource::Root => {
                let new_id = self.root.append_text_node(&parent.id, text, data);
                NodeId { id: new_id, source: NodeIdSource::Root }
            },
            NodeIdSource::AbsoluteNode(id) => {
                let new_id = self.absolute_nodes[id].flow.append_text_node(&parent.id, text, data);
                NodeId { id: new_id, source: NodeIdSource::AbsoluteNode(id) }
            },
        }
    }

    fn set_element_properties(&mut self, node: &Self::NodeId, properties: &Self::Properties) {
        // FIXME: handle if `position` changed

        match node.source {
            NodeIdSource::Root => {
                self.root.set_element_properties(&node.id, &properties.properties);
            },
            NodeIdSource::AbsoluteNode(id) => {
                self.absolute_nodes[id].flow.set_element_properties(&node.id, &properties.properties);
            },
        }
    }

    fn delete_node(&mut self, node: &Self::NodeId) {
        match node.source {
            NodeIdSource::Root => {
                self.root.delete_node(&node.id);
            },
            NodeIdSource::AbsoluteNode(id) => {
                self.absolute_nodes[id].flow.delete_node(&node.id);
            },
        }
    }
}

impl<'a, C, D> FlowDraw for &'a mut RootFlow<C, D> where C: Clone, D: Draw {
    type CustomData = C;
    type Draw = D;
    type DrawIter = DrawIter<'a, C, D>;

    #[inline]
    fn draw(self) -> Self::DrawIter {
        // TODO: not always necessary
        self.refresh();

        DrawIter {
            next_absolute_node: self.absolute_nodes.iter_mut(),
            current_iter: self.root.draw(),
            current_offset: [0.0, 0.0],
        }
    }
}

/// Iterator that draws the content of `RootFlow`.
pub struct DrawIter<'a, C, D> where C: 'a + Clone, D: 'a + Draw {
    // List of absolute positionned nodes.
    next_absolute_node: SliceIterMut<'a, AbsoluteNode<C, D>>,
    // Block currently being iterated.
    current_iter: <&'a mut BlockFlow<C, D> as FlowDraw>::DrawIter,
    // Offset to apply to the block currently being iterated.
    current_offset: [f32; 2],
}

impl<'a, C, D> Iterator for DrawIter<'a, C, D> where C: 'a + Clone, D: 'a + Draw {
    type Item = (LayoutElement<D::ImageResource, D::FontVariant>, C);

    fn next(&mut self) -> Option<Self::Item> {
        loop {
            if let Some((mut elem, custom)) = self.current_iter.next() {
                elem.apply_translation(self.current_offset);
                return Some((elem, custom));
            }

            self.current_iter = match self.next_absolute_node.next() {
                Some(node) => {
                    debug_assert!(node.position == Position::Absolute ||
                                  node.position == Position::Fixed);
                    self.current_offset = node.cached_offset;
                    node.flow.draw()
                },
                None => return None,
            };
        }
    }

    #[inline]
    fn size_hint(&self) -> (usize, Option<usize>) {
        (self.current_iter.size_hint().0, None)
    }
}

impl<C, D> fmt::Debug for RootFlow<C, D> where C: Clone + fmt::Debug, D: Draw + fmt::Debug {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        f.debug_struct("RootFlow")
            .field("root", &self.root)
            .field("absolute_nodes", &self.absolute_nodes)
            .finish()
    }
}

pub struct NodeId<C, D> where C: Clone, D: Draw {
    id: <BlockFlow<C, D> as Flow>::NodeId,
    source: NodeIdSource,
}

impl<C, D> Clone for NodeId<C, D> where C: Clone, D: Draw {
    #[inline]
    fn clone(&self) -> NodeId<C, D> {
        NodeId {
            id: self.id.clone(),
            source: self.source.clone(),
        }
    }
}

#[derive(Debug, Clone)]
enum NodeIdSource {
    Root,
    AbsoluteNode(usize),       // FIXME: what if a node is removed?
}

struct AbsoluteNode<C, D> where C: Clone, D: Draw {
    // The flow of the children of this node.
    flow: BlockFlow<C, D>,

    // The parent of the absolute node.
    parent: NodeId<C, D>,

    // Cached position of the node.
    cached_offset: [f32; 2],

    // Properties of the node.
    position: Position,
    top: LengthPercentageAuto,
    right: LengthPercentageAuto,
    bottom: LengthPercentageAuto,
    left: LengthPercentageAuto,
}

impl<C, D> fmt::Debug for AbsoluteNode<C, D> where C: Clone + fmt::Debug, D: Draw + fmt::Debug {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        f.debug_struct("AbsoluteNode")
            .field("flow", &self.flow)
            //.field("parent", &self.parent)        // TODO:
            .field("position", &self.position)
            .field("top", &self.top)
            .field("right", &self.right)
            .field("bottom", &self.bottom)
            .field("left", &self.left)
            .finish()
    }
}
