use std::ascii::AsciiExt;
use std::str::FromStr;

/// All CSS properties that are supported or may be supported in the future.
///
/// Does not include any of the aural-only or print-only properties, as they are out of scope of
/// this library.
#[derive(Debug, Copy, Clone, PartialEq, Eq, Hash)]
pub enum CssProperty {
    BackgroundAttachment,
    BackgroundColor,
    BackgroundImage,
    BackgroundPosition,
    BackgroundRepeat,
    BorderCollapse,
    BorderSpacing,
    BorderBottomColor,
    BorderBottomStyle,
    BorderBottomWidth,
    BorderLeftColor,
    BorderLeftStyle,
    BorderLeftWidth,
    BorderRightColor,
    BorderRightStyle,
    BorderRightWidth,
    BorderTopColor,
    BorderTopStyle,
    BorderTopWidth,
    Bottom,
    CaptionSide,
    Clear,
    Clip,
    Color,
    Content,
    CounterIncrement,
    CounterReset,
    Cursor,
    Direction,
    Display,
    EmptyCells,
    Float,
    FontFamily,
    FontSize,
    FontStyle,
    FontVariant,
    FontWeight,
    Height,
    Left,
    LetterSpacing,
    LineHeight,
    ListStyleImage,
    ListStylePosition,
    ListStyleType,
    MarginTop,
    MarginRight,
    MarginBottom,
    MarginLeft,
    MaxHeight,
    MaxWidth,
    MinHeight,
    MinWidth,
    OutlineColor,
    OutlineStyle,
    OutlineWidth,
    Overflow,
    PaddingLeft,
    PaddingRight,
    PaddingBottom,
    PaddingTop,
    Position,
    Quotes,
    Right,
    TableLayout,
    TextAlign,
    TextDecoration,
    TextIndent,
    TextTransform,
    Top,
    UnicodeBidi,
    VerticalAlign,
    Visibility,
    WhiteSpace,
    Width,
    WordSpacing,
    ZIndex,
}

impl FromStr for CssProperty {
    type Err = ();

    fn from_str(s: &str) -> Result<CssProperty, ()> {
        if s.eq_ignore_ascii_case("background-attachment") {
            Ok(CssProperty::BackgroundAttachment)
        } else if s.eq_ignore_ascii_case("background-color") {
            Ok(CssProperty::BackgroundColor)
        } else if s.eq_ignore_ascii_case("background-image") {
            Ok(CssProperty::BackgroundImage)
        } else if s.eq_ignore_ascii_case("background-position") {
            Ok(CssProperty::BackgroundPosition)
        } else if s.eq_ignore_ascii_case("background-repeat") {
            Ok(CssProperty::BackgroundRepeat)
        } else if s.eq_ignore_ascii_case("border-collapse") {
            Ok(CssProperty::BorderCollapse)
        } else if s.eq_ignore_ascii_case("border-spacing") {
            Ok(CssProperty::BorderSpacing)
        } else if s.eq_ignore_ascii_case("border-bottom-color") {
            Ok(CssProperty::BorderBottomColor)
        } else if s.eq_ignore_ascii_case("border-bottom-style") {
            Ok(CssProperty::BorderBottomStyle)
        } else if s.eq_ignore_ascii_case("border-bottom-width") {
            Ok(CssProperty::BorderBottomWidth)
        } else if s.eq_ignore_ascii_case("border-left-color") {
            Ok(CssProperty::BorderLeftColor)
        } else if s.eq_ignore_ascii_case("border-left-style") {
            Ok(CssProperty::BorderLeftStyle)
        } else if s.eq_ignore_ascii_case("border-left-width") {
            Ok(CssProperty::BorderLeftWidth)
        } else if s.eq_ignore_ascii_case("border-right-color") {
            Ok(CssProperty::BorderRightColor)
        } else if s.eq_ignore_ascii_case("border-right-style") {
            Ok(CssProperty::BorderRightStyle)
        } else if s.eq_ignore_ascii_case("border-right-width") {
            Ok(CssProperty::BorderRightWidth)
        } else if s.eq_ignore_ascii_case("border-top-color") {
            Ok(CssProperty::BorderTopColor)
        } else if s.eq_ignore_ascii_case("border-top-style") {
            Ok(CssProperty::BorderTopStyle)
        } else if s.eq_ignore_ascii_case("border-top-width") {
            Ok(CssProperty::BorderTopWidth)
        } else if s.eq_ignore_ascii_case("bottom") {
            Ok(CssProperty::Bottom)
        } else if s.eq_ignore_ascii_case("caption-side") {
            Ok(CssProperty::CaptionSide)
        } else if s.eq_ignore_ascii_case("clear") {
            Ok(CssProperty::Clear)
        } else if s.eq_ignore_ascii_case("clip") {
            Ok(CssProperty::Clip)
        } else if s.eq_ignore_ascii_case("color") {
            Ok(CssProperty::Color)
        } else if s.eq_ignore_ascii_case("content") {
            Ok(CssProperty::Content)
        } else if s.eq_ignore_ascii_case("counter-increment") {
            Ok(CssProperty::CounterIncrement)
        } else if s.eq_ignore_ascii_case("counter-reset") {
            Ok(CssProperty::CounterReset)
        } else if s.eq_ignore_ascii_case("cursor") {
            Ok(CssProperty::Cursor)
        } else if s.eq_ignore_ascii_case("direction") {
            Ok(CssProperty::Direction)
        } else if s.eq_ignore_ascii_case("display") {
            Ok(CssProperty::Display)
        } else if s.eq_ignore_ascii_case("empty-cells") {
            Ok(CssProperty::EmptyCells)
        } else if s.eq_ignore_ascii_case("float") {
            Ok(CssProperty::Float)
        } else if s.eq_ignore_ascii_case("font-family") {
            Ok(CssProperty::FontFamily)
        } else if s.eq_ignore_ascii_case("font-size") {
            Ok(CssProperty::FontSize)
        } else if s.eq_ignore_ascii_case("font-style") {
            Ok(CssProperty::FontStyle)
        } else if s.eq_ignore_ascii_case("font-variant") {
            Ok(CssProperty::FontVariant)
        } else if s.eq_ignore_ascii_case("font-weight") {
            Ok(CssProperty::FontWeight)
        } else if s.eq_ignore_ascii_case("height") {
            Ok(CssProperty::Height)
        } else if s.eq_ignore_ascii_case("left") {
            Ok(CssProperty::Left)
        } else if s.eq_ignore_ascii_case("letter-spacing") {
            Ok(CssProperty::LetterSpacing)
        } else if s.eq_ignore_ascii_case("line-height") {
            Ok(CssProperty::LineHeight)
        } else if s.eq_ignore_ascii_case("list-style-image") {
            Ok(CssProperty::ListStyleImage)
        } else if s.eq_ignore_ascii_case("list-style-position") {
            Ok(CssProperty::ListStylePosition)
        } else if s.eq_ignore_ascii_case("list-style-type") {
            Ok(CssProperty::ListStyleType)
        } else if s.eq_ignore_ascii_case("margin-top") {
            Ok(CssProperty::MarginTop)
        } else if s.eq_ignore_ascii_case("margin-right") {
            Ok(CssProperty::MarginRight)
        } else if s.eq_ignore_ascii_case("margin-bottom") {
            Ok(CssProperty::MarginBottom)
        } else if s.eq_ignore_ascii_case("margin-left") {
            Ok(CssProperty::MarginLeft)
        } else if s.eq_ignore_ascii_case("max-height") {
            Ok(CssProperty::MaxHeight)
        } else if s.eq_ignore_ascii_case("max-width") {
            Ok(CssProperty::MaxWidth)
        } else if s.eq_ignore_ascii_case("min-height") {
            Ok(CssProperty::MinHeight)
        } else if s.eq_ignore_ascii_case("min-width") {
            Ok(CssProperty::MinWidth)
        } else if s.eq_ignore_ascii_case("outline-color") {
            Ok(CssProperty::OutlineColor)
        } else if s.eq_ignore_ascii_case("outline-style") {
            Ok(CssProperty::OutlineStyle)
        } else if s.eq_ignore_ascii_case("outline-width") {
            Ok(CssProperty::OutlineWidth)
        } else if s.eq_ignore_ascii_case("overflow") {
            Ok(CssProperty::Overflow)
        } else if s.eq_ignore_ascii_case("padding-left") {
            Ok(CssProperty::PaddingLeft)
        } else if s.eq_ignore_ascii_case("padding-right") {
            Ok(CssProperty::PaddingRight)
        } else if s.eq_ignore_ascii_case("padding-bottom") {
            Ok(CssProperty::PaddingBottom)
        } else if s.eq_ignore_ascii_case("padding-top") {
            Ok(CssProperty::PaddingTop)
        } else if s.eq_ignore_ascii_case("position") {
            Ok(CssProperty::Position)
        } else if s.eq_ignore_ascii_case("quotes") {
            Ok(CssProperty::Quotes)
        } else if s.eq_ignore_ascii_case("right") {
            Ok(CssProperty::Right)
        } else if s.eq_ignore_ascii_case("table-layout") {
            Ok(CssProperty::TableLayout)
        } else if s.eq_ignore_ascii_case("text-align") {
            Ok(CssProperty::TextAlign)
        } else if s.eq_ignore_ascii_case("text-decoration") {
            Ok(CssProperty::TextDecoration)
        } else if s.eq_ignore_ascii_case("text-indent") {
            Ok(CssProperty::TextIndent)
        } else if s.eq_ignore_ascii_case("text-transform") {
            Ok(CssProperty::TextTransform)
        } else if s.eq_ignore_ascii_case("top") {
            Ok(CssProperty::Top)
        } else if s.eq_ignore_ascii_case("unicode-bidi") {
            Ok(CssProperty::UnicodeBidi)
        } else if s.eq_ignore_ascii_case("vertical-align") {
            Ok(CssProperty::VerticalAlign)
        } else if s.eq_ignore_ascii_case("visibility") {
            Ok(CssProperty::Visibility)
        } else if s.eq_ignore_ascii_case("white-space") {
            Ok(CssProperty::WhiteSpace)
        } else if s.eq_ignore_ascii_case("width") {
            Ok(CssProperty::Width)
        } else if s.eq_ignore_ascii_case("word-spacing") {
            Ok(CssProperty::WordSpacing)
        } else if s.eq_ignore_ascii_case("z-index") {
            Ok(CssProperty::ZIndex)
        } else {
            Err(())
        }
    }
}
