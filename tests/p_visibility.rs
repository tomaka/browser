extern crate browser;

use browser::draw::DummyDraw;

#[test]
fn hidden() {
    let draw = DummyDraw;
    let mut doc = browser::Document::parse(draw, "<p style=\"visibility:hidden;\">A</p>",
                                           [1024, 768]);

    assert_eq!(doc.draw().count(), 0);
}
