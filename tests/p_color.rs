extern crate browser;

use browser::draw::DrawProperties;
use browser::draw::DummyDraw;

#[test]
fn color() {
    let draw = DummyDraw;
    let mut doc = browser::Document::parse(draw, "<p style=\"color:red;\">A</p>",
                                           [1024, 768]);

    for node in doc.draw() {
        match node {
            DrawProperties::Text { text, color, .. } => {
                assert_eq!(text, 'A');
                assert_eq!(color, [1.0, 0.0, 0.0, 1.0]);
            },
            _ => panic!()
        }
    }
}

#[test]
fn color_inherit() {
    let draw = DummyDraw;
    let mut doc = browser::Document::parse(draw, "<div style=\"color:red;\"><p>A</p></div>",
                                           [1024, 768]);

    for node in doc.draw() {
        match node {
            DrawProperties::Text { text, color, .. } => {
                assert_eq!(text, 'A');
                assert_eq!(color, [1.0, 0.0, 0.0, 1.0]);
            },
            _ => panic!()
        }
    }
}
