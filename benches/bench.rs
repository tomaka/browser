#![feature(test)]

extern crate test;
extern crate browser;

use browser::draw::DummyDraw;
use test::Bencher;

#[bench]
fn init(b: &mut Bencher) {
    b.iter(|| {
        browser::Document::parse(DummyDraw, "<p>hello world</p>", [1024, 768])
    });
}

#[bench]
fn draw_hello_world(b: &mut Bencher) {
    let mut doc = browser::Document::parse(DummyDraw, "<p>hello world</p>", [1024, 768]);

    b.iter(|| {
        doc.draw().collect::<Vec<_>>()
    });
}

#[bench]
fn draw_hello_world_cursor_move(b: &mut Bencher) {
    let mut doc = browser::Document::parse(DummyDraw, "<p>hello world</p>", [1024, 768]);
    let mut cursor_position = None;

    b.iter(|| {
        doc.set_cursor_position(cursor_position);

        if cursor_position.is_none() {
            cursor_position = Some([10, 10]);   // likely position of the text
        } else {
            cursor_position = None;
        }

        doc.draw().collect::<Vec<_>>()
    });
}

#[bench]
fn draw_hello_world_cursor_move_hover_rule(b: &mut Bencher) {
    let html: &'static str = r#"
        <html><head>
            <style type="text/css">
                p:hover { color: red; }
            </style>
        </head>
        <body>
            <p>hello world</p>
        </body></html>"#;

    let mut doc = browser::Document::parse(DummyDraw, html, [1024, 768]);
    let mut cursor_position = None;

    b.iter(|| {
        doc.set_cursor_position(cursor_position);

        if cursor_position.is_none() {
            cursor_position = Some([10, 10]);   // likely position of the text
        } else {
            cursor_position = None;
        }

        doc.draw().collect::<Vec<_>>()
    });
}

#[bench]
fn draw_lot_of_text(b: &mut Bencher) {
    let text: &'static str = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam \
                              bibendum nisi ac felis varius sodales. Proin ac vehicula magna, ac \
                              porta orci. Suspendisse quis posuere metus, vel egestas massa. Morbi \
                              elit libero, pellentesque eget ultrices sagittis, fringilla a dui. \
                              Nam ac dui est. Nulla in nisl in augue elementum congue eu nec \
                              lacus. Phasellus et aliquam dui. Aliquam ac ullamcorper leo. Vivamus \
                              pretium condimentum ultrices. Aliquam erat volutpat. Aliquam cursus \
                              cursus urna hendrerit eleifend. Ut finibus mi massa, a dictum nisi \
                              pharetra eget. Suspendisse sed elit vel sem tempor vulputate. Sed \
                              varius tempus libero, ac imperdiet dolor pharetra sit amet. Nullam \
                              rutrum vitae nulla vitae molestie. Maecenas dictum arcu et ligula \
                              scelerisque, ac aliquam justo interdum. Vestibulum finibus mattis \
                              sapien imperdiet condimentum. Nunc gravida enim bibendum lectus \
                              vehicula, quis mattis felis interdum. Aenean ac eleifend est. \
                              Quisque iaculis dolor ligula, sit amet pretium orci efficitur et. \
                              Nulla tellus sem, porttitor id congue a, dignissim in enim. \
                              Pellentesque habitant morbi tristique senectus et netus et malesuada \
                              fames ac turpis egestas. In nec odio bibendum, posuere arcu eget, \
                              ultrices erat. Mauris commodo finibus felis ac convallis. Proin ante \
                              velit, varius quis nulla et, rutrum efficitur diam. Ut et sapien \
                              vitae mi porta sagittis sit amet at nisi. Ut orci ipsum, placerat \
                              id semper sagittis, tincidunt vitae mauris. Morbi consectetur orci \
                              quis massa ultricies, vitae imperdiet velit interdum. Phasellus \
                              malesuada congue felis ut luctus. Aliquam eget nisi non ligula \
                              maximus varius non eu orci. Nulla facilisi. Etiam nec nibh mollis, \
                              maximus dui eu, lobortis quam. Etiam sollicitudin erat eu sapien \
                              hendrerit, in tristique sapien luctus. Suspendisse potenti. Morbi \
                              hendrerit nibh ac tortor scelerisque, nec lacinia orci varius. \
                              Suspendisse potenti. Maecenas eu ultrices sem. Proin rhoncus risus \
                              sem, ut laoreet ipsum congue ut. Ut imperdiet magna sed vehicula \
                              convallis. Curabitur vulputate arcu ac justo ornare tincidunt. Sed \
                              fringilla aliquam eleifend. Curabitur eget accumsan quam. Integer eu \
                              purus porttitor, laoreet turpis at, fermentum massa. Curabitur \
                              pulvinar interdum hendrerit. Cras ut nunc ipsum. Quisque blandit \
                              vulputate ex, non cursus ante tristique bibendum.";

    let mut doc = browser::Document::parse(DummyDraw, &format!("<p>{0}{0}</p>", text),
                                           [1024, 768]);

    b.iter(|| {
        doc.draw().collect::<Vec<_>>()
    });
}
