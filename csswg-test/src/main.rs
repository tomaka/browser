extern crate browser;
extern crate git2;
extern crate regex;
extern crate walkdir;

use std::env;
use std::fs::File;
use std::io::Read;
use std::panic;
use std::path::Path;
use std::process;

use git2::Repository;
use regex::Regex;
use walkdir::WalkDir;

fn main() {
    println!("Cloning the csswg-test repository");
    match Repository::clone("https://github.com/w3c/csswg-test", "csswg-test") {
        Ok(_) => (),
        Err(e) => println!("Failed to clone: {}\nContinuing anyway", e),
    };

    // TODO: the use of a regex is obviously hacky
    let regex = Regex::new(r#"<link rel="match" href="(.*?)">"#).unwrap();

    // It is possible to pass a list of words as arguments to the program. Only tests that contain
    // all these words in their name will be attempted.
    let keywords = env::args().skip(1).collect::<Vec<_>>();

    // Final results
    let mut passed = Vec::<String>::new();
    let mut failed = Vec::<String>::new();
    let mut skipped = Vec::<String>::new();

    'tests_loop: for entry in WalkDir::new("csswg-test") {
        let entry = entry.unwrap();
        if !entry.file_type().is_file() {
            continue;
        }

        {
            let path_str = entry.path().display().to_string();
            for kw in keywords.iter() { if !path_str.contains(kw) { continue 'tests_loop; } }
        }

        let original_content = {
            let mut file = match File::open(entry.path()) {
                Ok(f) => f,
                Err(err) => panic!("Failed to open file {:?}\n{}", entry.path(), err)
            };

            let mut original_content = String::new();
            match file.read_to_string(&mut original_content) {
                Ok(_) => (),
                Err(_) => continue,
            }
            original_content
        };

        let reference = {
            let captures = match regex.captures(&original_content) {
                None => continue,
                Some(c) => c,
            };

            captures.at(1).unwrap().to_owned()
            // We turn that into a String because we move out `original_content` below.
        };

        let reference_content = {
            let path = entry.path().parent().unwrap().join(Path::new(&reference));

            let mut file = match File::open(&path) {
                Ok(f) => f,
                Err(_) => {
                    skipped.push(entry.path().display().to_string());
                    continue;
                }
            };

            let mut ctnt = String::new();
            file.read_to_string(&mut ctnt).unwrap();
            ctnt
        };

        let success = panic::catch_unwind(move || {
            let mut original_document = browser::Document::parse(browser::draw::DummyDraw,
                                                                 &original_content, [1024, 768]);
            let mut reference_document = browser::Document::parse(browser::draw::DummyDraw,
                                                                  &reference_content, [1024, 768]);
            original_document.draw().eq(reference_document.draw())
        }).unwrap_or(false);

        if success {
            passed.push(entry.path().display().to_string());
        } else {
            failed.push(entry.path().display().to_string());
        }
    }

    println!("{} passed\n{} failed\n{} skipped", passed.len(), failed.len(), skipped.len());

    if failed.is_empty() {
        process::exit(0);
    } else {
        process::exit(1);
    }
}
