Game-oriented HTML/CSS engine.

Handling the game UI is one of the most frequently encountered problems in game development.
HTML/CSS have some major advantages:

- They are well known technologies. You can ask anyone to work on your game UI without having
  to teach them.
- All IDEs support them, and you can use a browser to easily get a preview and to debug your UI
  without having to launch the actual game.
- The strengths and limitations of HTML/CSS are well-known, contrary to an in-house system which
  may require some refactoring if you want to add features that you didn't think about.
- You can serve HTML from a server and render it in-game. You can serve the same content for
  both your public website and your game.

The main disadvantage compared to an in-house system is of course the decreased performances.

# Features

- Rendering and user input must be handled by the user. Game developers usually dont want to
  deal with the headaches of having two separate rendering systems.
- Loading fonts and images must be handled by the user.
- Javascript is not supported. Only HTML/CSS are.

The scope of this library is the following:

- HTML 5.
- [CSS 2.2](https://www.w3.org/TR/CSS22/), with the exception of:
 - Tables.
 - `float` and `clear`.
 - Media queries.
 - Aural and print properties.
- The background-related properties of [CSS 3 Backgrounds and Borders](https://www.w3.org/TR/css3-background/).
- [CSS 3 Transitions](https://www.w3.org/TR/css3-transitions/).
- [CSS 3 Animations](https://www.w3.org/TR/css3-animations/).
- [CSS 3 Flexible Box Layout](https://www.w3.org/TR/css-flexbox-1/).

Any bug or unsupported feature related to these should be reported.

# Rustdoc output

https://tomaka.gitlab.io/browser/browser/index.html

# Usage

Using this library is done in four three:

1) Implement the `Draw` trait on one of your structs. That's the longest and most challenging
   part, as you have to handle loading fonts and images and rendering them.

2) Create a `Document` with `Document::parse(my_draw_impl, html_content)`, where `my_draw_impl`
   is an instance of the struct you implemented the `Draw` trait on.

3) Call `document.draw()` in order to obtain a list of elements to draw, sorted from the deepest
   to the outermost element. Pass these elements to your draw system.
