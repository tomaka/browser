extern crate bmfont;
extern crate browser;
#[macro_use]
extern crate glium;
extern crate image;

use std::env;
use std::fs::File;
use std::io::Read;
use std::path::Path;
use std::process;
use std::rc::Rc;
use std::thread;
use std::time::Duration;
use std::time::Instant;

use browser::draw as browser_draw;
use glium::glutin;
use glium::DisplayBuild;
use glium::Surface;

mod draw;

fn main() {
    // Load the content of the HTML file.
    let content = {
        let file_path = env::args().skip(1).next();
        let file_path = match file_path {
            None => {
                println!("Usage: {} <path_to_html_file>", env::args().next().unwrap());
                process::exit(0);
            },
            Some(p) => Path::new(&p).to_owned()
        };

        let mut out = String::new();
        File::open(&file_path).expect("File doesn't exist").read_to_string(&mut out).unwrap();
        out
    };

    // Open the window.
    let display = glutin::WindowBuilder::new()
        .with_title("Browser renderer")
        .build_glium()
        .unwrap();

    // Build the `Draw` object, which is the interface between the browser and us.
    let draw_system = Rc::new(draw::DrawSystem::new(&display));

    // Build the document.
    let mut document = browser::Document::parse(DrawDummy(draw_system.clone()), &content,
                                                { let fb = display.get_framebuffer_dimensions();
                                                 [fb.0, fb.1]});

    loop {
        // Drawing a frame.
        let mut target = display.draw();
        target.clear_color(1.0, 1.0, 1.0, 1.0);
        draw_system.draw(&mut target, {
            // In a real application, you would directly pass `document.draw()` to the function
            // call. However since we want to print the time it took for the browser to output
            // the values, we collect them in a Vec first.
            let before = Instant::now();
            let values = document.draw().collect::<Vec<_>>();
            println!("Duration taken by calling `draw`: {:?}",
                     Instant::now().duration_since(before));
            values.into_iter()
        });
        target.finish().unwrap();

        // Polling and handling the events received by the window
        for event in display.poll_events() {
            match event {
                glutin::Event::Resized(w, h) => document.set_viewport_size([w, h]),
                glutin::Event::MouseMoved(x, y) => if x >= 0 && y >= 0 {
                    document.set_cursor_position(Some([x as u32, y as u32]))
                } else {
                    document.set_cursor_position(None)
                },
                glutin::Event::Focused(false) => document.set_cursor_position(None),
                glutin::Event::Closed => return,
                _ => ()
            }
        }

        // Sleeping a bit.
        thread::sleep(Duration::from_millis(16));
    }
}

// Since is is forbidden to implement a trait on `Rc<DrawSystem>`, we have to use a
// wrapper struct.
struct DrawDummy(Rc<draw::DrawSystem>);
impl browser_draw::Draw for DrawDummy {
    type ImageResource = <draw::DrawSystem as browser_draw::Draw>::ImageResource;
    type FontResource = <draw::DrawSystem as browser_draw::Draw>::FontResource;
    type FontVariant = <draw::DrawSystem as browser_draw::Draw>::FontVariant;

    #[inline]
    fn parse_image(&self, url: &str) -> Result<Self::ImageResource, ()> {
        self.0.parse_image(url)
    }

    #[inline]
    fn intrinsic_dimensions(&self, image: &Self::ImageResource) -> [f32; 2] {
        self.0.intrinsic_dimensions(image)
    }

    #[inline]
    fn parse_font(&self, url: &str) -> Result<Self::FontResource, ()> {
        self.0.parse_font(url)
    }

    #[inline]
    fn default_font(&self) -> Self::FontResource {
        self.0.default_font()
    }

    #[inline]
    fn font_variant(&self, font: &Self::FontResource, weight: i32) -> Self::FontVariant {
        self.0.font_variant(font, weight)
    }

    #[inline]
    fn glyph_infos(&self, font: &Self::FontVariant, glyph: char) -> browser_draw::GlyphInfos {
        self.0.glyph_infos(font, glyph)
    }

    #[inline]
    fn line_height(&self, font: &Self::FontVariant) -> f32 {
        self.0.line_height(font)
    }

    #[inline]
    fn kerning(&self, font: &Self::FontVariant, first_char: char, second_char: char) -> f32 {
        self.0.kerning(font, first_char, second_char)
    }
}
