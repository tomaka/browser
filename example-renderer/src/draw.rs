use std::collections::HashMap;
use std::fs::File;
use std::path::Path;
use std::sync::Arc;
use std::sync::Mutex;

use bmfont::BMFont;
use bmfont::OrdinateOrientation;
use browser::draw as browser_draw;
use glium;
use glium::Surface;
use image;

pub struct DrawSystem {
    display: glium::Display,
    program: glium::Program,
    vertex_buffer: glium::VertexBuffer<Vertex>,

    texture_array: glium::texture::SrgbTexture2dArray,
    texture_array_next_index: Mutex<u32>,   // TODO: atomic

    images: Mutex<HashMap<String, Arc<ImgResource>>>,
    fonts: Mutex<HashMap<String, Arc<FontResource>>>,
}

impl DrawSystem {
    pub fn new(display: &glium::Display) -> DrawSystem {
        let vertex_buffer = glium::VertexBuffer::immutable(display, &[
            Vertex { i_position: [0.0, 0.0] },
            Vertex { i_position: [0.0, 1.0] },
            Vertex { i_position: [1.0, 0.0] },
            Vertex { i_position: [1.0, 1.0] }
        ]).unwrap();

        DrawSystem {
            display: display.clone(),
            program: glium::Program::from_source(display, VS_SRC, FS_SRC, None).unwrap(),
            vertex_buffer: vertex_buffer,
            texture_array: {
                let mip = glium::texture::MipmapsOption::NoMipmap;
                glium::texture::SrgbTexture2dArray::empty_with_mipmaps(display, mip, 512, 512, 64).unwrap()
            },
            texture_array_next_index: Mutex::new(0),
            images: Mutex::new(HashMap::new()),
            fonts: Mutex::new(HashMap::new()),
        }
    }

    pub fn draw<S, I>(&self, target: &mut S, elements: I)
        where S: Surface,
              I: Iterator<Item = browser_draw::DrawProperties<Arc<ImgResource>, Arc<FontResource>>>
    {
        let dimensions = {
            let d = target.get_dimensions();
            [d.0 as f32, d.1 as f32]
        };

        let instances = elements.filter_map(|element| {
            match element {
                browser_draw::DrawProperties::Text { text, font, color, left_coordinate,
                                                     top_coordinate, width, height } =>
                {
                    let glyph_info = match font.font.glyph_info(text as u32) {
                        Ok(g) => g,
                        Err(_) => return None
                    };

                    Some(Instance {
                        i_offset: [left_coordinate, top_coordinate],
                        i_size: [width, height],
                        i_tex_index: font.texture.index,
                        i_background_color: [0.0, 0.0, 0.0, 0.0],
                        i_text_color: color,
                        i_image_alpha: 0.0,
                        i_tex_offset: [glyph_info.x as f32 / font.texture.width,
                                       glyph_info.y as f32 / font.texture.height],
                        i_tex_size: [glyph_info.width as f32 / font.texture.width,
                                     glyph_info.height as f32 / font.texture.height],
                    })
                },
                browser_draw::DrawProperties::Color { color, left_coordinate, top_coordinate,
                                                      width, height, .. } =>
                {
                    Some(Instance {
                        i_offset: [left_coordinate, top_coordinate],
                        i_size: [width, height],
                        i_tex_index: 0,
                        i_background_color: color,
                        i_text_color: [0.0, 0.0, 0.0, 1.0],
                        i_image_alpha: 0.0,
                        i_tex_offset: [0.0, 0.0],
                        i_tex_size: [0.0, 0.0],
                    })
                },
                browser_draw::DrawProperties::Image { image, image_width, image_height,
                                                      left_coordinate, top_coordinate,
                                                      width, height, .. } =>
                {
                    Some(Instance {
                        i_offset: [left_coordinate, top_coordinate],
                        i_size: [width, height],
                        i_tex_index: image.index,
                        i_background_color: [0.0, 0.0, 0.0, 0.0],
                        i_text_color: [0.0, 0.0, 0.0, 0.0],
                        i_image_alpha: 1.0,
                        i_tex_offset: [0.0, 0.0],
                        i_tex_size: [width / image_width, height / image_height],
                    })
                },
                browser_draw::DrawProperties::Cursor { cursor, position } => {
                    println!("Predefined cursor {:?} at {:?}", cursor, position);
                    None
                },
            }
        }).collect::<Vec<_>>();

        let instance_buffer = glium::VertexBuffer::new(&self.display, &instances).unwrap();

        target.draw((&self.vertex_buffer, instance_buffer.per_instance().unwrap()),
                    glium::index::NoIndices(glium::index::PrimitiveType::TriangleStrip),
                    &self.program, &uniform!{
                        u_viewport_size: dimensions,
                        u_texture: &self.texture_array,
                    },
                    &glium::DrawParameters {
                        blend: glium::Blend::alpha_blending(),
                        .. Default::default()
                    }).unwrap();
    }
}

impl browser_draw::Draw for DrawSystem {
    type ImageResource = Arc<ImgResource>;
    type FontResource = Arc<FontResource>;
    type FontVariant = Arc<FontResource>;

    fn parse_image(&self, url: &str) -> Result<Self::ImageResource, ()> {
        let mut images = self.images.lock().unwrap();
        if let Some(image) = images.get(url) {
            return Ok(image.clone());
        }

        let image = image::load(try!(File::open(&Path::new(url)).map_err(|_| ())), image::PNG)
                                .unwrap().to_rgba();
        let image_dimensions = image.dimensions();
        let image = glium::texture::RawImage2d::from_raw_rgba_reversed(image.into_raw(),
                                                                       image_dimensions);
        let opengl_texture = {
            let mip = glium::texture::MipmapsOption::NoMipmap;
            glium::texture::SrgbTexture2d::with_mipmaps(&self.display, image, mip)
        }.unwrap();

        let index = {
            let mut idx = self.texture_array_next_index.lock().unwrap();
            let i = *idx;
            *idx += 1;
            i
        };

        glium::framebuffer::SimpleFrameBuffer::new(&self.display, &opengl_texture).unwrap()
            .fill(&glium::framebuffer::SimpleFrameBuffer::new(&self.display, self.texture_array.main_level().layer(index).unwrap()).unwrap(),
                  glium::uniforms::MagnifySamplerFilter::Linear);

        let result = Arc::new(ImgResource {
            index: index as i32,
            width: opengl_texture.get_width() as f32,
            height: opengl_texture.get_height().unwrap() as f32,
        });

        images.insert(url.to_owned(), result.clone());
        Ok(result)
    }

    #[inline]
    fn intrinsic_dimensions(&self, image: &Self::ImageResource) -> [f32; 2] {
        [image.width, image.height]
    }

    fn parse_font(&self, url: &str) -> Result<Self::FontResource, ()> {
        let mut fonts = self.fonts.lock().unwrap();
        if let Some(font) = fonts.get(url) {
            return Ok(font.clone());
        }

        let file = try!(File::open(&Path::new(url)).map_err(|_| ()));

        let font_infos = BMFont::new(file, OrdinateOrientation::BottomToTop)
                                     .expect("Unable to parse font atlas file");

        let pages = font_infos.pages();
        assert_eq!(pages.len(), 1);     // not implemented for more pages
        let texture = self.parse_image(&pages[0]).unwrap();

        let result = Arc::new(FontResource {
            font: font_infos,
            texture: texture,
        });

        fonts.insert(url.to_owned(), result.clone());

        Ok(result)
    }

    #[inline]
    fn default_font(&self) -> Self::FontResource {
        self.parse_font("times.fnt").unwrap()
    }

    #[inline]
    fn font_variant(&self, font: &Self::FontResource, _font_weight: i32) -> Self::FontVariant {
        font.clone()
    }

    fn glyph_infos(&self, font: &Self::FontVariant, glyph: char) -> browser_draw::GlyphInfos {
        let glyph_info = match font.font.glyph_info(glyph as u32) {
            Ok(g) => g,
            Err(_) => {
                return browser_draw::GlyphInfos {
                    width: 0.0,
                    height: 0.0,
                    x_offset: 0.0,
                    x_advance: 0.0,
                    y_offset: 0.0,
                };
            }
        };

        browser_draw::GlyphInfos {
            width: glyph_info.width as f32 / font.font.base_height() as f32,
            height: glyph_info.height as f32 / font.font.base_height() as f32,
            x_offset: glyph_info.xoffset as f32 / font.font.base_height() as f32,
            x_advance: glyph_info.xadvance as f32 / font.font.base_height() as f32,
            y_offset: (font.font.line_height() as i32 - glyph_info.yoffset) as f32 / font.font.base_height() as f32,
        }
    }

    #[inline]
    fn line_height(&self, font: &Self::FontVariant) -> f32 {
        font.font.line_height() as f32 / font.font.base_height() as f32
    }

    #[inline]
    fn kerning(&self, font: &Self::FontVariant, first_char: char, second_char: char) -> f32 {
        let val = font.font.kerning_value(first_char as u32, second_char as u32);
        val as f32 / font.font.base_height() as f32
    }
}

// TODO: shouldn't be public
#[derive(Debug)]
pub struct ImgResource {
    index: i32,
    width: f32,
    height: f32,
}

// TODO: shouldn't be public
#[derive(Debug)]
pub struct FontResource {
    font: BMFont,
    texture: Arc<ImgResource>,
}

const VS_SRC: &'static str = r#"
#version 140

uniform vec2 u_viewport_size;

in vec2 i_position;
in vec2 i_offset;
in int i_tex_index;
in vec2 i_size;
in vec4 i_text_color;
in float i_image_alpha;
in vec4 i_background_color;
in vec2 i_tex_offset;
in vec2 i_tex_size;

out vec2 v_tex_coords;
flat out int v_tex_index;
out vec4 v_text_color;
out float v_image_alpha;
out vec4 v_background_color;

void main() {
    vec2 px_position = i_offset + i_position * i_size;

    vec2 viewport_position = vec2(2.0) * px_position / u_viewport_size - vec2(1.0);
    viewport_position.y *= -1.0;

    gl_Position = vec4(viewport_position, 0.0, 1.0);
    v_tex_coords = i_tex_offset + i_position * i_tex_size;
    v_background_color = i_background_color;
    v_text_color = i_text_color;
    v_image_alpha = i_image_alpha;
    v_tex_index = i_tex_index;
}
"#;

const FS_SRC: &'static str = r#"
#version 140

uniform sampler2DArray u_texture;

in vec2 v_tex_coords;
flat in int v_tex_index;
in vec4 v_text_color;
in float v_image_alpha;
in vec4 v_background_color;
out vec4 f_color;

void main() {
    vec2 real_coords = v_tex_coords;
    real_coords.y = 1.0 - real_coords.y;

    vec4 texel = texture(u_texture, vec3(real_coords, float(v_tex_index)));
    float text_alpha = texel.r;

    f_color = mix(text_alpha * v_text_color, v_background_color, v_background_color.a);
    f_color = mix(f_color, texel, v_image_alpha);
}
"#;

#[derive(Copy, Clone)]
struct Vertex {
    i_position: [f32; 2],
}

implement_vertex!(Vertex, i_position);

#[derive(Copy, Clone)]
struct Instance {
    i_offset: [f32; 2],
    i_size: [f32; 2],
    i_tex_index: i32,
    i_background_color: [f32; 4],
    i_text_color: [f32; 4],
    i_image_alpha: f32,
    i_tex_offset: [f32; 2],
    i_tex_size: [f32; 2],
}

implement_vertex!(Instance, i_offset, i_size, i_tex_index, i_background_color, i_text_color,
                            i_image_alpha, i_tex_offset, i_tex_size);
