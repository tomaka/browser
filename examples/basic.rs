extern crate browser;

use browser::draw::DummyDraw;

fn main() {
    let draw = DummyDraw;
    let mut doc = browser::Document::parse(draw, "<p style=\"display: block; margin-left: 8px;\">Hello <strong>world</strong></p>", [1024, 768]);

    loop {
        let _ = doc.draw().collect::<Vec<_>>();
    }
}
